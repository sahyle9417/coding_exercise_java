package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_1024 {
	public static void main(String[] args)throws Exception{
		Scanner scan = new Scanner(System.in);
		long N = scan.nextLong();
		int L = scan.nextInt();
		
		for(int l=L; l<=100; l++) {
			
			// 합공식 이용해 N과 L값으로 start값 역산
			double start_double = (2*(double)N + l - l*l)/(2*l);
			
			// 역산된 start값을 int로 바꾼 값을 합공식에 다시 대입해서 N이 나오는지 확인
			long start_int = Math.round(start_double);
			long sumx2 = ((start_int) + (start_int+l-1)) * l;
			
			// 역산된 start값과 L로 계산된 합이 N과 일치
			if(sumx2 == N*2 && start_int>0) {
				for(int tmp=0; tmp<l; tmp++) {
					System.out.print((start_int+tmp)+" ");
				}
				return;
			}
		}
		System.out.print(-1);
	}
}
