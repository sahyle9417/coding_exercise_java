package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_samsung_3190 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int N = Integer.parseInt(br.readLine());
		
		// 사과는 1, 뱀은 9, 나머지 빈공간은 0
		int[][] map = new int[N+1][N+1];
		
		int K = Integer.parseInt(br.readLine());
		StringTokenizer st;
		for(int k=0; k<K; k++) {
			st = new StringTokenizer(br.readLine());
			int apple_r = Integer.parseInt(st.nextToken());
			int apple_c = Integer.parseInt(st.nextToken());
			map[apple_r][apple_c] = 1;
		}
		
		int L = Integer.parseInt(br.readLine());
		int[] direction_change_at = new int[10001];
		for(int l=0; l<L; l++) {
			st = new StringTokenizer(br.readLine());
			int time = Integer.parseInt(st.nextToken());
			char direction_change = st.nextToken().charAt(0);
			if(direction_change=='L') {
				direction_change_at[time] = -1;
			}
			else {
				direction_change_at[time] = 1;
			}
		}

		int[] di = {0, 1, 0, -1};
		int[] dj = {1, 0, -1, 0};
		
		LinkedList<ij> snake = new LinkedList<>();
		snake.add(new ij(1, 1));
		int head_i = 1;
		int head_j = 1;
		map[1][1] = 9;
		int direction = 0;
		int time=1;
		for(; time<=10000; time++) {
			//System.out.println("\ntime:"+time);
			
			head_i += di[direction];
			head_j += dj[direction];
			//System.out.println("head:"+head_i+","+head_j);
			
			// 벽 또는 자기 자신과 부딪힘
			if(head_i>N || head_i<1 || head_j>N || head_j<1 || map[head_i][head_j]==9) {
				//System.out.println("game over");
				break;
			}
			snake.add(new ij(head_i, head_j));
			if(map[head_i][head_j]!=1) {
				ij tail = snake.removeFirst();
				map[tail.i][tail.j] = 0;
			}
			map[head_i][head_j] = 9;
			
			//System.out.print("direction:"+direction);
			direction = (direction + direction_change_at[time] + 4) % 4;
			//System.out.println("->"+direction);
			/*for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					System.out.print(map[i][j]+" ");
				}
				System.out.print("\n");	
			}*/
			
		}
		System.out.println(time);
		
	}
	
	static class ij{
		int i;
		int j;
		ij(int i, int j){
			this.i = i;
			this.j = j;
		}
	}
}
