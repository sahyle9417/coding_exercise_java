package coding_exercise_java;

import java.io.*;

public class linked_list_leet_easy_Delete_Node_in_a_Linked_List {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		int n = Integer.parseInt(str);
		ListNode[] list = new ListNode[n];
		for (int i = 0; i < n; i++) {
			int val = Integer.parseInt(br.readLine());
			list[i] = new ListNode(val);
			if (i != 0) {
				list[i - 1].next = list[i];
			}
		}
		int delNodeVal = Integer.parseInt(br.readLine());
		ListNode delNode = null;
		for (int i = 0; i < n; i++) {
			if (list[i].val == delNodeVal) {
				delNode = list[i];
			}
		}
		linked_list_leet_easy_Delete_Node_in_a_Linked_List_Solution solution = new linked_list_leet_easy_Delete_Node_in_a_Linked_List_Solution();
		solution.deleteNode(delNode);
		ListNode node = list[0];
		while (node != null) {
			System.out.println(node.val);
			node = node.next;
		}
	}
}

/*
class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}
*/

class linked_list_leet_easy_Delete_Node_in_a_Linked_List_Solution {
	public void deleteNode(ListNode node) {
		node.val = node.next.val;
		node.next = node.next.next;
	}
	/*
	public void deleteNode(ListNode node) {
    	ListNode prev = node;
        while(node.next != null) {
        	node.val = node.next.val;
        	prev = node;
        	node = node.next;
        }
        prev.next = null;
    }
    */
}
