package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_boj_1325_reverse {

	static int N;
	static ArrayList<Integer>[] map;
	static int[] visit;
	static int[] mutate;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());

		N = Integer.parseInt(st.nextToken());
		map = new ArrayList[N+1];
		
		for(int n=1; n<=N; n++) {
			map[n] = new ArrayList<Integer>();
		}
		mutate = new int[N+1];
		
		int M = Integer.parseInt(st.nextToken());
		for(int m=1; m<=M; m++) {
			st = new StringTokenizer(br.readLine());
			int to = Integer.parseInt(st.nextToken());
			int from = Integer.parseInt(st.nextToken());
			map[to].add(from);
		}
		
		for(int leaf=1; leaf<=N; leaf++) {
			visit = new int[N+1];
			dfs(leaf);
		}
		
		int max = 0;
		for(int n=1; n<=N; n++) {
			max = Math.max(mutate[n], max);
		}
		for(int n=1; n<=N; n++) {
			if(mutate[n]==max) {
				System.out.print(n+" ");
			}
		}
	}
	
	static void dfs(int to) {
		visit[to] = 1;
		for(int from : map[to]) {
			if(visit[from]==0) {
				mutate[from]++;
				dfs(from);
			}
		}
	}
}
