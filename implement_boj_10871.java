package coding_exercise_java;

import java.io.*;
import java.util.*;

public class implement_boj_10871 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int X = Integer.parseInt(st.nextToken());
		
		int[] seq = new int[N+1];
		st = new StringTokenizer(br.readLine());
		for(int i=1; i<=N; i++) {
			seq[i] = Integer.parseInt(st.nextToken());
		}
		for(int i=1; i<=N; i++) {
			if(seq[i]<X) {
				System.out.print(seq[i]+" ");
			}
		}
	}
}
