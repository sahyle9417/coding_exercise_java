package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_samsung_5373 {
	
	// 위, 아래, 앞, 뒤, 좌, 우
	static int[][][] dice_org =	{{{0, 0, 0}, {0, 0, 0}, {0, 0, 0}},	//face=0
								{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}}, 	//face=1
								{{2, 2, 2}, {2, 2, 2}, {2, 2, 2}},	//face=2
								{{3, 3, 3}, {3, 3, 3}, {3, 3, 3}},	//face=3
								{{4, 4, 4}, {4, 4, 4}, {4, 4, 4}}, 	//face=4
								{{5, 5, 5}, {5, 5, 5}, {5, 5, 5}}};	//face=5
	static int[][][] dice = new int[6][3][3];
	
	public static void main(String[] args) throws Exception{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int TC = Integer.parseInt(br.readLine());
		for(int tc=0; tc<TC; tc++) {
			for(int face=0; face<6; face++) {
				for(int row=0; row<3; row++) {
					for(int col=0; col<3; col++) {
						dice[face][row][col] = dice_org[face][row][col];
					}
				}
			}
			int N = Integer.parseInt(br.readLine());
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int n=0; n<N; n++) {
				String move = st.nextToken();
				int face = face_parse(move.charAt(0));
				int direction = direction_parse(move.charAt(1));
				move(face, direction);
				/*for(int row=0; row<3; row++) {
					for(int col=0; col<3; col++) {
						System.out.print(color_parse(dice[0][row][col]));
					}
					System.out.print("\n");
				}
				System.out.print("\n");*/
			}
			for(int row=0; row<3; row++) {
				for(int col=0; col<3; col++) {
					System.out.print(color_parse(dice[0][row][col]));
				}
				System.out.print("\n");
			}
		}
	}
	
	static int face_parse(char f) {
		switch(f) {
		case('U'):
			return 0;
		case('D'):
			return 1;
		case('F'):
			return 2;
		case('B'):
			return 3;
		case('L'):
			return 4;
		default: // case('R'):
			return 5;
		}
	}
	static char color_parse(int c) {
		switch(c) {
		case(0):
			return 'w';
		case(1):
			return 'y';
		case(2):
			return 'r';
		case(3):
			return 'o';
		case(4):
			return 'g';
		default: // case(5):
			return 'b';
		}
	}
	static int direction_parse(char d) {
		switch(d) {
		case('+'):
			return 1;
		default: // case('-'):
			return 0;
		}
	}
	static void move(int face, int direction) {
		// 아랫면(1면)은 위에서 바라본 기준으로 했으므로 회전 방향을 반대로 생각해야 함
		// 이거 때매 헷갈려서 한참 삽질함
		if(face==1) {
			direction = (direction+1)%2;	// 0->1->0
		}
		own_face_change(face, direction);
		if(face==0 && direction==1) {
			other_face_change("3r0", "5r0", "2r0", "4r0");
		}
		else if(face==0 && direction==0) {
			other_face_change("4r0", "2r0", "5r0", "3r0");
		}
		else if(face==1 && direction==1) {
			other_face_change("5r2", "2r2", "4r2", "3r2");
		}
		else if(face==1 && direction==0) {
			other_face_change("3r2", "4r2", "2r2", "5r2");
		}
		else if(face==2 && direction==1) {
			other_face_change("0r2", "5c0~", "1r2", "4c2~");
		}
		else if(face==2 && direction==0) {
			other_face_change("4c2", "1r2~", "5c0", "0r2~");
		}
		else if(face==3 && direction==1) {
			other_face_change("0r0~", "4c0", "1r0~", "5c2");
		}
		else if(face==3 && direction==0) {
			other_face_change("5c2~", "1r0", "4c0~", "0r0");
		}
		else if(face==4 && direction==1) {
			other_face_change("0c0", "2c0~", "1c0", "3c2~");
		}
		else if(face==4 && direction==0) {
			other_face_change("3c2", "1c0~", "2c0", "0c0~");
		}
		else if(face==5 && direction==1) {
			other_face_change("0c2~", "3c0", "1c2~", "2c2");
		}
		else{ //else if(face==5 && direction==0) {
			other_face_change("2c2~", "1c2", "3c0~", "0c2");
		}
	}
	
	static void other_face_change(String first, String second, String third, String fourth) {
		int[] tmp = new int[3];
		int fourth_face = fourth.charAt(0)-'0';
		if(fourth.charAt(1)=='r') {
			int row_num = fourth.charAt(2)-'0';
			tmp[0] = dice[fourth_face][row_num][0];
			tmp[1] = dice[fourth_face][row_num][1];
			tmp[2] = dice[fourth_face][row_num][2];
		}
		else {
			int col_num = fourth.charAt(2)-'0';
			tmp[0] = dice[fourth_face][0][col_num];
			tmp[1] = dice[fourth_face][1][col_num];
			tmp[2] = dice[fourth_face][2][col_num];			
		}
		if(fourth.length()==4) {		//'~'로 끝나면 0과 2번째 교체
			int temp = tmp[0];
			tmp[0] = tmp[2];
			tmp[2] = temp;
		}
		
		overwrite_face(third, fourth);
		overwrite_face(second, third);
		overwrite_face(first, second);
		
		int first_face = first.charAt(0)-'0';
		if(first.charAt(1)=='r') {
			int row_num = first.charAt(2)-'0';
			dice[first_face][row_num][0] = tmp[0];
			dice[first_face][row_num][1] = tmp[1];
			dice[first_face][row_num][2] = tmp[2];
		}
		else {
			int col_num = first.charAt(2)-'0';
			dice[first_face][0][col_num] = tmp[0];
			dice[first_face][1][col_num] = tmp[1];
			dice[first_face][2][col_num] = tmp[2];	
		}
	}
	
	static void overwrite_face(String from, String to) {
		int[] tmp = new int[3];
		
		int from_face = from.charAt(0)-'0';
		if(from.charAt(1)=='r') {
			int row_num = from.charAt(2)-'0';
			tmp[0] = dice[from_face][row_num][0];
			tmp[1] = dice[from_face][row_num][1];
			tmp[2] = dice[from_face][row_num][2];
		}
		else {
			int col_num = from.charAt(2)-'0';
			tmp[0] = dice[from_face][0][col_num];
			tmp[1] = dice[from_face][1][col_num];
			tmp[2] = dice[from_face][2][col_num];			
		}
		if(from.length()==4) {	//'~'로 끝나면 0과 2번째 교체
			int temp = tmp[0];
			tmp[0] = tmp[2];
			tmp[2] = temp;
		}

		int to_face = to.charAt(0)-'0';
		if(to.charAt(1)=='r') {
			int row_num = to.charAt(2)-'0';
			dice[to_face][row_num][0] = tmp[0];
			dice[to_face][row_num][1] = tmp[1];
			dice[to_face][row_num][2] = tmp[2];
		}
		else {
			int col_num = to.charAt(2)-'0';
			dice[to_face][0][col_num] = tmp[0];
			dice[to_face][1][col_num] = tmp[1];
			dice[to_face][2][col_num] = tmp[2];
		}
	}
	
	static void own_face_change(int face, int direction) {
		int tmp00 = dice[face][0][0];
		int tmp01 = dice[face][0][1];
		int tmp02 = dice[face][0][2];
		int tmp10 = dice[face][1][0];
		int tmp12 = dice[face][1][2];
		int tmp20 = dice[face][2][0];
		int tmp21 = dice[face][2][1];
		int tmp22 = dice[face][2][2];
		if(direction==0) {
			dice[face][0][0] = tmp02;
			dice[face][0][1] = tmp12;
			dice[face][0][2] = tmp22;
			dice[face][1][0] = tmp01;
			dice[face][1][2] = tmp21;
			dice[face][2][0] = tmp00;
			dice[face][2][1] = tmp10;
			dice[face][2][2] = tmp20;
		}
		else {	//direction==1
			dice[face][0][0] = tmp20;
			dice[face][0][1] = tmp10;
			dice[face][0][2] = tmp00;
			dice[face][1][0] = tmp21;
			dice[face][1][2] = tmp01;
			dice[face][2][0] = tmp22;
			dice[face][2][1] = tmp12;
			dice[face][2][2] = tmp02;
		}
	}
}
