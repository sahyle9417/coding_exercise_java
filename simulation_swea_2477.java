package coding_exercise_java;

import java.io.*;
import java.util.*;



public class simulation_swea_2477 {
	
	static class staff{
		int need_time;
		int remain_time;
		consumer c = null;
		staff(int need_time, int remain_time){
			this.need_time = need_time;
			this.remain_time = remain_time;
		}
	}

	static class consumer{
		int id;
		int arrive_time;
		int recept_id;
		int repair_id;
		consumer(int id, int arrive_time){
			this.id = id;
			this.arrive_time = arrive_time;
		}
	}
	
	public static void main(String[] args) throws Exception{
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());	// 접수 창구 수
			int M = Integer.parseInt(st.nextToken());	// 정비 창구 수
			int K = Integer.parseInt(st.nextToken());	// 고객 수
			int A = Integer.parseInt(st.nextToken());	// 사용 고객 내역이 알고 싶은 접수 창수
			int B = Integer.parseInt(st.nextToken());	// 사용 고객 내역이 알고 싶은 정비 창수
			
			// 접수 담당자 객체 배열
			staff[] recept_array = new staff[N+1];
			st = new StringTokenizer(br.readLine());
			for(int n=1; n<=N; n++) {
				int need_time = Integer.parseInt(st.nextToken());
				recept_array[n] = new staff(need_time, 0);
			}

			// 정비 담당자 객체 배열
			staff[] repair_array = new staff[N+1];
			st = new StringTokenizer(br.readLine());
			for(int m=1; m<=M; m++) {
				int need_time = Integer.parseInt(st.nextToken());
				repair_array[m] = new staff(need_time, 0);
			}

			// 고객 리스트
			LinkedList<consumer> consumer_q = new LinkedList<>();
			st = new StringTokenizer(br.readLine());
			for(int k=1; k<=K; k++) {
				int arrive_time = Integer.parseInt(st.nextToken());
				consumer_q.add(new consumer(k, arrive_time));
			}

			LinkedList<consumer> recept_q = new LinkedList<>();	// 접수 대기큐
			LinkedList<consumer> repair_q = new LinkedList<>();	// 수리 대기큐
			LinkedList<consumer> answer_q = new LinkedList<>();	// 수리 완료된 고객 중 A,B 조건에 해당하는 고객 큐
			
			// 처음에 창구에서 작업 중인 고객 수 체크 안해서 삽질했었음, 문제 흐름에 집중해서 놓치는 부분 없도록 하자
			int recept_wip = 0;	// 접수 진행 중인 고객 수
			int repair_wip = 0;	// 수리 진행 중인 고객 수
			
			int time = 0;
			// 도착전 고객 큐(consumer_q), 접수 대기큐(recept_q), 접수 진행중인 사람 수(recept_wip), 수리 대기큐(repair_q) 비워지면 끝내도 됨
			// 수리 진행중인 경우 굳이 수리 끝낼 필요까진 없음, 수리가 진행중이라는 건 수리 창구가 정해졌다는 것이므로 정답에 필요한 A,B 조건 검증 가능하기 때문
			while(!consumer_q.isEmpty() || !recept_q.isEmpty() || recept_wip>0 || !repair_q.isEmpty()) {
				// 접수 대기큐, 수리 대기큐에 있는 고객 서비스 하기 전에 접수 및 수리 완료된 고객 제거
				// 자리 먼저 비워주고 나서 집어넣어야만 한다.
				// 엘레베이터에서 먼저 내려야 탈 수 있듯, 접수 창구에서도 사람 빠져나가고 나서야 들어갈 수 있듯.

				// 접수 완료된 고객 접수 배열(recept_array)에서 빼서 수리 대기큐(repair_q)에 삽입
				for(int i=1; i<=N; i++) {
					staff recept = recept_array[i];
					// 접수 완료된 창구 비우고, 접수 완료된 고객을 수리 대기큐에 삽입
					if(recept.remain_time==0 && recept.c!=null) {
						consumer recept_finish_consumer = recept.c;
						recept.c = null;
						recept_wip--;
						repair_q.add(recept_finish_consumer);
					}
				}

				// 수리 완료된 고객 수리 배열(repair_array)에서 빼서 조건 비교 후 정답 큐(answer_q)에 기록
				for(int i=1; i<=M; i++) {
					staff repair = repair_array[i];
					// 수리 완료된 창구 비우고, 수리 완료된 고객 중 A,B 조건 해당자만 정답 큐에 삽입
					if(repair.remain_time==0 && repair.c!=null) {
						consumer repair_finish_consumer = repair.c;
						repair.c = null;
						repair_wip--;
						if(repair_finish_consumer.recept_id==A && repair_finish_consumer.repair_id==B) {
							answer_q.add(repair_finish_consumer);
						}
					}
				}

				// 해당 시점에 도착한 고객을 접수 대기큐에 추가
				while(!consumer_q.isEmpty()) {
					if(time==consumer_q.peekFirst().arrive_time) {
						recept_q.add(consumer_q.pollFirst());
					}
					else {
						break;
					}
				}
				
				// 접수 창구 비어있는만큼 접수 대기 큐에서 고객 흡수
				for(int i=1; i<=N; i++) {
					staff recept = recept_array[i];
					// 가용 접수 직원(remain_time==0) 있고 접수 대기하는 고객 있다면 고객 흡수 
					if(recept.remain_time==0 && recept.c==null && !recept_q.isEmpty()) {
						consumer c = recept_q.pollFirst();
						c.recept_id = i;
						recept.remain_time = recept.need_time;
						recept.c = c;
						recept_wip++;
					}
				}
				
				// 수리 창구 비어있는만큼 수리 대기 큐에서 고객 흡수
				for(int i=1; i<=M; i++) {
					staff repair = repair_array[i];
					// 가용 접수 직원(remain_time==0) 있고 접수 대기하는 고객 있다면 고객 흡수 
					if(repair.remain_time==0 && repair.c==null && !repair_q.isEmpty()) {
						consumer c = repair_q.pollFirst();
						c.repair_id = i;
						repair.remain_time = repair.need_time;
						repair.c = c;
						repair_wip++;
					}
				}
				
				// 시간 지난 것으로 가정하고 접수 배열과 수리 배열의 remain time 1 빼기
				for(int i=1; i<=N; i++) {
					if(recept_array[i].c==null) {
						continue;
					}
					recept_array[i].remain_time--;
				}
				for(int i=1; i<=M; i++) {
					if(repair_array[i].c==null) {
						continue;
					}
					repair_array[i].remain_time--;
				}
				time++;
			}
			// 고객큐, 접수큐, 수리큐는 모두 비워졌지만 수리중인 고객도 고려해야함
			// 수리 중인 고객이 다 완료될때까지 대기해도 되지만 수리에 들어간 시점에서 이미 답은 나오기 때문에 이렇게 구현함
			for(int i=1; i<=M; i++) {
				if(repair_array[i].c!=null && repair_array[i].c.recept_id==A && i==B) {
					answer_q.add(repair_array[i].c);
				}
			}
			int answer = 0;
			if(answer_q.isEmpty()) {
				answer = -1;
			}
			else {
				for(consumer c : answer_q) {
					answer += c.id;
				}
			}
			System.out.println("#"+tc+" "+answer);
		}
	}
}
