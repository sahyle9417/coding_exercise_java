package coding_exercise_java;

//https://github.com/Gony8477/Total/blob/master/new_Remote.py

import java.util.*;
import java.io.*;

public class simulation_boj_1107 {

	static int N;
	static int N_length;
	static ArrayList<String> valid_digit = new ArrayList<>();
	static int min_distance;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		N = Integer.parseInt(br.readLine());
		
		// 상용로그(log10)로 자릿수 구할 경우 0은 따로 처리해줘야한다.
		// (int)Math.log10(0)+1 == -2147483647
		// (int)Math.log10(1)+1 == 1
		// (int)Math.log10(100000)+1 == 6
		if(N==0) {
			N_length = 1;
		}
		else {
			N_length = (int)Math.log10(N)+1;
		}
		min_distance = Math.abs(N-100);
		int broke_num = Integer.parseInt(br.readLine());

		boolean[] button = new boolean[10];
		Arrays.fill(button, true);

		// 고장난 버튼이 없다면 br.readLine()이 실행되어서는 안된다.
		// 이 경우를 고려해주지 않는다면 85%까지 잘 진행된다가 런타임오류가 뜬다.
		if(broke_num!=0) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int tmp=0; tmp<broke_num; tmp++) {
				button[Integer.parseInt(st.nextToken())] = false;
			}
		}

		for(int tmp=0; tmp<=9; tmp++) {
			if(button[tmp]) {
				valid_digit.add(String.valueOf(tmp));
			}
		}
		recursive_make_number("");
		System.out.println(min_distance);
	}
	
	static void recursive_make_number(String number_string) {
		for(String digit : valid_digit) {
			String new_number_string = number_string + digit;	// 숫자 연산으로 해도 되지만 문자열 concat이 더 쉽다.
			int new_number_int = Integer.parseInt(new_number_string);
			//System.out.println(new_number_int);
			int distance = Math.abs(N - new_number_int) + new_number_string.length();
			min_distance = Math.min(min_distance, distance);
			if(new_number_string.length() < 6 && new_number_string.length() <= N_length) {
				// 채널 상한인 500,000의 길이(6자리)와 같아지면 더 긴 숫자(7자리) 만들어볼 필요없음
				// N과 길이 같으면(N_length) 한자리수 더 긴 숫자(length==N_length+1)까지는 만들어볼 필요있음 (N=999일때, 1000과의 거리는 계산 필요)
				// 단, N보다 두 자리수 더 긴 숫자는 만들어볼 필요없음
				recursive_make_number(new_number_string);
			}
		}
	}
}
