package coding_exercise_java;

import java.io.BufferedReader;
import java.io.InputStreamReader;

//n가지 종류의 동전이 있다. 각각의 동전이 나타내는 가치는 다르다.
//이 동전을 적당히 사용해서, 그 가치의 합이 k원이 되도록 하고 싶다. 그 경우의 수를 구하시오. 각각의 동전은 몇 개라도 사용할 수 있다.
//사용한 동전의 구성이 같은데, 순서만 다른 것은 같은 경우이다.
//첫째 줄에 n, k가 주어진다. (1 ≤ n ≤ 100, 1 ≤ k ≤ 10,000)
//다음 n개의 줄에는 각각의 동전의 가치가 주어진다. 동전의 가치는 100,000보다 작거나 같은 자연수이다.

public class dp_boj_2293 {
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String tmp = br.readLine();
		int n = Integer.parseInt(tmp.split(" ")[0]);		//동전의 종류
		int k = Integer.parseInt(tmp.split(" ")[1]);		//만들고 싶은 금액
		int[] coin = new int[n];								//동전 리스트

		for(int i=0; i<n; i++) {
			coin[i] = Integer.parseInt(br.readLine());
		}
		
		int[] output = new int[k+1];		//0~k원을 만드는 경우의 수를 기록
		output[0] = 1;						//0원을 만드는 경우의 수는 항상 1가지
		
		for(int i=0; i<n; i++) {			//새로운 종류의 (i번째) 동전을 추가
			if(coin[i] <= k) {
				output[coin[i]]++;			//추가된 동전 1개만 사용한 경우 나오는 금액에 경우의 수 1 추가 (이것 안하면 배열이 채워지지 않음)
			}
			for(int j=1; j<=k; j++) {
				if(j+coin[i] <= k) {
					output[j+coin[i]] += output[j];	//i-1번째 동전까지만 사용해서 해당 금액(j+coin[i])을 만드는 경우의 수에 i번째 동전까지 사용해서 만드는 경우의 수 추가
				}
			}
		}
		System.out.println(output[k]);
	}
}
