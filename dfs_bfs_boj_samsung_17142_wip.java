package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_bfs_boj_samsung_17142_wip {
	
	static class ij{
		int i;
		int j;
		ij(int i, int j){
			this.i = i;
			this.j = j;
		}
	}
	
	static int N;
	static int M;
	static int[][] map_org;
	static LinkedList<ij> v_list;
	static int all_v_num;
	static boolean[] v_choose;
	static int answer;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		map_org = new int[N+1][N+1];
		
		v_list = new LinkedList<>();
		
		for(int i=1; i<=N; i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map_org[i][j] = Integer.parseInt(st.nextToken());
				if(map_org[i][j]==2) {
					v_list.add(new ij(i, j));
				}
			}
		}

		all_v_num = v_list.size();
		v_choose = new boolean[all_v_num];
		
		answer = Integer.MAX_VALUE;
		dfs(0, 0);
		if(answer==Integer.MAX_VALUE) {
			answer = -1;
		}
		System.out.println(answer);
		
	}
	
	static void dfs(int index, int num) {
		if(num==M) {
			/*for(boolean b : v_choose) {
				if(b) {
					System.out.print("o ");
				}
				else {
					System.out.print("x ");				
				}
			}
			System.out.print("\n");*/
			calc();
		}
		if(index<all_v_num) {
			v_choose[index] = true;
			dfs(index+1, num+1);
			
			v_choose[index] = false;
			dfs(index+1, num);
		}
	}

	static int[] di = {-1, 1, 0, 0};
	static int[] dj = {0, 0, -1, 1};
	
	static void calc() {
		
		int[][] map = new int[N+1][N+1];
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				map[i][j] = map_org[i][j];
			}
		}
		
		boolean[][] visit = new boolean[N+1][N+1];
		
		LinkedList<ij> bfs_q = new LinkedList<>();
		
		for(int i=0; i<all_v_num; i++) {
			if(v_choose[i]) {
				ij v = v_list.get(i);
				bfs_q.add(v);
				visit[v.i][v.j] = true;
				map[v.i][v.j] = 3;
			}
		}
		
		int time = 0;
		boolean finish = true;
		
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				if(map[i][j]==0 || map[i][j]==2) {
					finish = false;
					break;
				}
			}
		}
		
		if(finish) {
			answer = Math.min(answer, time);
			return;
		}
		
		time++;
		
		while(!bfs_q.isEmpty()) {
			for(boolean b : v_choose) {
				if(b) {
					System.out.print("o ");
				}
				else {
					System.out.print("x ");				
				}
			}
			
			int q_size = bfs_q.size();
			for(int qs=0; qs<q_size; qs++) {
				ij from = bfs_q.poll();
				for(int d=0; d<4; d++) {
					int to_i = from.i+di[d];
					int to_j = from.j+dj[d];
					if(to_i<1 || to_i>N || to_j<1 || to_j>N || visit[to_i][to_j]) {
						continue;
					}
					if(map[to_i][to_j]==1) {
						visit[to_i][to_j] = true;
						continue;
					}
					if(map[to_i][to_j]==0 || map[to_i][to_j]==2) {
						visit[to_i][to_j] = true;
						map[to_i][to_j] = 3;
						bfs_q.add(new ij(to_i, to_j));
					}
				}
			}
			System.out.println("\nt:"+time);
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					System.out.print(map[i][j]+" ");
				}
				System.out.print("\n");
			}
			finish = true;
			
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					if(map[i][j]==0 || map[i][j]==2) {
						finish = false;
						break;
					}
				}
			}
			if(finish) {
				if(time<answer) {
					answer = time;
					/*for(int i=1; i<=N; i++) {
						for(int j=1; j<=N; j++) {
							System.out.print(map[i][j]+" ");
						}
						System.out.print("\n");
					}*/
				}
				return;
			}
			else{
				time++;
			}
		}
		
	}
}
