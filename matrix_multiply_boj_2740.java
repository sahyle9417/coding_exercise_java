package coding_exercise_java;
import java.io.*;
import java.util.*;

//N*M크기의 행렬 A와 M*K크기의 행렬 B가 주어졌을 때, 두 행렬을 곱하는 프로그램을 작성하시오.
//첫째 줄에 행렬 A의 크기 N 과 M이 주어진다.
//둘째 줄부터 N개의 줄에 행렬 A의 원소 M개가 순서대로 주어진다.
//그 다음 줄에는 행렬 B의 크기 M과 K가 주어진다.
//이어서 M개의 줄에 행렬 B의 원소 K개가 차례대로 주어진다. 

public class matrix_multiply_boj_2740 {
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		String nm = br.readLine();
		int n = Integer.parseInt(nm.split(" ")[0]);
		int m = Integer.parseInt(nm.split(" ")[1]);
		int[][] first_matrix = new int[n][m];
		for(int i=0; i<n; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=0; j<m; j++) {
				first_matrix[i][j] = Integer.parseInt(st.nextToken());
				//System.out.println("1st["+i+"]["+j+"]="+first_matrix[i][j]);
			}
		}
		
		String mk = br.readLine();
		int m2 = Integer.parseInt(mk.split(" ")[0]);
		if(m!=m2) {
			System.out.println("invalid matrix size");
			return;
		}
		int k = Integer.parseInt(mk.split(" ")[1]);
		int[][] second_matrix = new int[m][k];
		int[][] result_matrix = new int[n][k];
		for(int i=0; i<m; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=0; j<k; j++) {
				second_matrix[i][j] = Integer.parseInt(st.nextToken());
				//System.out.println("2nd["+i+"]["+j+"]="+second_matrix[i][j]);
			}
		}

		for(int i=0; i<n; i++) {
			for(int j=0; j<k; j++) {
				for(int l=0; l<m; l++) {
					result_matrix[i][j] += first_matrix[i][l]*second_matrix[l][j];
					//System.out.println("result["+i+"]["+j+"]="+result_matrix[i][j]);
				}
				System.out.print(result_matrix[i][j]+" ");
			}
			System.out.println();
		}
		
	}
}
