package coding_exercise_java;
import java.util.Scanner;

//첫째 줄에 A와 B가 주어진다. (0 < A, B < 10)
//첫째 줄에 A+B를 출력한다.

public class scanner_boj_1000 {
	public static void main(String[] args){
		int int_a, int_b;
		Scanner scn = new Scanner(System.in);
		
		/*
		byte byt = scn.nextByte();
 		short srt = scn.nextShort();
		long lng = scn.nextLong();
 		float flt = scn.nextFloat();
		double dbl = scn.nextDouble();
		boolean ble = scn.nextBoolean(); 
		String word = scn.next(); //공백이 나올때까지 문자 입력받기
		String line = scn.nextLine(); //엔터가 나올때까지 문자열 입력받기
		*/
		
		
		int_a = scn.nextInt(); //공백 또는 엔터가 나올때까지 int 입력받기
		int_b = scn.nextInt();
		scn.close();
		System.out.println(int_a+int_b);
	}
}
