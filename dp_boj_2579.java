package coding_exercise_java;

import java.io.BufferedReader;
import java.io.InputStreamReader;

//계단 오르기 게임은 계단 아래 시작점부터 계단 꼭대기에 위치한 도착점까지 가는 게임이다.
//각각의 계단에는 일정한 점수가 쓰여 있는데 계단을 밟으면 그 계단에 쓰여 있는 점수를 얻게 된다.
//계단 오르는 데는 다음과 같은 규칙이 있다.
//규칙 1 : 계단은 한 번에 한 계단씩 또는 두 계단씩 오를 수 있다. 즉, 한 계단을 밟으면서 이어서 다음 계단이나, 다음 다음 계단으로 오를 수 있다.
//규칙 2 : 연속된 세 개의 계단을 모두 밟아서는 안 된다. 단, 시작점은 계단에 포함되지 않는다.
//규칙 3 : 마지막 도착 계단은 반드시 밟아야 한다.
//각 계단에 쓰여 있는 점수가 주어질 때 이 게임에서 얻을 수 있는 총 점수의 최댓값을 구하는 프로그램을 작성하시오.

public class dp_boj_2579 {
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		int stair_length = Integer.parseInt(br.readLine());
		int[] stair = new int[stair_length];
		int[] output = new int[stair_length];
		for (int i = 0; i < stair.length; i++) {
			stair[i] = Integer.parseInt(br.readLine());
		}
		if (stair.length >= 1) {
			output[0] = stair[0];
		}
		if (stair.length >= 2) {
			output[1] = stair[0] + stair[1];
		}
		if (stair.length >= 3) {
			output[2] = Math.max(stair[0] + stair[2], stair[1] + stair[2]);
		}

		for (int i = 3; i < output.length; i++) {
			output[i] = Math.max(output[i - 3] + stair[i - 1] + stair[i], output[i - 2] + stair[i]);
			// 마지막 계단은 밟아야 하므로 항상 포함시켜주고 아래 경우의 수 중에서 더 큰 것을 기록
			// 경우의 수 1) 전전전계단까지의 최적에 전전계단 안밟고 전계단 밟는 경우
			// 경우의 수 2) 전전계단까지의 최적에 전계단을 안밟는 경우
		}
		System.out.println(output[stair_length - 1]);
	}
}
