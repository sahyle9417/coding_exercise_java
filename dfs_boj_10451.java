package coding_exercise_java;

import java.util.*;
import java.io.*;

public class dfs_boj_10451 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		// TC:테스트 케이스 개수
		int TC = Integer.parseInt(br.readLine());
		for(int tc=0; tc<TC; tc++) {
			
			// N:정점 개수
			int N = Integer.parseInt(br.readLine());
			// map:연결관계 기록
			int[] map = new int[N+1];
			// visit:방문여부 기록
			int[] visit = new int[N+1];

			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int n=1; n<=N; n++) {
				map[n] = Integer.parseInt(st.nextToken());
			}
			
			/*for(int n=1; n<=N; n++) {	
				System.out.print(map[n]+" ");
			}*/
			
			int cycle = 0;
			
			for(int n=1; n<=N; n++) {
				if(visit[n]==1) {
					continue;
				}
				int temp = n;
				while(visit[temp]==0) {
					visit[temp] = 1;
					temp = map[temp];
				}
				cycle++;
			}
			
			System.out.println(cycle);
		}
	}
}
