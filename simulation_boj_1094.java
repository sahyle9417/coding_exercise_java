package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_1094 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int X = Integer.parseInt(br.readLine());
		int ret = 0;
		while(X>0) {
			int pow2 = 1;
			while(pow2<=X) {
				pow2 *= 2;
			}
			X -= (pow2/2);
			ret++;
		}
		System.out.println(ret);
	}
}
