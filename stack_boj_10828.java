package coding_exercise_java;
import java.util.Stack;
import java.io.*;

//첫째 줄에 주어지는 명령의 수 N (1 ≤ N ≤ 10,000)이 주어진다. 둘째 줄부터 N개의 줄에는 명령이 하나씩 주어진다. 
//push 숫자 : 스택에 삽입
//pop : 스택에서 빼며 출력 (스택이 비었다면 -1 출력)
//top : 스택 맨위에 있는 값 출력 (스택이 비었다면 -1 출력)
//size : 스택의 크기 출력
//empty : 스택이 비었다면 1 출력, 그렇지 않으면 0 출력


public class stack_boj_10828 {
	public static void main(String[] args) {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		try {
			int command_num = Integer.parseInt(br.readLine());
			String[] command = new String[command_num];
			for(int i=0; i<command.length; i++) {
				command[i]=br.readLine();
			}

			br.close();
			isr.close();
			
			Stack<Integer> stack = new Stack<>();
			
			for(int i=0; i<command.length; i++) {
				if(command[i].split(" ")[0].equals("push")){
					stack.push(Integer.parseInt(command[i].split(" ")[1]));
				}
				else if(command[i].equals("pop")) {
					if(stack.isEmpty()) {	//if문과 else문을 System.out.println(stack.isEmpty()?-1:stack.pop());로 대체가능
						System.out.println(-1);
					}
					else {
						System.out.println(stack.pop());
					}
				}
				else if(command[i].equals("top")) {
					if(stack.isEmpty()) {	//if문과 else문을 System.out.println(stack.isEmpty()?-1:stack.peek());로 대체가능
						System.out.println(-1);
					}
					else {
						System.out.println(stack.peek());
					}
				}
				else if(command[i].equals("size")) {
					System.out.println(stack.size());
				}
				else if(command[i].equals("empty")) {
					if(stack.isEmpty()) {	//if문과 else문을 System.out.println(stack.isEmpty()?1:0);로 대체가능
						System.out.println(1);
					}
					else {
						System.out.println(0);
					}
				}
			}
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
}
