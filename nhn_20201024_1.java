package coding_exercise_java;

import java.util.Scanner;
import java.util.*;
import java.lang.StringBuilder;

class nhn_20201024_1 {
	private static void solution(int numOfOrder, String[] orderArr) {
		for (int orderIdx = 0; orderIdx < numOfOrder; orderIdx++) {
			String order = orderArr[orderIdx];
			System.out.println(recursive_solution(order, 0, order.length() - 1));
		}
	}
	private static String recursive_solution(String order, int start_idx, int end_idx) {
		System.out.println(start_idx + "-" + end_idx);
		StringBuilder sb = new StringBuilder();
		if(start_idx == end_idx) {
			sb.append(order.charAt(start_idx));
		}
		while(start_idx < end_idx) {
			char c = order.charAt(start_idx);
			// 문자로 시작
			if(c == 'R' || c == 'G' || c == 'B') {
				// 문자 뒤에 괄호 -> 문자 분배
				if(order.charAt(start_idx + 1) == '(') {
					int end_idx_tmp = start_idx + 2;
					while(order.charAt(end_idx_tmp) != ')') {
						end_idx_tmp++;
					}
					String order_tmp = recursive_solution(order, start_idx + 2, end_idx_tmp - 1);
					for(int i=0; i<order_tmp.length(); i++) {
						sb.append(c);
						sb.append(order_tmp.charAt(i));
					}
					start_idx = end_idx_tmp;
					continue;
				}
				// 문자 뒤에 괄호 없음, 문자 최종 결과에 추가하고 한칸 이동
				else {
					sb.append(c);
					start_idx++;
					System.out.println(start_idx + "-" + end_idx);
					continue;
				}
			}
			// 숫자로 시작
			else {
				int num = order.charAt(start_idx) - '0';
				// 숫자 뒤에 괄호 -> 반복
				if(order.charAt(start_idx + 1) == '(') {
					int end_idx_tmp = start_idx + 2;
					while(order.charAt(end_idx_tmp) != ')') {
						end_idx_tmp++;
					}
					String order_tmp = recursive_solution(order, start_idx + 2, end_idx_tmp - 1);
					for(int i=0; i<num; i++) {
						sb.append(order_tmp);
					}
					start_idx = end_idx_tmp;
					continue;
				}
				// 숫자 뒤에 괄호 없음, 문자 바로 곱하기
				else {
					for(int i=0; i<num; i++) {
						sb.append(order.charAt(start_idx + 1));
					}
				}
				
			}
		}
		System.out.println(sb.toString());
		return sb.toString();
	}

	private static class InputData {
		int numOfOrder;
		String[] orderArr;
	}

	private static InputData processStdin() {
		InputData inputData = new InputData();

		try (Scanner scanner = new Scanner(System.in)) {
			inputData.numOfOrder = Integer.parseInt(scanner.nextLine().replaceAll("\\s+", ""));

			inputData.orderArr = new String[inputData.numOfOrder];
			for (int i = 0; i < inputData.numOfOrder; i++) {
				inputData.orderArr[i] = scanner.nextLine().replaceAll("\\s+", "");
			}
		} catch (Exception e) {
			throw e;
		}

		return inputData;
	}

	public static void main(String[] args) throws Exception {
		InputData inputData = processStdin();
		solution(inputData.numOfOrder, inputData.orderArr);
	}
}