package coding_exercise_java;

import java.io.*;

public class linked_list_leet_easy_Merge_Two_Sorted_Lists {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int len1 = Integer.parseInt(br.readLine());
		ListNode[] list1 = new ListNode[len1];
		for (int i = 0; i < len1; i++) {
			int val = Integer.parseInt(br.readLine());
			list1[i] = new ListNode(val);
			if (i != 0) {
				list1[i - 1].next = list1[i];
			}
		}

		int len2 = Integer.parseInt(br.readLine());
		ListNode[] list2 = new ListNode[len2];
		for (int i = 0; i < len2; i++) {
			int val = Integer.parseInt(br.readLine());
			list2[i] = new ListNode(val);
			if (i != 0) {
				list2[i - 1].next = list2[i];
			}
		}
		ListNode node1;
		ListNode node2;
		if (len1 == 0) {
			node1 = null;
		} else {
			node1 = list1[0];
		}
		if (len2 == 0) {
			node2 = null;
		} else {
			node2 = list2[0];
		}

		linked_list_leet_easy_Merge_Two_Sorted_Lists_Solution solution = new linked_list_leet_easy_Merge_Two_Sorted_Lists_Solution();
		ListNode ret = solution.mergeTwoLists(node1, node2);
		while (ret != null) {
			System.out.println(ret.val);
			ret = ret.next;
		}
	}
}

/*
class ListNode {
	int val;
	ListNode next;
	ListNode() {}
	ListNode(int val) {
		this.val = val;
	}
	ListNode(int val, ListNode next) {
		this.val = val;
		this.next = next;
	}
}
*/

class linked_list_leet_easy_Merge_Two_Sorted_Lists_Solution {
	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		// dummy node
		ListNode head_prev = new ListNode(0, null);
		ListNode tail = head_prev;

		while (l1 != null && l2 != null) {
			if (l1.val < l2.val) {
				tail.next = l1;
				tail = tail.next;
				l1 = l1.next;
			}
			else if (l1.val == l2.val) {
				// 이 부분에서 l1추가 로직 다 끝나고나서 l2추가 로직 시작해야 하는 것 유의
				tail.next = l1;
				tail = tail.next;
				l1 = l1.next;

				tail.next = l2;
				tail = tail.next;
				l2 = l2.next;
			}
			else if (l1.val > l2.val) {
				tail.next = l2;
				tail = tail.next;
				l2 = l2.next;
			}
		}
		if (l1 != null) {
			tail.next = l1;
		}
		else if (l2 != null) {
			tail.next = l2;
		}
		return head_prev.next;
	}
	/*
	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		// dummy node
		ListNode head_prev = new ListNode(-1);
		ListNode tail = head_prev;

		while (true) {
			if (l1 == null) {
				tail.next = l2;
				break;
			}
			if (l2 == null) {
				tail.next = l1;
				break;
			}
			if (l1.val < l2.val) {
				tail.next = l1;
				l1 = l1.next;
			}
			 else {
				tail.next = l2;
				l2 = l2.next;
			}
			tail = tail.next;
		}
		return head_prev.next;
	}
	*/
}