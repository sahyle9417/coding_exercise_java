package coding_exercise_java;

import java.io.*;

public class string_leet_easy_Reverse_Integer {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		int input = Integer.parseInt(str);
		string_leet_easy_Reverse_Integer_Solution solution = new string_leet_easy_Reverse_Integer_Solution();
		int ret = solution.reverse(input);
		System.out.println(ret);
	}
}

class string_leet_easy_Reverse_Integer_Solution {

	// long을 사용할 수 있는 쉬운 풀이
	public int reverse(int x) {
		long rev = 0;

		while (x != 0) {
			rev = rev * 10 + x % 10;
			x = x / 10;
		}

		if (rev > Integer.MAX_VALUE || rev < Integer.MIN_VALUE) {
			return 0;
		}

		return (int) rev;

	}

	// int만을 사용하는 어려운 풀이
	// 연산 전후로 부호가 바뀌었다면 Overflow로 판단하려고 했으나
	// Overflow 발생 후에도 부호 유지하는 것이 가능 (1534236469)
	// 양수인데 연산 후에 작아지거나, 음수인데 연산 후에 커지는 경우를 Overflow로 판단하려고 했으나,
	// Overflow 발생 후에도 증감 방향을 유지하는 것이 가능 (1534236469)
	// 그냥 연산 전에 연산 후의 결과가 넘칠지 미리 검사하는 것이 정석
	// 곱셈 전 검사와 덧셈 전 검사를 나눠서 수행해야 함
	// 곱셈 전 검사와 덧셈 전 검사를 한번에 수행하면 Overflow 아닌데 Overflow로 판별 (1463847412)
	/*
	public int reverse(int x) {
		//final int MAX = 2147483647;
		//final int MIN = -2147483648;
		int ret = 0;
		while (x != 0) {
			if(ret > Integer.MAX_VALUE / 10 || ret < Integer.MIN_VALUE / 10) {
				return 0;
			}
			ret *= 10;
			if(ret > 0 && ret > Integer.MAX_VALUE - (x % 10)) {
				return 0;
			}
			else if(ret < 0 && ret < Integer.MIN_VALUE - (x % 10)) {
				return 0;
			}
			ret += x % 10;
			x /= 10;
			//System.out.println(x + "," + ret);
		}
		return ret;
	}
	*/
}