package coding_exercise_java;

class Tree2{
	class Node{
		int data;
		Node(int data){
			this.data=data;
		}
		Node left;
		Node right;
	}
	Node root;
	
	public void make_tree(int[] input_array) {
		root = make_tree_recursive(input_array, 0, input_array.length-1);
	}
	
	public Node make_tree_recursive(int[] input_array, int start, int end) {
		if(start>end) {
			return null;
		}
		int mid = (start+end)/2;
		Node node = new Node(input_array[mid]);
		node.left = make_tree_recursive(input_array, start, mid-1);
		node.right = make_tree_recursive(input_array, mid+1, end);
		return node;
	}
	
	public void tree_search(Tree2 tree, int find) {
		tree_search_recursive(tree.root, find);
	}
	
	public void tree_search_recursive(Node node, int find) {
		if(find > node.data) {
			System.out.println("Data("+find+") is bigger than "+node.data);
			if(node.right == null) {
				System.out.println("No such data");
			}
			else {
				tree_search_recursive(node.right, find);
			}
		}
		else if(find < node.data){
			System.out.println("Data("+find+") is smaller than "+node.data);
			if(node.left == null) {
				System.out.println("No such data");
			}
			else {
				tree_search_recursive(node.left , find);
			}
		}
		else {
			System.out.println("Data found");
		}
	}
}


public class binary_search_tree2 {
	public static void main(String[] args) {
		int[] input_array = new int[100];
		for(int i=0; i<input_array.length; i++) {
			input_array[i] = i*2;
		}
		Tree2 tree = new Tree2();
		tree.make_tree(input_array);
		tree.tree_search(tree, 150);
	}
}
