package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_swea_5656_solution {
	static int N;
	static int W;
	static int H;
	static int[][] map;
	static int answer;
	public static void main(String[] args) throws Exception {
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			
			st = new StringTokenizer(br.readLine());
			N = Integer.parseInt(st.nextToken());
			W = Integer.parseInt(st.nextToken());
			H = Integer.parseInt(st.nextToken());
			
			map = new int[H+1][W+1];
			for(int h=1; h<=H; h++) {
				st = new StringTokenizer(br.readLine());
				for(int w=1; w<=W; w++) {
					map[h][w] = Integer.parseInt(st.nextToken());
				}
			}
			answer = Integer.MAX_VALUE;
			solve(0);
			System.out.println("#"+tc+" "+answer);
		}
	}
	// n번째 구슬 떨어뜨리기
	static void solve(int n) {
		
		// 남은 벽돌 개수 세기
		// 처음에 n==N일때만 세서 정답 갱신하려고 했는데
		// n<N일때 끝나버리면 정답 갱신이 아예 안 이뤄지는 버그 있었음
		// 이는 열(w)에 벽돌이 남아있지 않은 경우 해당 열에 구슬 떨어뜨리는 재귀호출(solve(n+1))을 스킵했기 때문임
		int count = 0;
		for(int h=1; h<=H; h++) {
			for(int w=1; w<=W; w++) {
				if(map[h][w]!=0) {
					count++;
				}
			}
		}
		// 구슬 N개 다 떨어뜨렸거나 깰 벽돌이 남아있지 않다면 종료
		// 처음에 벽돌이 남아있지 않은 경우(count==0)을 함께 고려하지 않아서
		// 구슬 다 떨어지기 전(n<N)에 벽돌이 다 없어진 경우 정답갱신이 아예 이뤄지지 않던 버그 있었음
		// 이는 열(w)에 벽돌이 남아있지 않은 경우 해당 열에 구슬 떨어뜨리는 재귀호출(solve(n+1))을 스킵했기 때문임
		if(count==0 || n==N) {
			answer = Math.min(count, answer);
			return;
		}
		
		// 아직 떨어뜨릴 구슬 남아있음
		// 구슬 떨어뜨리기 전에 원본(map_before) 백업, 떨어뜨리고 나서 다시 원상복구 할 것임
		int[][] map_backup = copy(map);
		// 열(w)을 바꿔가며 구슬을 한번씩 떨어뜨릴 것임
		for(int w=1; w<=W; w++) {
			// 해당 열(w)에 구슬을 떨어뜨렸을 때 맞닿게 되는 행(h) 구하기
			// 즉, 해당 열(w)에서 가장 위에 위치한 0이 아닌 행(h) 구하기
			int top_h = 1;
			while(top_h<=H && map[top_h][w]==0) {
				top_h++;
			}
			// 해당 열(w)은 벽돌이 남아있지 않으므로(모두 0) 열 스킵
			// 이 구문 때문에 모든 맵에 벽돌이 하나도 남아있지 않을 경우 다음 구슬을 떨어뜨리는 solve(n+1)을 실행시키지 않아서
			// n<N일 때 모든 맵에 벽돌이 하나도 남아있지 않을 경우 정답 갱신이 이뤄지지 않는 버그 있었음
			// 대신 쓸데없는 열에 벽돌을 떨어뜨리지 않아 재귀호출 횟수를 많이 줄여준다.
			if(top_h==H+1) {
				continue;
			}
			// 실제로 벽돌 연쇄적으로 깨기
			break_hw(top_h, w);
			// 빈공간(0) 없게 아래로 끌어내리기
			reset();
			// 다음 구슬 떨어뜨리기
			solve(n+1);
			// 특정 열(w)에서 구슬 떨어뜨리고 나면 (재귀 호출이 끝나면)
			// for문에 의해 다음 열(w+1) 선택해서 구슬 떨어뜨리기 위해서는 w열에서 구슬 떨어뜨리기 전 상태로 원상복구해야함
			map = copy(map_backup);
		}
	}
	
	
	static int[] dh = {1, -1, 0, 0};
	static int[] dw = {0, 0, 1, -1};
	
	// 실제로 map[h][w]칸에 위치한 블록과 연쇄적으로 깨지는 블록들 깨뜨리기
	static void break_hw(int h, int w) {
		if(map[h][w]==0) {
			return;
		}
		// map[h][w] 내용을 break_length에 백업해두고 map[h][w]을 0으로 바꿔줘야 무한 재귀호출이 발생하지 않는다.
		// 출발지에서 주위로 폭발을 퍼뜨리는데 주위에서 다시 출발지로 폭발을 퍼뜨리려고 하기 때문(서로 호출)에 문제가 생기는 것
		// 이미 폭발시킨 곳은 0으로 기록해두면 나중에 다른 칸에서 해당 칸으로 와도 0이 씌여있으니 재귀호출을 발생하지 않는다.
		// map[h][w]을 그대로 두고 재귀호출 시작했다가 무한루프에 빠져서 StackOverflow 나서 한참동안 삽질함
		int break_length = map[h][w];
		map[h][w] = 0;
		for(int bl=1; bl<break_length; bl++) {
			for(int d=0; d<4; d++) {
				int next_h = h+dh[d]*bl;
				int next_w = w+dw[d]*bl;
				// 지도 벗어나지 않는 한에서 1 벽돌 발견 시 map에서 해당 칸만 지우기, 굳이 재귀호출X
				if(check_range(next_h, next_w) && map[next_h][next_w]==1) {
					map[next_h][next_w] = 0;
				}
				// 지도 벗어나지 않는 한에서 2 이상의 벽돌 발견 시 연쇄적(재귀호출)으로 블록 깨뜨리기
				else if(check_range(next_h, next_w) && map[next_h][next_w]>1) {
					break_hw(next_h, next_w);
				}
			}
		}
	}
	/*// 비효율적인 reset 함수
	static void reset() {
		// 열 선택
		for(int w=1; w<=W; w++) {
			// 해당 열의 0을 없애고 아래로 몰아버릴 것임
			for(int h_down=H; h_down>=2; h_down--) {
				// 가장 아래에 위치한 0 찾기 (맨 밑에서부터 위로 올라오며 0 만나기전까지 스킵)
				if(map[h_down][w]!=0) {
					continue;
				}
				// 가장 아래에 위치한 0 발견(h_down), 0에 가장 가까운 0이 아닌 숫자(h_up) 찾기
				for(int h_up=h_down-1; h_up>=1; h_up--) {
					// 0이 아닌 가장 가까운 숫자(h_up) 발견했으니, 해당 숫자를 아래의 0에 기록
					if(map[h_up][w]!=0) {
						map[h_down][w] = map[h_up][w];
						map[h_up][w] = 0;
						break;
					}
				}
			}
		}
	}*/
	static void reset() {
		// 각 열(1~W)마다 큐를 가지며 큐에는 0이 아닌 숫자들을 삽입
		Queue<Integer>[] non_zero_q = new LinkedList[W+1];
		// 각각의 열에 대해 독립적으로 0 아닌 숫자 추출
		for(int w=1; w<=W; w++) {
			non_zero_q[w] = new LinkedList<>();
			// 아래에서 위로 거슬러 올라오며 0이 아닌 숫자만 큐에 삽입 후 지우기
			// 결국 map에는 0만 남게된다
			for(int h=H; h>=1; h--) {
				if(map[h][w]!=0) {
					non_zero_q[w].add(map[h][w]);
					map[h][w] = 0;
				}
			}
		}
		// 각각의 열에 대해 독립적으로 기록
		for(int w=1; w<=W; w++) {
			// 아래에서부터 위로 올라오며 큐 길이만큼 숫자 채우기
			int h = H;
			for(int element : non_zero_q[w]) {
				map[h--][w] = element;
			}
		}
	}
	
	static boolean check_range(int h, int w) {
		if(h>=1 && h<=H && w>=1 && w<=W) {
			return true;
		}
		else {
			return false;
		}
	}
	
	static int[][] copy(int[][] map){
		int[][] map_copy = new int[H+1][W+1];
		for(int h=1; h<=H; h++) {
			for(int w=1; w<=W; w++) {
				map_copy[h][w] = map[h][w];
			}
		}
		return map_copy;
	}
}
