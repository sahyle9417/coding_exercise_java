package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_swea_4012 {
	
	static int N;
	static int[][] map;
	static int answer = Integer.MAX_VALUE;
	
	public static void main(String[] args) throws Exception{
		//System.out.println(System.getProperty("user.dir"));
		
		/*FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);*/
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			
			N = Integer.parseInt(br.readLine());
			map = new int[N+1][N+1];
			
			for(int i=1; i<=N; i++) {
				st = new StringTokenizer(br.readLine());
				for(int j=1; j<=N; j++) {
					map[i][j] = Integer.parseInt(st.nextToken());
				}
			}

			answer = Integer.MAX_VALUE;
			solve();
			System.out.println("#"+tc+" "+answer);
			
		}
	}
	
	static void solve() {
		boolean[] a = new boolean[N+1];
		dfs(a, 0, 1);
	}
	
	static void dfs(boolean[] a, int a_num, int index) {
		// 인덱스 넘어가서까지 재귀호출X
		if(index>N) {
			return;
		}
		// a에 속하는 재료가 전체의 절반 (b에 속하는 재료도 전체의 절반)
		if(a_num == N/2) {
			int a_score = 0;
			int b_score = 0;
			for(int i=1; i<=N; i++) {
				for(int j=i+1; j<=N; j++) {
					// i번째 재료와 j번째 재료 모두 a에 속함
					if(a[i] && a[j]) {
						a_score += map[i][j];
						a_score += map[j][i];
					}
					// i번째 재료와 j번째 재료 모두 b에 속함
					else if(!a[i] && !a[j]) {
						b_score += map[i][j];
						b_score += map[j][i];
					}
				}
			}
			answer = Math.min(Math.abs(a_score-b_score), answer);
		}
		// index번째 재료 a에 속하게 해서 재귀호출
		a[index] = true;
		dfs(a, a_num+1, index+1);

		// index번째 재료 b에 속하게 해서 재귀호출
		a[index] = false;
		dfs(a, a_num, index+1);
	}
}
