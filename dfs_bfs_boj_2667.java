package coding_exercise_java;

import java.util.*;
import java.io.*;

public class dfs_bfs_boj_2667 {
	
	static int n;
	static int[][] map;
	static int[][] visit; 
	static LinkedList<Integer> output = new LinkedList<Integer>();
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		n = Integer.parseInt(br.readLine());
		map = new int[n+1][n+1];
		visit = new int[n+1][n+1];
		
		for(int i=1; i<=n; i++) {
			String temp = br.readLine();
			for(int j=1; j<=n; j++) {
				// char로 표현된 숫자의 아스키 코드값에서 '0'의 아스키 코드값을 빼면 int로 변환할 수 있다.
				map[i][j] = temp.charAt(j-1) - '0';
				// 집이 없는 영역(map에서 0으로 표시된 영역)은 방문하지 않도록 visit 배열에 1을 기록
				if(map[i][j]==0) {
					visit[i][j] = 1;
				}
			}
		}
		
		for(int i=1; i<=n; i++) {
			for(int j=1; j<=n; j++) {
				if(visit[i][j]==0) {
					output.add(0);
					dfs(i, j);
				}
			}
		}
		
		System.out.println(output.size());
		Collections.sort(output);
		for(int element : output) {
			System.out.println(element);
		}
		
		/*for(int i=1; i<=n; i++) {
			for(int j=1; j<=n; j++) {
				System.out.print(map[i][j]);
			}
			System.out.print("\n");
		}*/

	}
	
	public static void dfs(int i, int j) {
		if(visit[i][j]==1) {
			return;
		}
		
		output.set(output.size()-1, output.getLast()+1);
		visit[i][j]=1;
		
		if(i+1<=n && visit[i+1][j]==0) {
			dfs(i+1, j);
		}
		if(i-1>=1 && visit[i-1][j]==0) {
			dfs(i-1, j);
		}
		if(j+1<=n && visit[i][j+1]==0) {
			dfs(i, j+1);
		}
		if(j-1>=1 && visit[i][j-1]==0) {
			dfs(i, j-1);
		}
	}
}
