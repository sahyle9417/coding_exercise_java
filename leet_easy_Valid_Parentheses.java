package coding_exercise_java;

import java.io.*;
import java.util.*;

public class leet_easy_Valid_Parentheses {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		leet_easy_Valid_Parentheses_Solution solution = new leet_easy_Valid_Parentheses_Solution();
		System.out.println(solution.isValid(br.readLine()));
	}
}

class leet_easy_Valid_Parentheses_Solution {

	public boolean isValid(String s) {
		Stack<Character> stack = new Stack<>();

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);

			if (c == '(' || c == '{' || c == '[') {
				stack.add(c);
			}

			if (c == ')' || c == '}' || c == ']') {
				if (stack.isEmpty()) {
					return false;
				}
				char popped = stack.pop();
				if (c == ')' && popped != '(') {
					return false;
				}
				if (c == '}' && popped != '{') {
					return false;
				}
				if (c == ']' && popped != '[') {
					return false;
				}
			}
		}
		// 이 부분 빼먹어서 틀렸었음, 조심하자
		// 다 끝나고 stack이 비어있어야만 Valid
		if (stack.isEmpty()) {
			return true;
		}
		// 다 끝났는데도 stack에 뭔가가 남아있다면 Invalid
		else {
			return false;
		}
	}
}
