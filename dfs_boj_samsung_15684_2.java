package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_boj_samsung_15684_2 {
	
	static int N;
	static int M;
	static int H;
	static boolean[][] map;
	static int min_trial = Integer.MAX_VALUE;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		// 세로선 개수 : N개 (1~N)
		N = Integer.parseInt(st.nextToken());
		// 실제로 초기에 주어진 가로선 개수 : M개
		M = Integer.parseInt(st.nextToken());
		// 가로선 놓을 수 있는 위치 개수 : H개 (1~H)
		H = Integer.parseInt(st.nextToken());
		
		// 가로선 놓을 수 있는 위치 : 1~H(H개)
		// 가로선 시작지점이 될 수 있는 세로선 : 1~N-1(N-1개)
		// 실제로 초기에 주어진 가로선 개수 : M개
		map = new boolean[H+1][N];
		
		for(int m=0; m<M; m++) {
			st = new StringTokenizer(br.readLine());
			int h = Integer.parseInt(st.nextToken());
			int n = Integer.parseInt(st.nextToken());
			map[h][n] = true;
		}
				
		dfs(1, 1, 0);
		
		if(min_trial==Integer.MAX_VALUE) {
			System.out.println(-1);
		}
		else {
			System.out.println(min_trial);
		}
	}

	// dfs에 사다리 놓기 시작한 위치 넘기는 이유 : 사다리 놓는 순서에 따른 경우의 수 중복 방지
	// 현재 사다리 놓은 곳 뒤에서부터 사다리 넣어보면 된다.
	// 사다리 놓을 수 있는 위치가 ABC 순으로 위치해있다면 ABC 순서로 사다리 놓아봤다면,
	// BAC, BCA, CAB, CBA등의 순서로 사다리 놓아볼 필요 없음
	// 즉, 순열(Permutation)이 아니라 조합(Combination)이기 때문
	static void dfs(int start_h, int start_n, int trial) {
		
		//System.out.println("\nstart:"+start_h+","+start_n+" trial:"+trial);
		
		// 현재 케이스가 성공하더라도 min_trial 갱신 못한다면 성공여부 확인 필요X
		if(trial>=min_trial) {
			return;
		}
		// 현재 케이스 성공 여부 확인
		if(move()) {
			min_trial = Math.min(trial, min_trial);
		}
		// 현재 케이스가 3번 시도한 결과물이라면 현재 케이스에서 파생된 케이스들(trial==4) 시도해볼 필요없음
		if(trial==3) {
			return;
		}
		// 현재 케이스에서 파생된 케이스들(trial+1)이 성공하더라도 min_trial 갱신 못한다면 시도해볼 필요X
		if(trial+1>=min_trial) {
			return;
		}
		
		// 직전에 사다리 놓은 곳 뒤에서부터 사다리 넣어보면 된다.
		// 직전에 사다리 놓은 행부터 보면 된다.
		for(int h=start_h; h<=H; h++) {
			// 열 스킵 처리가 약간 까다로운데,
			// 직전에 사다리 놓은 행에서만 열 스킵이 일어난다.
			// 나머지 아래 행들에 대해서는 직전에 사다리 놓은 열보다 작은 열이라고 해서 스킵하면 안된다.
			// 그렇게 하면 좌측 하단 영역은 고려해야하는데 스킵된다.
			for(int n=1; n<=N-1; n++) {
				// 스킵(continue) 조건을 n<=start_n으로 하면 안되는 이유는 맨 처음에 1,1부터 시작하는데
				// 1,1도 사다리 놓을지 말지 판단해야하기 때문이다.
				if(h==start_h && n<start_n) {
					continue;
				}
				// 해당 칸에 이미 사다리 있으면 스킵
				if(map[h][n]) {
					continue;
				}
				// 왼쪽이 존재하며(n>=2) 왼쪽에 사다리도 있다면 현 위치 스킵
				if(n>=2 && map[h][n-1]) {
					continue;
				}
				// 오른쪽이 존재하며(n<=N-2) 오른쪽에 사다리도 있다면 현 위치 스킵
				if(n<=N-2 && map[h][n+1]) {
					continue;
				}
				// 사다리 놓을 수 있는 위치 발견
				// 사다리 삽입 후 DFS 수행, 수행 후에는 사다리 도로 치우기
				map[h][n] = true;
				dfs(h, n, trial+1); // 해당 칸과 바로 다음 칸은 사다리 어차피 못 놓으니 시작 인덱스에서 스킵도 가능
				map[h][n] = false;
			}
		}
	}
	
	
	static boolean move() {
		
		/*for(int h=1; h<=H; h++) {
			for(int n=1; n<=N-1; n++) {
				if(map[h][n]) {
					System.out.print("o ");
				}
				else {
					System.out.print("x ");
				}
			}
			System.out.print("\n");
		}*/
		
		// 시작할 세로줄 하나 정해서 내려가기 시작
		for(int start_n=1; start_n<=N; start_n++) {
			//System.out.print("n:"+start_n);
			// 실시간으로 자신이 위치한 세로줄 위치 바뀌는데 이를 n에 기록할 것임
			int n = start_n;
			// 사다리 분기점 하나씩 통과하며 좌우에 가로줄 있나 검사
			for(int h=1; h<=H; h++) {
				// 왼쪽에서 가로줄 발견, 왼쪽으로 이동
				if(n>=2 && map[h][n-1]) {
					n--;
				}
				// 오른쪽에서 가로줄 발견, 오른쪽으로 이동 (좌우에 둘다 존재하지는 않음)
				else if(n<=N-1 && map[h][n]){
					n++;
				}
			}
			//System.out.println("->"+n);
			// 사다리 다 내려왔으니 초기 세로줄 위치(start_n)와 현재 위치(n) 비교해 다르면 실패
			if(n!=start_n) {
				return false;
			}
		}
		// 모든 세로줄에서 출발지점(start_n)과 도착지점(n)이 같음
		return true;
	}
	
	/*static boolean[][] copy_array(boolean[][] input){
		boolean[][] output = new boolean[input.length][input[0].length];
		for(int i=0; i<input.length; i++) {
			for(int j=0; j<input[0].length; j++) {
				output[i][j] = input[i][j];
			}
		}
		return output;
	}*/
}
