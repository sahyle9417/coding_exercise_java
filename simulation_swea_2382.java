package coding_exercise_java;

import java.io.*;
import java.util.*;

class cluster implements Comparable<cluster>{
	int i;
	int j;
	int num;
	int direction;
	cluster(int i, int j, int num, int direction){
		this.i = i;
		this.j = j;
		this.num = num;
		this.direction = direction;
	}
	@Override
	public int compareTo(cluster c){
		if(this.i < c.i) {
			return -1;
		}
		else if(this.i > c.i) {
			return 1;
		}
		else if(this.j < c.j) {
			return -1;
		}
		else if(this.j > c.j) {
			return 1;
		}
		else if(this.num > c.num) {
			return -1;
		}
		else { // this.i==c.i && this.j==c.j && this.num<=c.num
			return 1;
		}
	}
}

public class simulation_swea_2382 {
	public static void main(String[] args) throws Exception{
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());
			int M = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());
			
			LinkedList<cluster> bfs_q = new LinkedList<>();
			
			for(int k=1; k<=K; k++) {
				st = new StringTokenizer(br.readLine());
				int i = Integer.parseInt(st.nextToken());
				int j = Integer.parseInt(st.nextToken());
				int num = Integer.parseInt(st.nextToken());
				int direction = Integer.parseInt(st.nextToken());
				bfs_q.add(new cluster(i, j, num, direction));
			}

			int[] di = {0, -1, 1, 0, 0};
			int[] dj = {0, 0, 0, -1, 1};
			
			
			int time = 1;
			while(time<=M) {

				// 같은 칸의 미생물들 합치기 여기서 해야할 듯?
				Collections.sort(bfs_q);
				
				/*System.out.println("before");
				for(cluster c : bfs_q) {
					System.out.println(c.i+","+c.j+" d:"+c.direction+" n:"+c.num);
				}*/
				
				
				
				int index = 0;
				while(index<=bfs_q.size()-2) {
					cluster c1 = bfs_q.get(index);
					cluster c2 = bfs_q.get(index+1);
					// 좌표 같은 것 만나면 숫자 합치고 지워버리기
					if(c1.i==c2.i && c1.j==c2.j) {
						c1.num += c2.num;
						bfs_q.remove(index+1);
						continue;
					}
					// 좌표 같지 않음, index 옮기고 계속 진행
					else {
						index++;
						continue;
					}
				}
				
				
				/*System.out.println("after");
				for(cluster c : bfs_q) {
					System.out.println(c.i+","+c.j+" d:"+c.direction+" n:"+c.num);
				}*/
				
				int q_size = bfs_q.size();
				for(int qs=0; qs<q_size; qs++) {

					cluster c = bfs_q.poll();
					c.i += di[c.direction];
					c.j += dj[c.direction];
					// 빨간 시약 닿음
					if(c.i==0 || c.i==N-1 || c.j==0 || c.j==N-1) {
						c.num /= 2;
						if(c.direction%2==1) {
							c.direction++;
						}
						else {
							c.direction--;
						}
					}
					bfs_q.add(c);
				}
				time++;
			}
			int answer = 0;
			for(cluster c : bfs_q) {
				answer += c.num;
			}
			System.out.println("#"+tc+" "+answer);
		}
	}
}
