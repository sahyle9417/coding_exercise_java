package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_swea_4013 {
	
	static LinkedList<Integer>[] gear_status = new LinkedList[4];
	
	public static void main(String[] args) throws Exception{
		/*FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);*/
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		StringTokenizer st;
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			
			int K = Integer.parseInt(br.readLine());
			//gear
			for(int g=0; g<4; g++) {
				gear_status[g] = new LinkedList<>();
				st = new StringTokenizer(br.readLine());
				// blade
				for(int b=0; b<8; b++) {
					gear_status[g].add(Integer.parseInt(st.nextToken()));
				}
			}
			
			for(int k=0; k<K; k++) {
				// 인접한 gear가 함께 움직이는지 기록
				boolean[] move_together = new boolean[3];
				for(int g=0; g<3; g++) {
					// 극이 달라야 같이 움직임
					move_together[g] = (gear_status[g].get(2)!=gear_status[g+1].get(6));
				}
				st = new StringTokenizer(br.readLine());
				int moved_gear = Integer.parseInt(st.nextToken())-1;
				int direction = Integer.parseInt(st.nextToken());
				int[] move_direction = new int[4];
				move_direction[moved_gear] = direction;
				
				for(int g=moved_gear-1; g>=0; g--) {
					if(move_together[g]) {
						move_direction[g] = move_direction[g+1]*(-1);
					}
					else {
						break;
					}
				}
				for(int g=moved_gear+1; g<=3; g++) {
					if(move_together[g-1]) {
						move_direction[g] = move_direction[g-1]*(-1);
					}
					else {
						break;
					}
				}
				for(int g=0; g<4; g++) {
					move_gear(g, move_direction[g]);
				}
			}
			int answer = 0;
			for(int g=0; g<4; g++) {
				if(gear_status[g].get(0)==1) {
					answer += Math.pow(2, g);
				}
			}
			System.out.println("#"+tc+" "+answer);
		}
		
	}
	// 처음에 direction 1이 아니면 다 -1이라고 처리해서 틀렸었음
	// 나같은 경우는 direction 0이면 안돌아가는 로직이 있었는데 이를 무시해서 틀렸던 것임
	static void move_gear(int gear_num, int direction){
		if(direction==1) {
			gear_status[gear_num].addFirst(gear_status[gear_num].removeLast());
		}
		else if(direction==-1) {
			gear_status[gear_num].addLast(gear_status[gear_num].removeFirst());
		}
	}
}
