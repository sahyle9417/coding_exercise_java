package coding_exercise_java;
import java.io.BufferedReader;
import java.io.InputStreamReader;

//RGB거리에 사는 사람들은 집을 빨강, 초록, 파랑중에 하나로 칠하려고 한다.
//그들은 모든 이웃은 같은 색으로 칠할 수 없다는 규칙도 정했다. 집 i의 이웃은 집 i-1과 집 i+1이다. 처음 집과 마지막 집은 이웃이 아니다.
//모든 집을 칠할 때 드는 비용의 최솟값을 구하라.
//첫째 줄에 집의 수 N이 주어진다. 둘째 줄부터 N개의 줄에 각 집을 빨강으로 칠할 때, 초록으로 칠할 때, 파랑으로 칠할 때 드는 비용이 주어진다.

public class dp_boj_1149 {
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		int input_size = Integer.parseInt(br.readLine());
		int[][] input = new int[input_size][3];

		String tmp;
		for(int i=0; i<input_size; i++) {
			tmp = br.readLine();
			for(int j=0; j<3; j++) {
				input[i][j] = Integer.parseInt(tmp.split(" ")[j]);
			}
		}

		for(int i=1; i<input_size; i++) {
			input[i][0] += Math.min(input[i-1][1], input[i-1][2]);
			input[i][1] += Math.min(input[i-1][0], input[i-1][2]);
			input[i][2] += Math.min(input[i-1][0], input[i-1][1]);
		}

		int output = input[input_size-1][0];
		
		for(int j=1; j<3; j++) {
			if(input[input_size-1][j] < output) {
				output = input[input_size-1][j];
			}
		}
		System.out.println(output);

	}		
}
