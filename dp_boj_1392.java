package coding_exercise_java;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class dp_boj_1392 {
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		int n = Integer.parseInt(br.readLine());
		int[][] input = new int[n][n];
		for(int i=0; i<n; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=0; j<=i; j++) {
				input[i][j] = Integer.parseInt(st.nextToken());
			}
		}

		if(n>1) {
			input[1][0]+=input[0][0];
			input[1][1]+=input[0][0];			
		}
		for(int i=2; i<n; i++) {
			input[i][0] += input[i-1][0];
			for(int j=1; j<i; j++) {
				input[i][j] += Math.max(input[i-1][j-1], input[i-1][j]);
			}
			input[i][i] += input[i-1][i-1];
		}
		
		int rtn=input[n-1][0];
		for(int i=1; i<n; i++) {
			if(input[n-1][i] > rtn) {
				rtn = input[n-1][i];
			}
			//rtn = Math.max(rtn, input[n-1][i]);	//if문 대신 이렇게 해도 되는데 메모리, 시간 조금 더 씀
		}
		System.out.println(rtn);
	}
}
