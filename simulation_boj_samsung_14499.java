package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_samsung_14499 {
	
	static int[] dice = {0, 0, 0, 0, 0, 0, 0};	// 주사위 면 표현, 1~6번 인덱스만 사용, 0번 인덱스 사용X
	static int[][] map;
	static int N;
	static int M;
	static int x;
	static int y;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		// x축이 세로(수직), y축이 가로(수평)라서 헷갈렸음, 문제에서 좌표계 확실히 읽어보고 문제 풀자
		// 특히 이 경우에서 테스트 케이스로 걸러지지 않아서 틀렸는데 맞았다고 착각했다.
		x = Integer.parseInt(st.nextToken());
		y = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());
		
		map = new int[N][M];
		for(int n=0; n<N; n++) {
			st = new StringTokenizer(br.readLine());
			for(int m=0; m<M; m++) {
				map[n][m] = Integer.parseInt(st.nextToken());
			}
		}
		
		st = new StringTokenizer(br.readLine());
		for(int k=0; k<K; k++) {
			move(Integer.parseInt(st.nextToken()));
		}
	}
	
	// x축이 세로(수직), y축이 가로(수평)라서 헷갈렸음, 문제에서 좌표계 확실히 읽어보고 문제 풀자
	// 특히 이 경우에서 테스트 케이스로 걸러지지 않아서 틀렸는데 맞았다고 착각했다.
	static int[] dx = {0, 0, 0, -1, 1};
	static int[] dy = {0, 1, -1, 0, 0};
	
	static void move(int direction) {
		
		// 지도 밖으로 벗어나는지 확인
		int x_tmp = x+dx[direction];
		int y_tmp = y+dy[direction];
		if(x_tmp>=0 && x_tmp<N && y_tmp>=0 && y_tmp<M) {
			
			// 지도 상에서의 좌표 변경
			x = x_tmp;
			y = y_tmp;
			
			// 주사위 면 변경하기 위해 이전의 면별 숫자 기록
			int prev_1 = dice[1];
			int prev_2 = dice[2];
			int prev_3 = dice[3];
			int prev_4 = dice[4];
			int prev_5 = dice[5];
			int prev_6 = dice[6];
			
			// 주사위 면 갱신
			if(direction == 1) {		// 동
				dice[1] = prev_4;
				dice[3] = prev_1;
				dice[4] = prev_6;
				dice[6] = prev_3;
			}
			else if(direction == 2) {	// 서
				dice[1] = prev_3;
				dice[3] = prev_6;
				dice[4] = prev_1;
				dice[6] = prev_4;
			}
			else if(direction == 3) {	// 북
				dice[1] = prev_5;
				dice[2] = prev_1;
				dice[5] = prev_6;
				dice[6] = prev_2;
			}
			else {						// 남
				dice[1] = prev_2;
				dice[2] = prev_6;
				dice[5] = prev_1;
				dice[6] = prev_5;
			}

			/*System.out.println("\ndirection:"+direction);
			System.out.println("x:"+x+" y:"+y);
			System.out.println("before");
			for(int i=1; i<=6; i++) {
				System.out.print(dice[i]+" ");
			}
			System.out.print("\n");
			for(int n=0; n<N; n++) {
				for(int m=0; m<M; m++) {
					System.out.print(map[n][m]+" ");
				}
				System.out.print("\n");
			}*/
			
			// 지도에 0 적혀있으면 주사위 바닥면 숫자가 지도에 복사
			if(map[x][y]==0) {
				map[x][y] = dice[6];
			}
			// 지도에 0이 아닌 숫자 적혀있으면 지도에 적힌 숫자가 주사위 바닥면으로 복사되고, 지도에는 0이 적힌다.
			else {
				dice[6] = map[x][y];
				map[x][y] = 0;
			}
			
			/*System.out.println("after");
			for(int i=1; i<=6; i++) {
				System.out.print(dice[i]+" ");
			}
			System.out.print("\n");
			for(int n=0; n<N; n++) {
				for(int m=0; m<M; m++) {
					System.out.print(map[n][m]+" ");
				}
				System.out.print("\n");
			}*/
			
			System.out.println(dice[1]);
		}
	}
}
