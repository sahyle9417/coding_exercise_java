package coding_exercise_java;

import java.io.*;
import java.util.*;

public class bfs_dp_12761 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int A = Integer.parseInt(st.nextToken());
		int B = Integer.parseInt(st.nextToken());
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());

		int[] count = new int[100001];
		Arrays.fill(count, Integer.MAX_VALUE);
		count[N] = 0;
		
		Queue<Integer> q = new LinkedList<>();
		q.add(N);
		
		while(!q.isEmpty()) {
			int n = q.poll();
			int[] move = {-1, 1, -A, A, -B, B, n*(A-1), n*(B-1)};
			for(int i=0; i<move.length; i++) {
				int next_n = n+move[i];
				// 인덱스 범위 안에 있으면서 더 적은 이동횟수로 이동할 수 있는 곳만 새로 방문
				if(next_n>=1 && next_n <=100000 && count[next_n]>count[n]+1) {
					// 최소 이동 횟수 갱신
					count[next_n] = count[n] + 1;
					if(next_n==M) {
						System.out.println(count[next_n]);
						return;
					}
					q.add(next_n);
					//System.out.println(n+" -> "+next_n+" count:"+count[next_n]+" i:"+i);
				}
			}
		}
		//System.out.println(count[M]);
	}
}
