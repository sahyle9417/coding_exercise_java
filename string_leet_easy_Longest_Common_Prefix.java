package coding_exercise_java;

import java.io.*;
import java.util.*;

public class string_leet_easy_Longest_Common_Prefix {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		int n = Integer.parseInt(input);
		String[] list = new String[n];
		for(int i=0; i<n; i++) {
			input = br.readLine();
			list[i] = input;
		}
		Longest_Common_Prefix_Solution solution = new Longest_Common_Prefix_Solution();
		String ret = solution.longestCommonPrefix(list);
		System.out.println(ret);
	}
}

class Longest_Common_Prefix_Solution {
    public String longestCommonPrefix(String[] strs) {
        int n = strs.length;
        if(n==0) {
        	return "";
        }

        int min_length = strs[0].length();
        for(int i=1; i<n; i++) {
        	if(strs[i].length() < min_length) {
        		min_length = strs[i].length();
        	}
        }
        if(min_length==0) {
        	return "";
        }
        
        int char_idx = 0;
        while(char_idx < min_length) {
        	char c = strs[0].charAt(char_idx);
        	int str_idx = 1;
	        for(; str_idx<n; str_idx++) {
	        	if(strs[str_idx].charAt(char_idx) != c) {
	        		break;
	        	}
	        }
	        if(str_idx==n) {
	        	char_idx++;
	        }
	        else {
	        	break;
	        }
        }
        return strs[0].substring(0, char_idx);
    }
}