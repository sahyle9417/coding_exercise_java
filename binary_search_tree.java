package coding_exercise_java;

class Tree{
	class Node{
		int data;		//현재 노드의 값
		Node (int data){	//Node의 생성자
			this.data=data;
		}
		Node left;		//좌측 하단 노드
		Node right;	//우측 하단 노드
	}
	Node root;
	
	//static이 아닌 make_tree_recursive 함수를 호출해주는 역할을 수행하므로 static 아님, 즉, static이 안static 호출 불가
	public void make_tree(int[] input_array) {
		//tree를 만들면서 tree의 요소인 Node타입 변수 root를 정의해줘야함
		root = make_tree_recursive(input_array, 0, input_array.length-1);
	}

	//Tree객체 내에서 여러번 호출되고 개별적으로 사용되므로 static 아님
	//Tree의 Node타입 변수인 root를 정의하기 위해 Node타입을 return해줘야함
	public Node make_tree_recursive(int[] input_array, int start, int end) {
		if(start>end) {	//재귀호출 시에 끝나는 지점을 반드시 명확하게 지정할 것
			return null;
		}
		int mid = (start+end)/2;
		Node node = new Node(input_array[mid]); 	//배열의 정가운데 요소를 노드의 data에 넣고
		node.left = make_tree_recursive(input_array, start, mid-1);	//배열의 좌측 요소들로 함수 재귀호출 후 그 중 가운데 요소를 return받아 left_node로 지정
		node.right = make_tree_recursive(input_array, mid+1, end);	//배열의 우측 요소들로 함수 재귀호출 후 그 중 가운데 요소를 return받아 right_node로 지정
		return node; //가운데 요소를 data로 갖는 노드 return
	}
	
	public void search_tree(Tree tree, int find){
		search_tree_recursive(tree.root, find);
	}

	public void search_tree_recursive(Node node, int find){
		if(find < node.data) {
			System.out.println("Data("+find+") is smaller than "+node.data);
			if(node.left == null) {
				System.out.println("No such data");
			}
			else{
				search_tree_recursive(node.left, find);	//좌측 서브트리에서 다시 탐색
			}
		}
		else if(find > node.data) {
			System.out.println("Data("+find+") is bigger than "+node.data);
			if(node.right == null) {
				System.out.println("No such data");
			}
			else{
				search_tree_recursive(node.right, find);	//우측 서브트리에서 다시 탐색
			}
		}
		else {
			System.out.println("Data("+find+") found");
		}
	}
}

public class binary_search_tree {
	public static void main(String[] args) {
		int[] input_array = new int[100];
		for(int i=0; i<input_array.length;i++) {
			input_array[i] = i*2;
		}
		Tree tree = new Tree();
		tree.make_tree(input_array);
		tree.search_tree(tree, 150);
	}
}
