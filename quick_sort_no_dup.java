package coding_exercise_java;
import java.util.Scanner;

import coding_exercise_java.report_performance;

public class quick_sort_no_dup {

	public static void quick_sort(int[] input){
		quick_sort(input,0,input.length-1);
	}
	
	public static void quick_sort(int input[], int left, int right) {
		if (left < right) {
			int pivot_index = partition(input, left, right);
			quick_sort(input, left, pivot_index - 1);
			quick_sort(input, pivot_index + 1, right);
		}
	}
	
	public static int partition(int input[], int left, int right) {
		int pivot_value = input[(left + right) / 2]; // pivot_value는 인덱스가 아닌 실제 값
		while (left < right) {
			while ((input[left] < pivot_value) && (left < right)) //left가 pivot과 같거나 큰 값을 만난 경우 멈춤
				left++;
			while ((input[right] > pivot_value) && (left < right)) //right가 pivot과 같거나 큰 값을 만난 경우 멈춤
				right--;
			if (left < right) {		//left와 right이 pivot에서 만난 경우를 제외하고 left와 right를 swap
				int temp = input[left];
				input[left] = input[right];
				input[right] = temp;
			}
			//pivot 위치를 직접 조정하지 않아도 swap을 반복하며 pivot이 left와 right이 만나는 지점으로 이동
		}
		return left;
	}

	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int input_length = scan.nextInt();
		int input[] = new int[input_length];
		for(int i=0; i<input_length; i++){
			input[i]=scan.nextInt();
		}
		scan.close();
		
		quick_sort(input);
		
		for(int i=0; i<input_length; i++){
			System.out.println(input[i]);
		}
	}
}
