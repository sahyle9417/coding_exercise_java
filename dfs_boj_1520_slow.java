package coding_exercise_java;

import java.io.*;
import java.util.*;

/*class location_mn{
	int m;
	int n;
	location_mn(int m, int n){
		this.m = m;
		this.n = n;
	}
}*/

public class dfs_boj_1520_slow {
	
	static int M;
	static int N;
	static int[][] map;
	static int[][] dp;
	static int[] dm = {-1, 1, 0, 0};
	static int[] dn = {0, 0, -1, 1};
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		M = Integer.parseInt(st.nextToken());
		N = Integer.parseInt(st.nextToken());
		map = new int[M+1][N+1];
		dp = new int[M+1][N+1];
		
		for(int m=1; m<=M; m++) {
			st = new StringTokenizer(br.readLine());
			for(int n=1; n<=N; n++) {
				map[m][n] = Integer.parseInt(st.nextToken());
			}
		}

		dfs(1, 1);
		
		/*for(int m=1; m<=M; m++) {
			for(int n=1; n<=N; n++) {
				System.out.print(dp[m][n]+" ");
			}
			System.out.print("\n");
		}*/
		
		System.out.println(dp[M][N]);
	}
	
	static void dfs(int m, int n) {
		dp[m][n]++;
		if(m==M && n==N) {
			return;
		}
		for(int i=0; i<dm.length; i++) {
			int next_m = m+dm[i];
			int next_n = n+dn[i];
			if(next_m>=1 && next_m<=M && next_n>=1 && next_n<=N) {
				if(map[m][n]>map[next_m][next_n]) {
					dfs(next_m, next_n);
				}
			}
		}
	}
}
