package coding_exercise_java;

import java.io.IOException;				//쉽게 java.io.* 라고 써도 됨
import java.io.InputStreamReader;
import java.io.BufferedReader;			//입력만 할꺼면 여기까지만 써도 됨

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

public class buffered_reader_test{
	public static void main(String[] args) throws Exception {
		//buffered reader쓰려면 try catch문 사용하거나 main함수 뒤에 throw Exception 쓰기!

		//입력
		InputStreamReader isr = new InputStreamReader(System.in);		//InputStreamReader는 입력을 character로 읽어들인다. 키보드로 입력하는 글자 한개에 해당된다고 할 수 있다.
		BufferedReader br = new BufferedReader(isr);						// InputStreamReader에 버퍼링 기능을 추가한 것, 매번 읽어오기보다는 한번에 읽어온 후 버퍼에 보관하고 사용자가 요구할 때 버퍼에서 읽어오게 한다
		String line = br.readLine();
		br.close();
		isr.close();

		//출력
		OutputStreamWriter osw = new OutputStreamWriter(System.out);	//거의 안쓸듯
		BufferedWriter bw = new BufferedWriter(osw);						//거의 안쓸듯
		bw.write(line);
		bw.flush();
		bw.close();
		osw.close();

/*
		//try catch 써도 되고 안써도 되고
		try {
			String line = br.readLine();
			bw.write(line);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
*/

	}
}

/*
import java.util.Scanner;

public class scanner_buffered_reader{
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int intNum = scan.nextInt();
		//int intNum = Integer.parseInt(scan.nextLine());
			//개행으로 구분된 데이터를 받을 때는 nextLine으로 받는 것을 권장, 의도치 않게 개행이 섞여들어갈 수 있음
			//예를 들어 정수데이터와 문자열데이터가 개행으로 구분되어 입력되는 경우
			//nextInt쓰고 nextLine쓰면 정수를 받고나서 개행을 위해 입력한 엔터가 그 다음 문자열로 입력됨
			//이렇듯 개행으로 구분된 데이터는 일단 nextLine으로 받고 type에 맞게 parse하는 것 추천
		long longNum = scan.nextLong();
		double doubleNum = scan.nextDouble();
		String str = scan.next();
		String strWithSpace = scan.nextLine();
		scan.close();
	}
}
*/