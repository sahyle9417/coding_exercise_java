package coding_exercise_java;
import java.util.Scanner;

//상근이는 지금 사탕가게에 설탕을 정확하게 N킬로그램을 배달해야 한다. 
//설탕봉지는 3킬로그램 봉지와 5킬로그램 봉지가 있다.
//최대한 적은 봉지를 들고 가려고 한다.
//설탕을 정확하게 N킬로그램 배달해야 할 때, 봉지 몇 개를 가져가면 되는지 그 수를 구하는 프로그램을 작성하시오.

public class boj_2839 {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int input = scan.nextInt();
		scan.close();
		
		int x, y;
		int output = input;
		for(x=0;x<=input/3; x++){
			for(y=0;y<=input/5;y++){
				if(3*x+5*y>input){
					break;
				}
				else if(3*x+5*y==input){
					if(x+y<output){
						output=x+y;
					}
				}
			}
		}
		if(output==input){
			System.out.println(-1);
		}
		else{
			System.out.println(output);
		}
	}
}