package coding_exercise_java;

import java.util.*;
import java.io.*;

public class simulation_boj_2455 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		int max = 0;
		int num = 0;
		for(int i=0; i<4; i++) {
			st = new StringTokenizer(br.readLine());
			int out = Integer.parseInt(st.nextToken());
			int in = Integer.parseInt(st.nextToken());
			num += (in-out);
			max = Math.max(num, max);
		}
		System.out.println(max);		
	}
}
