package coding_exercise_java;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
 
public class deque_example {
    public static void main(String[] args) {
        Deque<Integer> deque = new ArrayDeque<>();	//Deque을 구현한 ArrayDeque 인스턴스 사용
        //Queue<Integer> q = new LinkedList<>();	//Queue를 구현한 LinkedList 인스턴스 사용
        
        deque.addFirst(1);
        System.out.println("deque.addFirst(1) : "+deque);	//[1]
        
        deque.addFirst(2);
        System.out.println("deque.addFirst(2) : "+deque);	//[2, 1]

        deque.push(3);										//push(값)과 addFirst(값) 동일
        System.out.println("deque.push(3) : "+deque);		//[3, 2, 1]
        
        deque.addLast(4);									//add(값)과 addLast(값) 동일
        System.out.println("deque.addLast(4) : "+deque);	//[3, 2, 1, 4]
        System.out.println();
        
        System.out.println("deque.contains(4) : "+deque.contains(4));	//true
        System.out.println("deque.size() : "+deque.size());				//4
        System.out.println();
        
        System.out.println("deque.peekFirst() : "+deque.peekFirst());	//peek()과 peekFirst() 동일
        																//3
        System.out.println("deque.peekLast() : "+deque.peekLast());		//4
        System.out.println();

        System.out.println("deque : "+deque);        						//[3, 2, 1, 4]
        System.out.println("deque.removeFirst() : "+deque.removeFirst());	//pop()와 removeFirst() 동일
        System.out.println("deque : "+deque);								//[2, 1, 4]
        System.out.println("deque.removeLast() : "+deque.removeLast());
        System.out.println("deque : "+deque);								//[2, 1]
        System.out.println();
        
        deque.clear();
        System.out.println("deque.clear()"+deque);
        System.out.println();

        deque.add(1);
        deque.add(2);
        deque.add(3);
        deque.add(1);
        deque.add(2);
        deque.add(3);
        System.out.println("deque : "+deque);								//[1, 2, 3, 1, 2, 3]
        System.out.println("deque.removeFirstOccurrence(3) : "+deque.removeFirstOccurrence(3));
        System.out.println("deque : "+deque);								//[1, 2, 1, 2, 3]
        System.out.println("deque.removeLastOccurence(1) : "+deque.removeLastOccurrence(1));
        System.out.println("deque : "+deque);								//[1, 2, 2, 3]
        
        Iterator it = deque.iterator();
        System.out.print("Iterator : ");
        while(it.hasNext()) {
        	System.out.print(it.next()+" ");
        }
    }
}