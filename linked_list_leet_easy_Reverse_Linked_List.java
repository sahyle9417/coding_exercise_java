package coding_exercise_java;

import java.io.*;

/*
class ListNode {
	int val;
	ListNode next;
	ListNode() {}
	ListNode(int val) {
		this.val = val;
	}
	ListNode(int val, ListNode next) {
		this.val = val;
		this.next = next;
	}
}
*/

public class linked_list_leet_easy_Reverse_Linked_List {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		int n = Integer.parseInt(str);
		ListNode[] list = new ListNode[n];
		for (int i = 0; i < n; i++) {
			int val = Integer.parseInt(br.readLine());
			list[i] = new ListNode(val);
			if (i != 0) {
				list[i - 1].next = list[i];
			}
		}
		linked_list_leet_easy_Reverse_Linked_List_Solution solution = new linked_list_leet_easy_Reverse_Linked_List_Solution();
		ListNode input = (n == 0) ? null : list[0];
		ListNode ret = solution.reverseList(input);
		while (ret != null) {
			System.out.println(ret.val);
			ret = ret.next;
		}
	}
}


class linked_list_leet_easy_Reverse_Linked_List_Solution {
	
	public ListNode reverseList(ListNode head) {
		// 길이 제약이 주어지지 않았으므로, 길이 0에 대한 대비 필요
		if(head == null) {
			return head;
		}
		ListNode prev = null;
		ListNode curr = head;
		while(curr != null) {
			ListNode next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}
		return prev;
	}
}
