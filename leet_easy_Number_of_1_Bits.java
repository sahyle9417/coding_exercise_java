package coding_exercise_java;

import java.io.*;

public class leet_easy_Number_of_1_Bits {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		leet_easy_Number_of_1_Bits_Solution solution = new leet_easy_Number_of_1_Bits_Solution();
		System.out.println(solution.hammingWeight(n));
	}
}

class leet_easy_Number_of_1_Bits_Solution {

	int hammingWeight(int n) {
		int count = 0;
		// 좌측 끝(31번째 bit, MSB)부터 우측 끝(0번째 bit, LSB)까지 bit 확인
		for (int i = 31; i >= 0; i--) {
			int bit = getBit(n, i) ? 1 : 0;
			System.out.print(bit);
			if (getBit(n, i) == true) {
				count++;
			}
		}
		System.out.println();
		return count;
	}

	// 우측 끝에서부터 i번째 bit가 1인지 여부 반환
	boolean getBit(int n, int i) {
		// 1 << i : 1을 좌측으로 i bit만큼 shift
		// & : Bitwise And
		return (n & (1 << i)) != 0;
	}
}
