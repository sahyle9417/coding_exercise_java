package coding_exercise_java;

import java.io.*;
import java.util.*;

/*
숫자 연산, 비교 등에서 이해할 수 없는 현상 난다면 아래와 같이 하기 (나눗셈, 소수점 안다뤄도 이상한 현상 발생 가능함)
1. Math라이브러리 사용 최소화하기 (특히 float, double, long 다루는 것들)
2. int로 casting하기
3. 나눗셈, 소수점 "간접적"으로라도 다루는 경우 int 대신 long 사용하기
   (소수와 "간접적"으로라도 연산하는 경우 무조건 int 대신 정수는 long, 소수는 double 쓰자)
4. 나눗셈, 소수점 연산, Math 라이브러리 사용하지 않더라도 위의 현상 발생할 수 있으니 항상 의심하자. (swea 5658)
*/

// 참조 : https://mygumi.tistory.com/248
public class simulation_boj_samsung_13458_dont_use_int_use_long {
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		
		int room[] = new int[N+1];
		StringTokenizer st = new StringTokenizer(br.readLine());
		for(int n=1; n<=N; n++) {
			room[n] = Integer.parseInt(st.nextToken());
		}
		
		st = new StringTokenizer(br.readLine());
		int B = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		
		for(int n=1; n<=N; n++) {
			room[n] -= B;
		}

		// 이걸 int로 했다가 소수점 정확도 낮아서 틀렸었음
		// 소수와 "간접적"으로라도 연산하는 경우 무조건 int 대신 정수는 long, 소수는 double 쓰자
		long answer = N;
		
		for(int n=1; n<=N; n++) {
			// 총감독관이 감시할 수 없는 인원이 존재
			if(room[n]>0) {
				// 필요한 부감독관 수 = 각 방에 총감독관이 감시할 수 없는 인원 수를 B로 나누고 올림
				if(room[n]%C==0) {
					answer += room[n]/C;
				}
				else {
					answer += (room[n]/C+1);
				}
			}
		}
		System.out.println(answer);
	}
}


/*

요약 : 소수와 "간접적"으로라도 연산하는 경우 무조건 int 대신 정수는 long, 소수는 double 쓰자

=====실패 사례 1=====
int answer = N;
if(room[n]%C==0) {
	answer += room[n]/C;
}
else {
	answer += room[n]/C+1;
}

=====실패 사례 2=====
int answer = N;
answer += (int)Math.ceil((double)room[n]/(double)C);

=====성공 사례 1=====
long answer = N;
answer += Math.ceil((double)room[n]/C);

=====성공 사례 2=====
long answer = N;
if(room[n]%C==0) {
	answer += room[n]/C;
}
else {
	answer += Math.ceil((double)room[n]/C);
}

=====성공 사례 3=====
long answer = N;
if(room[n]%C==0) {
	answer += room[n]/C;
}
else {
	answer += (room[n]/C+1);
}

=====성공 사례 4=====
long answer = N;
answer += Math.ceil(1.0 * room[n]/C);

*/


