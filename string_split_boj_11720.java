package coding_exercise_java;
import java.util.Scanner;

//N개의 숫자가 공백 없이 쓰여있다. 이 숫자를 모두 합해서 출력하는 프로그램을 작성하시오.
//첫째 줄에 숫자의 개수 N (1 ≤ N ≤ 100)이 주어진다. 둘째 줄에 숫자 N개가 공백없이 주어진다.

public class string_split_boj_11720 {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		input = scan.nextLine();
		scan.close();
		
		int output = 0;
		
		int length = input.length();
		for(int i=0 ; i<length ; i++){
			//output += (int)input.charAt(i); //이렇게 하면 Ascii 코드 값을 변환됨, 절대 금지!!
			output += (int)input.charAt(i)-48; //char를 int로 바꾸려면 casting하고 48빼면 된다!!
		}
		
		System.out.println(output);
	}
}
