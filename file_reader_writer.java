package coding_exercise_java;

import java.io.*;

public class file_reader_writer {
	
	public static void main(String[] args) throws IOException {
    	
		InputStreamReader isr = new InputStreamReader(System.in);	//콘솔로부터 입력파일,출력파일명 입력받기위한 isr
		BufferedReader br = new BufferedReader(isr);		//콘솔로부터 입력파일,출력파일명 입력받기위한 br
		
		System.out.print("입력할 파일이름:");
		String read_filename = br.readLine();				//입력파일명 입력
		System.out.print("출력할 파일이름:");
		String write_filename = br.readLine();				//출력파일명 입력
		
		br.close();
		isr.close();
		
		FileReader fr = new FileReader(read_filename);	//file reader 객체 생성
		FileWriter fw = new FileWriter(write_filename); //file writer 객체 생성

		BufferedReader file_br = new BufferedReader(fr);	//파일용 br 객체 생성
		BufferedWriter file_bw = new BufferedWriter(fw);	//파일용 bw 객체 생성
		
		String buf;		//키보드에 라인단위로 읽어온 데이터를 저장할 변수
		while((buf = file_br.readLine()) != null){ 	//입력파일에서 데이터를 한줄씩 읽어서
			System.out.println(buf);                	//화면에 출력
			file_bw.write(buf);								//출력파일에 기록
			file_bw.newLine();								//출력파일에 개행 기록
		}
		file_br.close();
		file_bw.close();
		fr.close();
		fw.close();		//file writer를 close하지 않으면 기록되지 않는다.
	}
}
