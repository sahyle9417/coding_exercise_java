package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_swea_4008 {
	
	static int N;
	static int[] operator;
	static long[] number;
	static long min;
	static long max;
	
	public static void main(String[] args) throws Exception{
		
		
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			N = Integer.parseInt(br.readLine());
			
			operator = new int[4];
			st = new StringTokenizer(br.readLine());
			for(int i=0; i<4; i++) {
				operator[i] = Integer.parseInt(st.nextToken());
			}
			
			number = new long[N];
			st = new StringTokenizer(br.readLine());
			for(int i=0; i<N; i++) {
				number[i] = Long.parseLong(st.nextToken());
			}
			
			min = Long.MAX_VALUE;
			max = Long.MIN_VALUE;
			dfs(number[0], 1);
			System.out.println("#"+tc+" "+(max-min));
		}
	}
	static void dfs(long result_before, int index) {
		// 0~N-1번째 숫자까지 존재하므로 index가 N이면 모든 연산자를 다 사용한 것임
		// 이거 index==N+1로 실수했어서 잠깐 삽질했음
		if(index==N) {
			min = Math.min(result_before, min);
			max = Math.max(result_before, max);
		}
		if(operator[0]>0) {
			operator[0]--;
			dfs(result_before+number[index], index+1);
			operator[0]++;
		}
		if(operator[1]>0) {
			operator[1]--;
			dfs(result_before-number[index], index+1);
			operator[1]++;
		}
		if(operator[2]>0) {
			operator[2]--;
			dfs(result_before*number[index], index+1);
			operator[2]++;
		}
		if(operator[3]>0) {
			operator[3]--;
			dfs(result_before/number[index], index+1);
			operator[3]++;
		}
	}
	
}
