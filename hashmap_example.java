package coding_exercise_java;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;	//****필수****
import java.util.Set;		//HashMap의 entrySet()메서드 이용해서 HashMap을 Set로 변환할때 필요
import java.util.Iterator;	//Set의 iterator()메서드 이용해서 it.next()로 요소 순차접근 시 필요
import java.util.Map;

// HashMap의 Entry는 object 타입의 key와 value 참조변수를 포함한다
// key와 value는 object 참조변수이므로 어떠한 인스턴스도 가리킬 수 있다
// 모든 Entry의 key값은 유일해야 한다 (중복된 key값을 가질 수 없다)
// value는 중복을 허용한다
// key값을 해싱해서 value를 검색하는데 속도가 매우 빠르다

public class hashmap_example {
	public static void main(String[] args) throws Exception {
		HashMap map = new HashMap<String, Integer>();
		HashMap map_size10 = new HashMap<String, Integer>(10);
		
		map.put("A", 1);
		map.put("B", 2);
		map.put("C", 3);
		map.put("C", 4);	// 기존의 C와 3은 덮어쓰기 됨
		
		System.out.println("map : "+map);
		System.out.println("map.containsKey(A) : "+map.containsKey("A"));
		System.out.println("map.containsValue(1) : "+map.containsValue(1));
		System.out.println("map.get(A) : "+map.get("A"));	//해당 key가 없으면 null 반환
		System.out.println();
		
		Set set = map.entrySet();
		System.out.println("map.entrySet() : "+set);
		System.out.println();
		
		Set keySet = map.keySet();
		System.out.println("map.keySet() : "+keySet);
		System.out.println();
		
		Collection values = map.values();
		System.out.println("map.values() : "+values);
		System.out.println("Collections.max(map.values()) : "+Collections.max(values));
		System.out.println("Collections.max(map.values()) : "+Collections.min(values));
		System.out.println();
		
		Iterator it = set.iterator();
		System.out.println("Iterator it = map.entrySet().iterator();");
		System.out.println("while(it.hasNext()) {Map.Entry e = (Map.Entry)it.next();}");
		while(it.hasNext()) {
			Map.Entry e = (Map.Entry)it.next();		//Map의 내부 클래스인 Entry클래스는 Object타입 인스턴스 변수 Key와 Value를 포함한다
			System.out.println("e.getKey() : "+e.getKey());
			System.out.println("e.getValue() : "+e.getValue());
		}
		System.out.println();
		
		System.out.println("map : "+map);
		map.replace("A",2);
		System.out.println("map.replace(A,2) : "+map);
		
		System.out.println("map.replace(A,100,101) : "+map.replace("A",100,101));	//Key가 A인 Entry의 Value가 100이면 101로 교체
		System.out.println("map.replace(A,100,101) : "+map);	//Value가 100이 아니기 때문에 아무일도 일어나지 않고 false 반환
		System.out.println();
		
		map.remove("A");
		System.out.println("map.remove(A) : "+map);
		System.out.println();
		
		System.out.println("map.size() : "+map.size());
		System.out.println();
		
		HashMap map_dup = (HashMap)map.clone();
		System.out.println("map.clone() : "+map);
		
		HashMap map_add = new HashMap<String, Integer>();
		map_add.putAll(map);
		System.out.println("map_add.putAll(map) : "+map);
		System.out.println();
		
		map.clear();
		System.out.println("map.clear() : "+map);
		System.out.println("map.isEmpty() : "+map.isEmpty());
	}
}
