package coding_exercise_java;

import java.io.*;
import java.util.*;

/*class ij{
	int i;
	int j;
	ij(int i, int j){
		this.i = i;
		this.j = j;
	}
}*/

public class dfs_boj_samsung_16234 {
	
	static int N;
	static int L;
	static int R;
	static int[][] map;
	static boolean[][] visit;
	static Queue<ij> union_ij = new LinkedList<>();			// 연합에 속하는 국가들의 좌표 모두 기록, 넣기(add)만 하고 빼지(poll) 않음
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		L = Integer.parseInt(st.nextToken());
		R = Integer.parseInt(st.nextToken());
		map = new int[N+1][N+1];		// 인구이동 확정된 상태 기록
		for(int i=1; i<=N; i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		
		
		int answer = 0;
		
		while(true) {

			visit = new boolean[N+1][N+1];
			boolean union_exist = false;
			
			int[][] map_tmp = new int[N+1][N+1];	// 인구이동 중간과정 기록
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					map_tmp[i][j] = map[i][j];
				}
			}
			
			for(int start_i=1; start_i<=N; start_i++) {
				for(int start_j=1; start_j<=N; start_j++) {
					
					// 이미 연합에 속한 국가이므로 여기서부터 출발할 필요 없음
					if(visit[start_i][start_j]) {
						continue;
					}

					// 어느 연합에도 속하지 않는 국가 발견, 여기서부터 연합 뻗어나갈 것임
					//System.out.println("\nstart from "+start_i+","+start_j);
					
					// DFS로 뻗어나가면서 연합으로 끌어들이기
					dfs(start_i, start_j);
					
					/*System.out.println("union with "+start_i+","+start_j);
					for(ij tmp : union_ij) {
						System.out.println(tmp.i+","+tmp.j);
					}*/
					
					// map_tmp에 2개 이상의 국가를 가진 연합 발견, 연합에 속한 국가들 인구 갱신 
					if(union_ij.size()>1) {
						union_exist = true;
						int avg = 0;
						for(ij nation : union_ij) {
							avg += map[nation.i][nation.j];
						}
						avg /= union_ij.size();
						//System.out.println("avg:"+avg);
						for(ij nation : union_ij) {
							map_tmp[nation.i][nation.j] = avg;
						}
					}
					union_ij.clear();
				}
			}
			map = map_tmp;

			/*System.out.print("\n");
			for(int x=1; x<=N; x++) {
				for(int y=1; y<=N; y++) {
					System.out.print(map[x][y]+" ");
				}
				System.out.print("\n");
			}*/
			
			if(union_exist) {
				answer++;
			}
			else {
				break;
			}
		}
		System.out.println(answer);
	}

	static int[] di = {1, -1, 0, 0};
	static int[] dj = {0, 0, 1, -1};
	
	static void dfs(int i, int j) {
		
		visit[i][j] = true;
		union_ij.add(new ij(i, j));
		
		for(int d=0; d<4; d++) {
			int next_i = i+di[d];
			int next_j = j+dj[d];
			// 이미 연합에 속하는 칸(union[i][j]==true)은 추가하면 안됨, 이거 빼먹어서 잠깐 삽질함
			if(next_i>=1 && next_i<=N && next_j>=1 && next_j<=N && !visit[next_i][next_j]) {
				int diff = Math.abs(map[next_i][next_j]-map[i][j]);
				if(diff>=L && diff<=R) {
					dfs(next_i, next_j);
				}
			}
		}
	}
}
