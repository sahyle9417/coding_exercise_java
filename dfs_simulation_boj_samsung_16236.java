package coding_exercise_java;

import java.io.*;
import java.util.*;

/*class Shark{
	int i;
	int j;
	int size=2;
	int eat=0;
	Shark(int i, int j){
		this.i = i;
		this.j = j;
	}
}*/

public class dfs_simulation_boj_samsung_16236 {

	static int N;
	static int[][] map;
	static int[][] distance_array;
	static Shark shark;
	
	public static void main(String[] args) throws Exception{		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		N = Integer.parseInt(br.readLine());
		map = new int[N+1][N+1];
		
		StringTokenizer st;
		int fish_num = 0;
		for(int i=1; i<=N; i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
				if(map[i][j]==9) {
					shark = new Shark(i, j);
					map[i][j] = 0;
				}
				else if(map[i][j]!=0) {
					fish_num++;
				}
			}
		}
		
		int answer = 0;
		
		// 남아있는 물고기가 있는지 확인
		while(fish_num > 0) {
			
			// 이동할 수 있는 칸들에 대해 각 칸까지의 최단 거리 기록할 배열(MAX로 초기화)
			distance_array = new int[N+1][N+1];
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					distance_array[i][j] = Integer.MAX_VALUE;
				}
			}
			// 현재 위치까지의 거리를 0으로 놓고 DFS로 뻗어나가기
			// 특정 칸 도착 시 기존에 적혀있던 (다른 경로 통해서 도착한 거리)값과 내가 갖고 있던 거리+1을 비교해서 거리 갱신
			// 이걸 단순히 visit으로 방문했던 적 있으면 스킵하는 식으로 해서 최단거리가 나오지 않는 삽질했었음
			// DFS 최단거리 구하기는 빈출유형이므로 주기적으로 학습해야할 것임
			distance_array[shark.i][shark.j] = 0;
			fill_distance_array_dfs(shark.i, shark.j);
			System.out.println("\ndistance array from ["+shark.i+","+shark.j+"]");
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					if(distance_array[i][j]==Integer.MAX_VALUE) {
						System.out.print("x ");
					}
					else {
						System.out.print(distance_array[i][j]+" ");
					}
				}
				System.out.print("\n");
			}
			
			// 먹을 수 있는 물고기들 중에 가장 가까운 물고기까지의 최단 거리
			int min_distance = Integer.MAX_VALUE;
			// 자신의 위치(거리:0)은 최단 거리 계산에서 제외해야함
			distance_array[shark.i][shark.j] = Integer.MAX_VALUE;
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					if(map[i][j]!=0 && shark.size>map[i][j]) {
						min_distance = Math.min(distance_array[i][j], min_distance);
					}
				}
			}
			
			// 먹을 수 있고 도달할 수 있는 물고기 없음
			if(min_distance == Integer.MAX_VALUE) {
				break;
			}
			
			// 한번에 한마리만 먹도록 하기 위해 먹었는지 여부 기록할 flag
			boolean eat = false;
			
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					// 최단 거리에 위치한, 상어보다 작은 물고기 중 가장 위,왼쪽에 위치한 물고기 먹기
					if(map[i][j]!=0 && shark.size>map[i][j] && distance_array[i][j] == min_distance) {
						System.out.println("move ["+shark.i+","+shark.j+"]->["+i+","+j+"]");
						System.out.println("distance:"+min_distance);
						System.out.println("size:"+shark.size);
						// 시간 갱신
						answer += min_distance;
						// 물고기 제거
						map[i][j] = 0;
						fish_num--;
						// 상어 옮기기
						shark.i = i;
						shark.j = j;
						// 지도 배열 출력
						System.out.println("map");
						for(int k=1; k<=N; k++) {
							for(int l=1; l<=N; l++) {
								System.out.print(map[k][l]+" ");
							}
							System.out.print("\n");
						}
						// 잡아먹은 물고기 마릿수 업데이트, 상어 크기 변경
						shark.eat++;
						if(shark.eat==shark.size) {
							//System.out.println("size:"+shark.size+"->"+(shark.size+1));
							shark.size++;
							shark.eat = 0;
						}
						eat = true;
					}
					// 물고기 먹었으면 다음 물고기 먹기 전에 최단 거리 계산하기
					if(eat) {
						break;
					}
				}
				// 물고기 먹었으면 다음 물고기 먹기 전에 최단 거리 계산하기
				if(eat) {
					break;
				}
			}
			
		}
		System.out.println(answer);
		
	}

	static int[] di = {1, -1, 0, 0};
	static int[] dj = {0, 0, -1, 1};
	
	static void fill_distance_array_dfs(int i, int j) {
		for(int tmp=0; tmp<4; tmp++) {
			int next_i = i+di[tmp];
			int next_j = j+dj[tmp];
			if(next_i>=1 && next_i<=N && next_j>=1 && next_j<=N) {
				// 특정 칸 도착 시 기존에 적혀있던 (다른 경로 통해서 도착한 거리)값과 내가 갖고 있던 거리+1을 비교해서 거리 갱신
				// 이걸 단순히 visit으로 방문했던 적 있으면 스킵하는 식으로 해서 최단거리가 나오지 않는 삽질했었음
				// DFS 최단거리 구하기는 빈출유형이므로 주기적으로 학습해야할 것임
				if(shark.size>=map[next_i][next_j] && distance_array[next_i][next_j]>distance_array[i][j]+1) {
					distance_array[next_i][next_j] = distance_array[i][j]+1;
					fill_distance_array_dfs(next_i, next_j);
				}
			}
		}
	}
	
}