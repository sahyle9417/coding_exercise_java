package coding_exercise_java;

import java.io.*;
import java.util.*;

// ant_dist_id 객체의 dist 값에는 향하고 있는 방향의 끝으로 이동해서 떨어지기까지 이동해야하는 거리 기록
// 추후 1~k번째까지 어느 위치(왼쪽 끝 또는 오른쪽 끝)에서 떨어짐이 발생하는지 기록하기 위해, dist값 기준으로 정렬할 것임
class ant_dist_id implements Comparable<ant_dist_id>{	// dist 기준으로 정렬하기 위해 Comparable 객체를 구현한다(상속받는다).
	int dist;
	int id;
	ant_dist_id(int dist, int id){
		this.dist = dist;	// 가장 가까운 막대 끝까지의 거리
		this.id = id;		// 개미 ID, 부호로 방향 표시
	}
	
	ant_dist_id(){}
	
	@Override
	public int compareTo(ant_dist_id that) {	// dist값 기준 오름차순으로 ant_dist_id 객체를 정렬
		if(this.dist < that.dist) {				// 더 작은 dist값을 가지는 객체가 먼저 나옴
			return -1;
		}
		else if(this.dist > that.dist) { 		// 더 큰 dist값을 가지는 객체가 나중에 나옴
			return 1;
		}
		else {									// 같은 dist값을 가지는 경우 id에 대해 오름차순
			if(this.id < that.id) {			// 더 작은 id값을 가지는 객체가 먼저 나옴
				return -1;
			}
			else {								// 더 큰 id값을 가지는 객체가 먼저 나옴
				return 1;
			}
		}
	}
}

// 개미를 한 칸씩 직접 이동시키면 시간 초과 발생
// 아래 성질을 이용해 직접 한칸씩 움직이지 않고도 풀 수 있다.
// 1. 시간과 상관없이 개미가 막대 위에 놓여있는 순서는 변하지 않는다.
//    즉 개미 3, 개미 2, 개미 1 순서로 막대에 놓여있으면 서로간의 거리는 달라지더라도 놓여있는 순서는 유지된다.
//    마주보고 이동하다보면 충돌을 발생하지만 절대로 뛰어넘을 수 없기 때문이다.
// 2. 두 개미가 부딪칠 때 경로를 반대로 가는 대신, 가던 길을 그대로 가되 ID만 서로 바꿔서 진행한다고 생각을 해도 상황은 똑같다.
//    이렇게 생각하면 떨어지는 개미의 ID는 모르더라도 k번째 떨어지는 개미가 왼쪽에서 떨어지는지 오른쪽에서 떨어지는지 알 수 있다.
//    (단 떨어진 개미의 ID는 2번 성질만으로 알 수 없다.)
// 3. 2번 성질에 의해 k번째 떨어지는 개미가 떨어지는 위치가 왼쪽 끝인지 오른쪽 끝인지, 그리고 해당 위치(왼쪽 끝 또는 오른쪽 끝)에서 떨어지는 몇번째 개미인지도 알 수 있다.
//    예를 들어 k=5에서 좌,우,우,우,좌,... 순서로 떨어진다는 것을 안다면 5번째 떨어지는 개미는 왼쪽에서 2번째로 떨어지는 개미라는 것을 알 수 있다.
//    여기에 1번 성질에 의해 개미들의 순서는 바뀌지 않으므로 k번째 떨어지는 개미가 왼쪽에서 3번째로 떨어지는 개미라면, 초기에 왼쪽에서 3번째로 위치한 개미라는 것을 알 수 있다.
// 출처 : blog.encrypted.gg/56

public class simulation_boj_3163 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int TC = Integer.parseInt(br.readLine());
		for(int tc=0; tc<TC; tc++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());
			int L = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());
			
			// ant_dist_id 객체의 dist 값에는 향하고 있는 방향의 끝으로 이동해서 떨어지기까지 이동해야하는 거리 기록
			// 추후 1~k번째까지 어느 위치(왼쪽 끝 또는 오른쪽 끝)에서 떨어짐이 발생하는지 기록하기 위해, dist값 기준으로 정렬할 것임
			// ant_dist_id 객체의 id 값은 개미의 id 그대로 기록
			ant_dist_id ant_array[] = new ant_dist_id[N];
			
			for(int n=0; n<N; n++) {
				ant_array[n] = new ant_dist_id();
			}
			
			// comparative_position 배열에는 개미들의 상대적인 위치(순서)를 id 기준으로 기록
			// 왼쪽부터 3, -2, 4, 1의 id값을 가지는 개미가 위치해있다면 [3, -2, 4, 1]이 되는 것
			int[] comparative_position = new int[N];			

			// 2번 성질을 토대로 각 개미가 초기에 향하고 있는 방향과 해당 방향의 막대 끝까지의 거리 이용해
			// 언제 어느쪽(왼쪽 끝 또는 오른쪽 끝)에서 떨어짐이 발생하는지 알 수 있다. (단 여기에서는 떨어진 개미의 ID는 고려하지 않는다.)
			// 즉, k번째로 떨어지는 개미가 왼쪽에서 떨어지는지 오른쪽에서 떨어지는지 알 수 있고,
			// k번째로 떨어지는 개미가 해당 위치(왼쪽 끝 또는 오른쪽 끝)에서 떨어지는 몇번째 개미인지 알 수 있다.
			for(int n=0; n<N; n++) {
				st = new StringTokenizer(br.readLine());
				int loc = Integer.parseInt(st.nextToken());
				int id = Integer.parseInt(st.nextToken());
				comparative_position[n] = id;		// 개미들의 상대적인 위치(순서)를 id기준으로 기록, 가장 왼쪽에 위치한 개미의 id부터 입력되니까 입력 순서대로 배열에 넣으면 된다.
				if(id>0) {							// 오른쪽을 향하는 개미
					ant_array[n].dist = L-loc+1;	// 오른쪽 끝에서 떨어질 때까지 이동해야하는 거리
					ant_array[n].id = id;
				}
				else {								// 왼쪽을 향하는 개미
					ant_array[n].dist = loc+1;		// 왼쪽 끝에서 떨어질 때까지 이동해야하는 거리
					ant_array[n].id = id;
				}
			}
			
			Arrays.sort(ant_array);				// dist 기준으로 ant_dist_id 객체 배열을 정렬
			
			/*for(int n=0; n<N; n++) {
				System.out.println("dist:"+ant_array[n].dist+" id:"+ant_array[n].id);
			}*/
			
			//System.out.print("fall direction : ");
			int left = 0;						// K번째 떨어짐이 발생할때까지 왼쪽 끝에서 떨어지는 개미가 몇마리인지 기록
			int right = 0;						// K번째 떨어짐이 발생할때까지 오른쪽 끝에서 떨어지는 개미가 몇마리인지 기록
			for(int temp=0; temp<K; temp++) {	// 1~K번째 떨어짐이 발생할때까지 떨어지는 위치(왼쪽 끝 또는 오른쪽 끝)가 어딘지 left와 right에 누적 기록
				if(ant_array[temp].id > 0) {	// temp번째로 작은 dist값을 가지는 개미가 초기에 오른쪽을 향하고 있었음
					right++;					// temp번째 떨어지는 개미는 오른쪽으로 떨어짐
					//System.out.print("right ");
				}
				else {							// temp번째로 작은 dist값을 가지는 개미가 초기에 왼쪽을 향하고 있었음
					left++;						// temp번째 떨어지는 개미는 왼쪽으로 떨어짐
					//System.out.print("left ");
				}
			}
			//System.out.print("\n");

			/*System.out.println("left:"+left);
			System.out.println("right:"+right);*/
			
			// K번째로 떨어지는 개미는 K-1번째로 떨어지는 개미와 동시에 떨어짐
			// 같은 dist를 가지는 개미는 id로 오름차순 정렬했으니, K-1번째 개미는 왼쪽, K번째 개미는 오른쪽으로 떨어짐
			// K번째로 떨어지는 개미의 id를 알기 위해서는 오른쪽으로 떨어지는 개미의 id를 출력한다. (동시에 떨어지는 두 개미 중 더 큰 id)
			if(K>1 && ant_array[K-1].dist == ant_array[K-2].dist) {	// K>1 조건 없으면 배열 범위 넘어가는 비교 수행해서 런타임 에러
				
				/*System.out.println("K="+(K-1)+" and K="+K+" fall at the same time");
				System.out.println("K="+(K-1)+" fall to left");
				System.out.println("K="+K+" fall to right");*/
				
				int left_fall_id = comparative_position[left-1];
				int right_fall_id = comparative_position[N-right];
				
				/*System.out.println("left_fall_id:"+left_fall_id);
				System.out.println("right_fall_id:"+right_fall_id);*/
				
				System.out.println(Math.max(left_fall_id, right_fall_id));
				continue;
			}
			
			// K번째로 떨어지는 개미는 K+1번째로 떨어지는 개미와 동시에 떨어짐
			// 같은 dist를 가지는 개미는 id로 오름차순 정렬했으니, K번째 개미는 왼쪽, K+1번째 개미는 오른쪽으로 떨어짐
			// K번째로 떨어지는 개미의 id를 알기 위해서는 왼쪽으로 떨어지는 개미의 id를 출력한다. (동시에 떨어지는 두 개미 중 더 작은 id)
			else if(K<=N-1 && ant_array[K-1].dist == ant_array[K].dist) {	// K<=N-1 조건 없으면 배열 범위 넘어가는 비교 수행해서 런타임 에러
				
				/*System.out.println("K="+K+" and K="+(K+1)+" fall at the same time");
				System.out.println("K="+K+" fall to left");
				System.out.println("K="+(K+1)+" fall to right");*/
				
				int left_fall_id = comparative_position[left-1];
				int right_fall_id = comparative_position[N-right-1];
				
				/*System.out.println("left_fall_id:"+left_fall_id);
				System.out.println("right_fall_id:"+right_fall_id);*/
				
				System.out.println(Math.min(left_fall_id, right_fall_id));
				continue;
			}
			
			// K번째로 떨어지는 개미는 동시에 떨어지는 개미가 없음
			else {
				// K번째로 떨어지는 개미는 왼쪽으로 떨어짐
				if(ant_array[K-1].id < 0) {
					int left_fall_id = comparative_position[left-1];
					//System.out.println("left_fall_id:"+left_fall_id);
					System.out.println(left_fall_id);
					continue;
				}
				// K번째로 떨어지는 개미는 오른쪽으로 떨어짐
				else {
					int right_fall_id = comparative_position[N-right];
					//System.out.println("right_fall_id:"+right_fall_id);
					System.out.println(right_fall_id);
					continue;
				}
			}
		}
	}
}
