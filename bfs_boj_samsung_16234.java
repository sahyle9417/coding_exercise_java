package coding_exercise_java;

import java.io.*;
import java.util.*;

/*class ij{
	int i;
	int j;
	ij(int i, int j){
		this.i = i;
		this.j = j;
	}
}*/

// 참조 : https://mygumi.tistory.com/338

public class bfs_boj_samsung_16234 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int L = Integer.parseInt(st.nextToken());
		int R = Integer.parseInt(st.nextToken());
		int[][] map = new int[N+1][N+1];
		for(int i=1; i<=N; i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}

		int[] di = {1, -1, 0, 0};
		int[] dj = {0, 0, 1, -1};

		// union : 연합에 속하는 국가들의 좌표 모두 기록, 넣기(add)만 하고 빼지(poll) 않음
		Queue<ij> union = new LinkedList<>();		
		// union_tmp : 인접한 국가들 연합에 추가시킬지 여부 판단하기 위해 사용하는 임시 큐, 넣고(add) 빼고(poll) 모두 수행
		Queue<ij> union_tmp = new LinkedList<>();	
		int answer = 0;
		
		while(true) {

			int[][] map_tmp = new int[N+1][N+1];
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					map_tmp[i][j] = map[i][j];
				}
			}
			boolean[][] visit = new boolean[N+1][N+1];
			boolean union_exist = false;
			
			for(int start_i=1; start_i<=N; start_i++) {
				for(int start_j=1; start_j<=N; start_j++) {
					
					// 이미 연합에 속한 국가이므로 여기서부터 출발할 필요 없음
					if(visit[start_i][start_j]) {
						continue;
					}
					
					// 어느 연합에도 속하지 않는 국가 발견, 여기서부터 연합 뻗어나갈 것임
					//System.out.println("\nstart from "+start_i+","+start_j);

					union.add(new ij(start_i, start_j));
					union_tmp.add(new ij(start_i, start_j));
					visit[start_i][start_j] = true;
					// 연합에 속하는 국가들의 인구 총합
					int sum = map[start_i][start_j];
					// 연합에 속하는 국가 수
					int count = 1;
					
					// BFS로 뻗어나가면서 연합으로 끌어들이기
					// BFS로 떨어진 거리 상관없이 인접 칸 간의 인구수 차이만 이용해 완전탐색
					while(!union_tmp.isEmpty()) {
						ij from = union_tmp.poll();
						int from_i = from.i;
						int from_j = from.j;
						
						// 인접한 칸과 연합이 성립하는지 확인
						for(int d=0; d<4; d++) {
							int next_i = from_i+di[d];
							int next_j = from_j+dj[d];
							
							// 이미 연합에 등록된 국가는 다시 볼 필요 없음
							if(next_i>=1 && next_i<=N && next_j>=1 && next_j<=N && !visit[next_i][next_j]) {
								int diff = Math.abs(map[next_i][next_j]-map[from_i][from_j]);
								//System.out.println("diff with "+from_i+","+from_j+" and "+next_i+","+next_j+" : "+diff);
								
								// 연합 성립
								if(diff>=L && diff<=R) {
									//System.out.println("from "+from_i+","+from_j+" add "+next_i+","+next_j);
									union.add(new ij(next_i, next_j));
									union_tmp.add(new ij(next_i, next_j));
									visit[next_i][next_j] = true;
									sum += map[next_i][next_j];
									count++;
								}
							}
						}
					}
					
					// 2개 이상의 국가를 가진 연합 발견, 연합에 속한 국가들 인구 갱신 
					//System.out.println("union_size:"+union.size());
					if(union.size()>1) {
						//System.out.println("\nsum:"+sum+" count:"+count+" avg:"+(sum/count));
						union_exist = true;
						for(ij nation : union) {
							map_tmp[nation.i][nation.j] = sum/count;
						}
					}
					union.clear();
					union_tmp.clear();
				}
			}

			map = map_tmp;
			
			/*for(int x=1; x<=N; x++) {
				for(int y=1; y<=N; y++) {
					System.out.print(map[x][y]+" ");
				}
				System.out.print("\n");
			}*/

			
			if(union_exist) {
				answer++;
			}
			else {
				break;
			}
		}
		System.out.println(answer);
	}
}
