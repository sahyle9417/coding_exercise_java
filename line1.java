package coding_exercise_java;

import java.io.*;

public class line1 {
	/*
	class Solution {
	    public int solution(String inputString) {
			int first = 0;
			int second = 0;
			int third = 0;
			int fourth = 0;
			int answer = 0;
			
			for(int i=0; i<inputString.length(); i++) {
				if(inputString.charAt(i) == '(') {
					first++;
				}
				else if(inputString.charAt(i) == '{') {
					second++;
				}
				else if(inputString.charAt(i) == '[') {
					third++;
				}
				else if(inputString.charAt(i) == '<') {
					fourth++;
				}

				else if(inputString.charAt(i) == ')') {
					first--;
					answer++;
				}
				else if(inputString.charAt(i) == '}') {
					second--;
					answer++;				
				}
				else if(inputString.charAt(i) == ']') {
					third--;
					answer++;				
				}
				else if(inputString.charAt(i) == '>') {
					fourth--;
					answer++;				
				}
				
				if(first < 0 || second < 0 || third < 0 || fourth < 0) {
					answer = -1;
					break;
				}
			}
			
			if(first != 0 || second != 0 || third != 0 || fourth != 0) {
				answer = -1;
			}
	        return answer;
	    }
	}
	*/
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String inputString = br.readLine();

		int first = 0;
		int second = 0;
		int third = 0;
		int fourth = 0;
		int answer = 0;
		
		for(int i=0; i<inputString.length(); i++) {
			if(inputString.charAt(i) == '(') {
				first++;
			}
			else if(inputString.charAt(i) == '{') {
				second++;
			}
			else if(inputString.charAt(i) == '[') {
				third++;
			}
			else if(inputString.charAt(i) == '<') {
				fourth++;
			}

			else if(inputString.charAt(i) == ')') {
				first--;
				answer++;
			}
			else if(inputString.charAt(i) == '}') {
				second--;
				answer++;				
			}
			else if(inputString.charAt(i) == ']') {
				third--;
				answer++;				
			}
			else if(inputString.charAt(i) == '>') {
				fourth--;
				answer++;				
			}
			
			if(first < 0 || second < 0 || third < 0 || fourth < 0) {
				answer = -1;
				break;
			}
		}
		if(first != 0 || second != 0 || third != 0 || fourth != 0) {
			answer = -1;
		}
		System.out.println(answer);
	}
}
