package coding_exercise_java;

/*
45656이란 수는 인접한 모든 자리수의 차이가 1이 난다. 이런 수를 계단 수라고 한다.
N이 주어질 때, 길이가 N인 계단 수가 총 몇 개 있는지 구하는 프로그램을 작성하시오. (0으로 시작하는 수는 없다.)
첫째 줄에 N이 주어진다. N은 1보다 크거나 같고, 100보다 작거나 같은 자연수이다.
첫째 줄에 정답을 1,000,000,000으로 나눈 나머지를 출력한다.
*/

import java.io.*;
import java.util.*;

public class dp_boj_10844 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		
		long[][] dp = new long[n+1][10];
		
		// 1번째 줄 직접 입력
		dp[1][0] = 0;	// 0으로 시작할 수 없다.
		for(int i=1; i<=9; i++) {
			dp[1][i] = 1;
		}
		
		// 2번째 줄부터 n번째 줄까지 점화식을 이용하여 기록 
		for(int i=2; i<=n; i++) {
			dp[i][0] = dp[i-1][1];
			for(int j=1; j<=8; j++) {
				// 애초에 dp배열에 백만으로 나눈 값을 넣어주면 오버플로우 발생 안함
				dp[i][j] = (dp[i-1][j-1] + dp[i-1][j+1]) % 1000000000;
			}
			dp[i][9] = dp[i-1][8];
		}
		
		/*for(int i=1; i<=n; i++) {
			for(int j=0; j<=9; j++) {
				System.out.print(dp[i][j]+" ");
			}
			System.out.print("\n");
		}*/

		long ret = 0;
		for(int j=0; j<=9; j++) {
			ret += dp[n][j];
		}
		// dp에 넣어줄때 백만으로 나눈 나머지를 넣었다고 하더라도 그들의 합은 백만을 넘을 수 있으므로 다시 백만으로 나누기
		System.out.println(ret % 1000000000);
	}
}
