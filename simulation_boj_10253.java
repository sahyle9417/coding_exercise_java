package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_10253 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			// 이 위치에 br을 다시 선언해서 런타임 오류 계속 발생했음, br은 한번만 선언하자.
			StringTokenizer st = new StringTokenizer(br.readLine());
			/*int A = Integer.parseInt(st.nextToken());
			int B = Integer.parseInt(st.nextToken());*/
			double A = Double.parseDouble(st.nextToken());
			double B = Double.parseDouble(st.nextToken());
			
			while(A != 1) {
				
				double x = Math.ceil(B/A);
				// A, B를 int로 하고 int x = (B%A==0) ? (B/A) : (B/A + 1) 하면 됨
				// A, B를 double로 하고 double x = Math.ceil(B/A) 하면 됨
				// A, B를 int로 하고 int x = Math.ceil(B/A) 하면 안됨
				// int끼리의 나눗셈은 항상 소수자릿수 버림 수행하기 때문
				//System.out.println("x:"+x);
				
				A = A*x-B;
				B = B*x;
				//System.out.println("A:"+A+" B:"+B);
				
				double g = gcd(A, B);
				A /= g;
				B /= g;
				// 'A /= gcd(A,B)', 'B /= gcd(A,B)' 이런 식으로 하면 안됨, 갱신된 A에 대한 gcd로 B를 나누기 때문
				//System.out.println("A:"+A+" B:"+B);
				
			}
			System.out.println((int)B);
		}
	}

	public static double gcd(double A, double B) {
		if(B%A == 0) {
			return A;
		}
		else {
			return gcd(B%A, A);
		}
	}
}
