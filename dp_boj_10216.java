package coding_exercise_java;

import java.io.*;
import java.util.*;

class location_xyr{
	int x;
	int y;
	int r;
	int parent;
	location_xyr(int x, int y, int r, int parent){
		this.x=x;
		this.y=y;
		this.r=r;
		this.parent=parent;
	}
}

public class dp_boj_10216 {

	static int N;
	static location_xyr[] input;
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int TC = Integer.parseInt(br.readLine());
		
		for(int tc=0; tc<TC; tc++) {
			
			N = Integer.parseInt(br.readLine());
			input = new location_xyr[N+1];
			for(int n=1; n<=N; n++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				int x = Integer.parseInt(st.nextToken());
				int y = Integer.parseInt(st.nextToken());
				int r = Integer.parseInt(st.nextToken());
				input[n] = new location_xyr(x, y, r, n);
			}
			
			int ret=N;
			for(int n=1; n<=N; n++) {
				int x1 = input[n].x;
				int y1 = input[n].y;
				int r1 = input[n].r;
				
				for(int m=n+1; m<=N; m++) {
					int x2 = input[m].x;
					int y2 = input[m].y;
					int r2 = input[m].r;
					
					int distance = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2);
					int radius = (r1+r2)*(r1+r2);
					int n_highest_parent = highest_parent(n);
					int m_highest_parent = highest_parent(m);
					
					if(distance<=radius && n_highest_parent!=m_highest_parent) {
						
						/*System.out.print("\nn:"+n+", m:"+m);
						System.out.println("\n(Before)");
						for(int i=1; i<=N; i++) {
							System.out.print(input[i].parent+" ");;
						}*/
						
						ret--;
						input[n_highest_parent].parent = m_highest_parent;
						highest_parent(n);
						
						/*System.out.println("\n(After)");
						for(int i=1; i<=N; i++) {
							System.out.print(input[i].parent+" ");;
						}
						System.out.println("\n");*/
					}
				}
			}
			
			/*int ret=0;
			for(int n=1; n<=N; n++) {
				if(n==input[n].parent) {
					ret++;
				}
			}*/
			System.out.println(ret);
		}
	}
	
	static int highest_parent(int n) {
		if(n==input[n].parent) {
			return n;
		}
		else {
			input[n].parent = highest_parent(input[n].parent);
			return input[n].parent;
		}
	}
}


/*
Additional Test Case
6
0 0 1
9 0 1
1 0 1
8 0 1
2 0 1
5 0 1
Answer:3
*/