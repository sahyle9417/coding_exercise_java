package coding_exercise_java;

import java.io.*;
import java.util.*;

/*class nm{
	int n;
	int m;
	nm(int n, int m){
		this.n = n;
		this.m = m;
	}
}*/

public class dfs_boj_samsung_14502 {

	static Queue<nm> virus_q = new LinkedList<>();
	static int N;
	static int M;
	static int[][] map;		// 입력받은 배열 그대로 유지(감염 이전으로 롤백하는데 사용)
	static int[][] map_tmp;	// 실제로 감염 과정 기록할 배열
	static int[] dn = {-1, 1, 0, 0};
	static int[] dm = {0, 0, -1, 1};
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());

		map = new int[N+1][M+1];
		ArrayList<nm> empty_area = new ArrayList<>();
		
		for(int n=1; n<=N; n++) {
			st = new StringTokenizer(br.readLine());
			for(int m=1; m<=M; m++) {
				map[n][m] = Integer.parseInt(st.nextToken());
				if(map[n][m]==0) {
					empty_area.add(new nm(n, m));
				}
			}
		}
		
		int max_safe_area_num = 0;
		for(int i=0; i<empty_area.size()-2; i++) {
			nm wall_1 = empty_area.get(i);
			
			for(int j=i+1; j<empty_area.size()-1; j++) {
				nm wall_2 = empty_area.get(j);
				
				for(int k=j+1; k<empty_area.size(); k++) {
					nm wall_3 = empty_area.get(k);
					
					// 벽과 감염으로 변형된 map_tmp 배열을 감염과 벽 세우기 전 상태로 롤백
					// 2차원 배열은 함수 사용 불가, for문으로 덮어쓰기 해줘야함
					// 그냥 map_tmp = map으로 하면 안됨
					// call by reference 원칙으로 주솟값이 넘어가서 map배열 내용도 바뀜
					map_tmp = new int[N+1][M+1];
					for(int n=1; n<=N; n++) {
						for(int m=1; m<=M; m++) {
							// 감염 발생 이전으로 롤백
							map_tmp[n][m] = map[n][m];
						}
					}
					// 벽 세우기
					map_tmp[wall_1.n][wall_1.m] = 1;
					map_tmp[wall_2.n][wall_2.m] = 1;
					map_tmp[wall_3.n][wall_3.m] = 1;
					
					// 감염 발생시킨 뒤 남아있는 안전 영역 넓이 반환
					int safe_area_num = safe_area_num();
					/*if(safe_area_num>max_safe_area_num) {
						System.out.println("wall: ["+wall_1.n +","+wall_1.m+"] ["+wall_2.n+","+wall_2.m+"] ["+wall_3.n+","+wall_3.m+"]");
						for(int n=1; n<=N; n++) {
							for(int m=1; m<=M; m++) {
								System.out.print(map_tmp[n][m]+" ");;
							}
							System.out.print("\n");
						}
						System.out.println("safe_area_num="+safe_area_num+"\n");
					}*/
					max_safe_area_num = Math.max(safe_area_num, max_safe_area_num);
				}
			}
		}
		System.out.println(max_safe_area_num);
		
	}
	
	static int safe_area_num() {
		LinkedList<nm> virus_q = new LinkedList<>();
		for(int n=1; n<=N; n++) {
			for(int m=1; m<=M; m++) {
				if(map_tmp[n][m]==2) {
					virus_q.add(new nm(n, m));
				}
			}
		}
		
		while(!virus_q.isEmpty()) {
			nm virus = virus_q.poll();
			dfs(virus);
		}
		
		int safe_area_num = 0;
		for(int n=1; n<=N; n++) {
			for(int m=1; m<=M; m++) {
				if(map_tmp[n][m]==0) {
					safe_area_num++;
				}
			}
		}
		return safe_area_num;
	}
	
	static void dfs(nm virus){
		int n = virus.n;
		int m = virus.m;
		map_tmp[n][m] = 2;
		for(int i=0; i<dn.length; i++) {
			int next_n = n+dn[i];
			int next_m = m+dm[i];
			if(next_n>=1 && next_n<=N && next_m>=1 && next_m<=M && map_tmp[next_n][next_m]==0) {
				dfs(new nm(next_n, next_m));
			}
		}
	}
	
}
