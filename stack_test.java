package coding_exercise_java;
public class stack_test {

	private int MAX = 5;
	private int top;
	private int[] item;

	public stack_test(){
		top = 0;
		item = new int[MAX];
	}
	
	public void push(int num){
		if (top >= MAX) {
			System.out.println("stack is full");
		}
		else {
			item[top] = num;
			top = top + 1;
			System.out.println(num);
		}
	}
	
	public void pop(){ 
		if (top == 0) {
			System.out.println("stack is empty");
		}
		else {
			top = top - 1; 
			int num;
			num = item[top]; 
			item[top] = 0; 
			System.out.println(num);
		}
	}
	
	public static void main(String[] args) {
		stack_test s = new stack_test();
		s.push(1);
		s.push(2);
		s.push(3);
		s.push(4);
		s.push(5);
		s.push(6);
		s.pop();
		s.pop();
		s.pop();
		s.pop();
		s.pop();
		s.pop();
	}
}