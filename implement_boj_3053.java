package coding_exercise_java;

import java.util.*;
import java.io.*;

public class implement_boj_3053 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		double R = Double.parseDouble(br.readLine());
		System.out.println(Math.pow(R, 2) * Math.PI);
		System.out.println(Math.pow(R, 2) * 2);
	}
}
