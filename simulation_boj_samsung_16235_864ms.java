package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_samsung_16235_864ms {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());

		int[][] food = new int[N+1][N+1];		// 양분
		int[][] food_add = new int[N+1][N+1];	// 겨울에 추가되는 양분
		int[][] dead_tree = new int[N+1][N+1];	// 죽은 나무에게서 발생한 양분
		for(int i=1; i<=N; i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				food[i][j] = 5;
				food_add[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		
		LinkedList<Integer>[][] map = new LinkedList[N+1][N+1];
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				map[i][j] = new LinkedList<>();
			}
		}
		
		for(int m=0; m<M; m++) {
			st = new StringTokenizer(br.readLine());
			int i = Integer.parseInt(st.nextToken());
			int j = Integer.parseInt(st.nextToken());
			int age = Integer.parseInt(st.nextToken());
			map[i][j].add(age);
		}
		
		for(int year=1; year<=K; year++) {
			
			// 양분 먹기
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					// 살아남은 나무만 기록하는 리스트 (추후 map[i][j]를 이걸로 업데이트 할 것임)
					LinkedList<Integer> live_tree = new LinkedList<>();
					// (느림)해당 칸의 나무 나이 순 정렬
					//Collections.sort(map[i][j]);
					// (빠름)정렬 안해도 됨, 어차피 초기에 주어진 나무들의 위치는 모두 각기 다르므로 나이 비교할일 없으며
					// 새로 추가되는 나이 1짜리 나무는 addFirst로 앞에 추가하면 알아서 정렬된다.
					for(int age : map[i][j]) {
						// 양분 먹을 수 있으면 양분 먹고 나이 증가
						if(age <= food[i][j]) {
							food[i][j] -= age;
							age++;
							live_tree.add(age);
						}
						// 양분 먹을 수 없는 나무는 양분됨
						else {
							dead_tree[i][j] += age/2;
						}
					}
					map[i][j] = live_tree;
				}
			}
			
			// 인접 칸으로 나무 번식
			int[] di = {-1, -1, -1, 0, 0, 1, 1, 1};
			int[] dj = {-1, 0, 1, -1, 1, -1, 0, 1};
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					// 나이가 5의 배수인 나무의 개수를 세서 해당 개수만큼 인접 8칸으로 나이 1짜리 나무 번식
					for(int age : map[i][j]) {
						// 나이가 5의 배수인 나무 발견할 때마다 주변에 한개씩 뿌려도 되지만,
						// 나이가 5의 배수인 나무의 개수(new_tree_count)만 세서 주변에 한번에 뿌려도 된다.
						if(age % 5 == 0) {
							for(int d=0; d<8; d++) {
								int new_tree_i = i+di[d];
								int new_tree_j = j+dj[d];
								if(new_tree_i>=1 && new_tree_i<=N && new_tree_j>=1 && new_tree_j<=N) {
									// 처음에 그냥 add하고 나이순 정렬을 수행했었는데
									// 애초에 1짜리 나무를 맨앞에 추가하면 정렬하지 않아도 됨
									// 엄청난 속도 향상(1540ms->864ms)
									//map[new_tree_i][new_tree_j].add(1);
									map[new_tree_i][new_tree_j].addFirst(1);
								}
							}
						}
					}
				}
			}
			
			// 양분 추가
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					food[i][j] += dead_tree[i][j];
					food[i][j] += food_add[i][j];
					dead_tree[i][j] = 0;
				}
			}
			
		}
		// K년 지났음
		int answer = 0;
		
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				answer += map[i][j].size();
			}
		}
		
		System.out.println(answer);
	}
}
