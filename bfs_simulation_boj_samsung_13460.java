package coding_exercise_java;

import java.io.*;
import java.util.*;

/*class ij{
	int i;
	int j;
	ij(int i, int j){
		this.i = i;
		this.j = j;
	}
}*/

/*class map_status{
	int[][] map;
	ij hole;
	ij red;
	ij blue;
	int last_direction;
	map_status(int[][] map, ij hole, ij red, ij blue, int last_direction){
		this.map = map;
		this.hole = hole;
		this.red = red;
		this.blue = blue;
		this.last_direction = last_direction;
	}
}*/

public class bfs_simulation_boj_samsung_13460 {

	static int N;
	static int M;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());

		// 초기화 안하면 오류날 수 있다고 해서 임시로 초기화
		ij hole = null;
		ij red = null;
		ij blue = null;
		
		// 0:빈공간(.), 1:벽(#), 2:출구(O), 3:빨간구슬(R), 4:파란구슬(B)
		int[][] map = new int[N+1][M+1];
		for(int n=1; n<=N; n++) {
			String line = br.readLine();
			for(int m=1; m<=M; m++) {
				char input = line.charAt(m-1);
				switch(input) {
				case('.'):
					map[n][m] = 0;
					break;
				case('#'):
					map[n][m] = 1;
					break;
				case('O'):
					map[n][m] = 2;
					hole = new ij(n, m);
					break;
				case('R'):
					map[n][m] = 3;
					red = new ij(n, m);
					break;
				default: // case('B'):
					map[n][m] = 4;
					blue = new ij(n, m);
					break;
				}
			}
		}
		/*for(int n=1; n<=N; n++) {
			for(int m=1; m<=M; m++) {
				System.out.print(map[n][m]+" ");
			}
			System.out.print("\n");
		}*/
		
		LinkedList<map_status> q = new LinkedList<>();
		// map_status : map, hole, red, blue, direction
		q.add(new map_status(map, hole, red, blue, -1));
		int trial = 0;
		while(!q.isEmpty()) {
			int q_size = q.size();
			for(int qs=0; qs<q_size; qs++) {
				map_status last = q.poll();
				// 파란공이 구멍에 위치 -> 실패, 더이상 시도X
				// 파란공과 빨간공이 모두 구멍에 위치해도 둘다 나간 것이므로 실패
				// 그래서 빨간공이 구멍에 위치(성공)했는지 검사하기 전에 파란공이 구멍에 위치(실패)했는지 먼저 검사해야함
				if(last.hole.i==last.blue.i && last.hole.j==last.blue.j) {
					continue;
				}
				// 파란공은 구멍에 위치X, 빨간공만 구멍에 위치 -> 성공
				if(last.hole.i==last.red.i && last.hole.j==last.red.j) {
					System.out.println(trial);
					return;
				}
				for(int next_direction=0; next_direction<4; next_direction++) {
					if(last.last_direction!=next_direction) {
						q.add(move(last, next_direction));
					}
				}
			}
			trial++;
			if(trial==11) {
				System.out.println(-1);
				return;
			}
		}
	}
	
	// 좌, 우, 상, 하
	static int[] di = {0, 0, -1, 1};
	static int[] dj = {-1, 1, 0, 0};
	
	static map_status move(map_status last, int direction) {
		//System.out.println("direction:"+direction);
		
		// 이전 상태 call by value
		int[][] map = new int[N+1][M+1];
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=M; j++) {
				map[i][j] = last.map[i][j];
			}
		}
		ij hole = new ij(last.hole.i, last.hole.j);
		ij red = new ij(last.red.i, last.red.j);
		ij blue = new ij(last.blue.i, last.blue.j);
		
		boolean red_finish = false;
		boolean blue_finish = false;
		
		while(!blue_finish || !red_finish) {
			
			/*for(int i=1; i<=N; i++) {
				for(int j=1; j<=M; j++) {
					System.out.print(map[i][j]+" ");
				}
				System.out.print("\n");
			}
			System.out.print("\n");*/
			
			
			if(!red_finish) {
				switch(map[red.i+di[direction]][red.j+dj[direction]]) {
				case(0): // 빈공간 이동
					map[red.i][red.j] = 0;
					red.i += di[direction];
					red.j += dj[direction];
					map[red.i][red.j] = 3;
					break;
				case(1): // 벽이면 멈춤
					red_finish = true;
					break;
				case(2): // 빨간 공이 먼저 출구 도착 시 성공 처리후 끝내기
					map[red.i][red.j] = 0;
					red.i = hole.i;
					red.j = hole.j;
					// 빨간공을 뒤따라서 파란공도 출구 도착하면 실패 처리해야함
					// 그래서 빨간공이 출구로 나갔어도 파란공 이동은 아직 멈춰선 안됨
					// 그래서 blue_finish는 설정X
					red_finish = true;
					break;
				default: // case(4), 파란공 만나면 멈춤
					if(blue_finish) { // 빨간공은 벽에, 파란공은 빨간공에 부딪혀 더이상 이동 불가
						red_finish = true;
					}
					break;
				}
			}
			if(!blue_finish) {
				switch(map[blue.i+di[direction]][blue.j+dj[direction]]) {
				case(0): // 빈공간 이동
					map[blue.i][blue.j] = 0;
					blue.i += di[direction];
					blue.j += dj[direction];
					map[blue.i][blue.j] = 4;
					break;
				case(1): // 벽이면 멈춤
					blue_finish = true;
					break;
				case(2): // 파란 공이 출구 도착 시 실패 처리후 끝내기
					map[blue.i][blue.j] = 0;
					blue.i = hole.i;
					blue.j = hole.j;
					map[hole.i][hole.j] = 4;
					blue_finish = true;
					red_finish = true;
					break;
				default: // case(3), 빨간공 만나면 멈춤
					if(red_finish) { // 빨간공은 벽에, 파란공은 빨간공에 부딪혀 더이상 이동 불가
						blue_finish = true;
					}
					break;
				}
			}
		}
		return new map_status(map, hole, red, blue, direction);
	}
}
