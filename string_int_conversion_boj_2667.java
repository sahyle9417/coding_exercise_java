package coding_exercise_java;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class string_int_conversion_boj_2667 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int n = Integer.parseInt(br.readLine());
		int[][] map = new int[n+1][n+1];
		int[][] visit = new int[n+1][n+1];
		
		for(int i=1; i<=n; i++) {
			String temp = br.readLine();
			for(int j=1; j<=n; j++) {
				// char로 표현된 숫자의 아스키 코드값에서 '0'의 아스키 코드값을 빼면 int로 변환할 수 있다.
				map[i][j] = temp.charAt(j-1) - '0';
			}
		}
		
		/*for(int i=1; i<=n; i++) {
			for(int j=1; j<=n; j++) {
				System.out.print(map[i][j]);
			}
			System.out.print("\n");
		}*/
		
		
		
	}
}
