package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_samsung_16235_1540ms {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());

		int[][] food = new int[N+1][N+1];		// 양분
		int[][] food_add = new int[N+1][N+1];	// 겨울에 추가되는 양분
		int[][] dead_tree = new int[N+1][N+1];	// 죽은 나무에게서 발생한 양분
		for(int i=1; i<=N; i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				food[i][j] = 5;
				food_add[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		
		LinkedList<Integer>[][] map = new LinkedList[N+1][N+1];
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				map[i][j] = new LinkedList<>();
			}
		}
		
		for(int m=0; m<M; m++) {
			st = new StringTokenizer(br.readLine());
			int i = Integer.parseInt(st.nextToken());
			int j = Integer.parseInt(st.nextToken());
			int age = Integer.parseInt(st.nextToken());
			map[i][j].add(age);
		}
		
		for(int year=1; year<=K; year++) {
			
			// 양분 먹기
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					// 살아남은 나무만 기록하는 리스트 (추후 map[i][j]를 이걸로 업데이트 할 것임)
					LinkedList<Integer> live_tree = new LinkedList<>();
					// 해당 칸의 나무 나이 순 정렬
					Collections.sort(map[i][j]);
					for(int age : map[i][j]) {
						// 양분 먹을 수 있으면 양분 먹고 나이 증가
						if(age <= food[i][j]) {
							food[i][j] -= age;
							age++;
							live_tree.add(age);
						}
						// 양분 먹을 수 없는 나무는 양분됨
						else {
							dead_tree[i][j] += age/2;
						}
					}
					map[i][j] = live_tree;
				}
			}
			
			// 인접 칸으로 나무 번식
			int[] di = {-1, -1, -1, 0, 0, 1, 1, 1};
			int[] dj = {-1, 0, 1, -1, 1, -1, 0, 1};
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					// 나이가 5의 배수인 나무는 인접 8칸으로 나이 1짜리 나무 번식
					for(int age : map[i][j]) {
						if(age % 5 == 0) {
							for(int d=0; d<8; d++) {
								int new_tree_i = i+di[d];
								int new_tree_j = j+dj[d];
								if(new_tree_i>=1 && new_tree_i<=N && new_tree_j>=1 && new_tree_j<=N) {
									map[new_tree_i][new_tree_j].add(1);
								}
							}
						}
					}
				}
			}
			
			// 양분 추가
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					food[i][j] += dead_tree[i][j];
					food[i][j] += food_add[i][j];
					dead_tree[i][j] = 0;
				}
			}
			
		}
		// K년 지났음
		int answer = 0;
		
		for(int i=1; i<=N; i++) {
			for(int j=1; j<=N; j++) {
				answer += map[i][j].size();
			}
		}
		
		System.out.println(answer);
	}
}
