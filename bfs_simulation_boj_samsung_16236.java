package coding_exercise_java;

import java.io.*;
import java.util.*;

// 참고 : https://whereisusb.tistory.com/239
// 참고 : https://www.acmicpc.net/board/view/12343 (BFS로 [N,M]까지 가는 최단거리)

public class bfs_simulation_boj_samsung_16236 {
	
	// BFS에서 큐에 들어갈 좌표 기록하는 객체
	static class ij{
		int i;
		int j;
		ij(int i, int j){
			this.i = i;
			this.j = j;
		}
	}
	
	public static void main(String[] args) throws Exception{		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int N = Integer.parseInt(br.readLine());
		int[][] map = new int[N+1][N+1];
		boolean[][] visit = new boolean[N+1][N+1];	// BFS는 매번 최단 거리로 이동하므로 이미 방문한 칸은 다시 방문하면 안됨

		ij shark = new ij(0, 0);	// 상어 위치 임시로 초기화
		int shark_size = 2;
		int shark_eat = 0;
		
		StringTokenizer st;
		for(int i=1; i<=N; i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
				if(map[i][j]==9) {
					shark = new ij(i, j);	// 상어 위치 실제 초기화
					map[i][j] = 0;			// 상어 자신은 먹이가 아니므로 지도 상에서 지운다.
				}
			}
		}
		
		Queue<ij> q = new LinkedList<>();
		q.add(shark);
		visit[shark.i][shark.j] = true;
		
		int[] di = {1, -1, 0, 0};
		int[] dj = {0, 0, 1, -1};
		
		int answer = 0;
		int distance = 0;
		
		while(!q.isEmpty()) {
			
			int q_size = q.size();				// 같은 거리를 가지는 원소들(현재 큐에 들어있는 원소들)의 개수 기록

			int eat_i = Integer.MAX_VALUE;		// 가장 상단의 좌표를 뽑아내기 위해 MAX로 설정 후 min으로 갱신
			int eat_j = Integer.MAX_VALUE;		// 가장 좌측의 좌표를 뽑아내기 위해 MAX로 설정 후 min으로 갱신

			for(int qs=0; qs<q_size; qs++) {	// 같은 거리 내에서 먹을 수 있는(상어보다 작은) 물고기 발견 시 더 먼 거리 탐색하지 않음

				ij ij_tmp = q.poll();
				int i = ij_tmp.i;
				int j = ij_tmp.j;
				
				for(int d_tmp=0; d_tmp<4; d_tmp++) {
					int next_i = i+di[d_tmp];
					int next_j = j+dj[d_tmp];
					
					// 방문한적 없으며 상어보다 작거나 같으면 이동 가능
					if(next_i>=1 && next_i<=N && next_j>=1 && next_j<=N && !visit[next_i][next_j] && map[next_i][next_j]<=shark_size) {
						q.add(new ij(next_i, next_j));
						visit[next_i][next_j] = true;
						
						// 먹을 수 있는 (상어보다 작은) 물고기가 위치한 칸 발견, 동일 거리 내에서 가장 상단좌측 골라내야 함
						if(map[next_i][next_j]!=0 && map[next_i][next_j]<shark_size) {
							// 기존에 발견했던 먹을 수 있는(상어보다 작은) 물고기보다 상단에 위치하는지 검사
							if(next_i<eat_i) {
								eat_i = next_i;
								eat_j = next_j;
							}
							// 기존에 발견했던 먹을 수 있는(상어보다 작은) 물고기보다 좌측에 위치하는지 검사
							else if(next_i==eat_i && next_j<eat_j) {
								eat_i = next_i;
								eat_j = next_j;
							}
						}
					}
				}
			}
			// 같은 거리 내의 영역 탐색 완료(qs==q_size)했으므로 distance 갱신
			distance++;
			// eat_i가 갱신되었다는 것은 같은 거리 내에 먹을 수 있는 (상어보다 작은) 물고기 발견한 것
			if(eat_i!=Integer.MAX_VALUE) {
				// 상어가 먹은 물고기 수 기록 및 크기 조정
				shark_eat++;
				if(shark_eat==shark_size) {
					shark_size++;
					shark_eat = 0;
				}
				// 물고기 제거
				map[eat_i][eat_j] = 0;
				// 상어 이동
				shark.i = eat_i;
				shark.j = eat_j;
				// 새로운 위치에서 앞서 했던 동작 반복하기 위해 반복문 나가는 (break문 사용하는) 것이 아니라,
				// visit 배열, 큐, distance, eat_i, eat_j 초기화해서 같은 반복문 내에서 새로운 이동을 시작한다.
				// q_size, eat_i, eat_j는 위에서 초기화
				// 이 부분 매우 참신했음
				// visit 배열 초기화
				visit = new boolean[N+1][N+1];
				visit[shark.i][shark.j] = true;
				// 새로운 위치에서 다시 다음 물고기 탐색하므로 큐 초기화 후 새로운 위치 삽입
				// q_size는 위에서 초기화
				q.clear();
				q.add(new ij(shark.i, shark.j));
				// answer 갱신 후 distance 초기화
				answer += distance;
				distance = 0;
			}
		}
		System.out.println(answer);
	}
}