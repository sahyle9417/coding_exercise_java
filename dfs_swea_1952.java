package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_swea_1952 {
	
	static int day_price;
	static int month1_price;
	static int month3_price;
	static int min_price;
	static int[] use_day;
	
	public static void main(String[] args) throws Exception{
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			
			st = new StringTokenizer(br.readLine());
			day_price = Integer.parseInt(st.nextToken());
			month1_price = Integer.parseInt(st.nextToken());
			month3_price = Integer.parseInt(st.nextToken());
			min_price = Integer.parseInt(st.nextToken());
			
			// 사용일수
			use_day = new int[12];
			st = new StringTokenizer(br.readLine());
			for(int i=0; i<12; i++) {
				use_day[i] = Integer.parseInt(st.nextToken());
			}
			dfs(0, 0);
			System.out.println("#"+tc+" "+min_price);
		}
	}
	static void dfs(int price, int index) {
		//System.out.println("index:"+index+" price:"+price);
		if(index>=12) {
			min_price = Math.min(price, min_price);
			return;
		}
		// 세달 이용권 사용
		dfs(price+month3_price, index+3);
		// 한달 이용권과 하루 이용권 중 더 저렴한 것 선택
		dfs(price+Math.min(month1_price, day_price*use_day[index]), index+1);
	}
}
