package coding_exercise_java;


public class linked_list_leet_easy_Linked_List_Cycle {
	public static void main(String[] args) {
		ListNode node1 = new ListNode(1);
		ListNode node2 = new ListNode(2);
		ListNode node3 = new ListNode(3);
		ListNode node4 = new ListNode(4);
		ListNode node5 = new ListNode(5);
		node1.next = node2;
		node2.next = node3;
		node3.next = node4;
		node4.next = node5;
		node5.next = node1;
		linked_list_leet_easy_Linked_List_Cycle_Solution solution = new linked_list_leet_easy_Linked_List_Cycle_Solution();
		boolean ret = solution.hasCycle(node1);
		System.out.println(ret);
	}
}



class linked_list_leet_easy_Linked_List_Cycle_Solution {
    public boolean hasCycle(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;
        // fast가 더이상 이동할 수 없는 리스트의 끝에 도달할 때까지 반복
        // 끝이 있다면 어차피 fast가 먼저 끝에 도달할 것이므로 slow는 검사할 필요 없음
        // 끝이 없다(cycle이 존재한다)면 loop를 벗어날 수 없으니 무조건 만날 수 밖에 없음
        while(fast != null && fast.next != null) {
        	fast = fast.next.next;
        	slow = slow.next;
        	if(fast == slow) {
        		return true;
        	}
        }
        return false;
    }
}


class ListNode {
    int val;
    ListNode next;
    ListNode(int x) {
        val = x;
        next = null;
    }
    ListNode(int x, ListNode n) {
        val = x;
        next = n;
    }
}
