package coding_exercise_java;

import java.util.LinkedList;
import java.util.List;

public class tree_leet_easy_Binary_Tree_Level_Order_Traversal_recursive {

	public static void main(String[] args) {
		// Integer[] tree_list = { null, 1, 2, 2, 3, 4, 4, 3 };
		Integer[] tree_list = { null, 3, 9, 20, null, null, 15, 7 };
		TreeNode root = makeTree(tree_list, 1);
		printTree(root);
		tree_leet_easy_Binary_Tree_Level_Order_Traversal_recursive_Solution solution = new tree_leet_easy_Binary_Tree_Level_Order_Traversal_recursive_Solution();
		List<List<Integer>> all_levels = solution.levelOrder(root);
		for (List<Integer> one_level : all_levels) {
			for (int element : one_level) {
				System.out.print(element + " ");
			}
			System.out.println();
		}
	}

	static void printTree(TreeNode root) {
		LinkedList<TreeNode> node_list = new LinkedList<>();
		node_list.add(root);
		while (!node_list.isEmpty()) {
			int q_size = node_list.size();
			for (int qs = 0; qs < q_size; qs++) {
				TreeNode node = node_list.pop();
				if (node == null) {
					System.out.print("X ");
					continue;
				}
				System.out.print(node.val + " ");
				node_list.add(node.left);
				node_list.add(node.right);
			}
		}
		System.out.println();
	}
	
	static TreeNode makeTree(Integer[] tree_list, int idx) {
		if (idx >= tree_list.length) {
			return null;
		}
		if (tree_list[idx] == null) {
			return null;
		}
		TreeNode node = new TreeNode((int) tree_list[idx]);
		node.left = makeTree(tree_list, idx * 2);
		node.right = makeTree(tree_list, idx * 2 + 1);
		return node;
	}

}

class tree_leet_easy_Binary_Tree_Level_Order_Traversal_recursive_Solution {

	public List<List<Integer>> levelOrder(TreeNode root) {
		List<List<Integer>> all_levels = new LinkedList<>();
		levelOrderRecursive(all_levels, 0, root);
		return all_levels;
	}

	void levelOrderRecursive(List<List<Integer>> all_levels, int level, TreeNode node) {
		if (node == null) {
			return;
		}
		if (all_levels.size() <= level) {
			all_levels.add(new LinkedList<Integer>());
		}

		levelOrderRecursive(all_levels, level + 1, node.left);

		all_levels.get(level).add(node.val);

		levelOrderRecursive(all_levels, level + 1, node.right);
	}
}

/*
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode() {}
	TreeNode(int val) {
		this.val = val;
	}
	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}
*/
