package coding_exercise_java;

import java.util.*;
import java.io.*;

public class dfs_boj_7569_fail {

	static int M;
	static int N;
	static int H;
	static int[][][] map;
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		M = Integer.parseInt(st.nextToken());
		N = Integer.parseInt(st.nextToken());
		H = Integer.parseInt(st.nextToken());
		
		map = new int[H+1][N+1][M+1];
		
		for(int h=1; h<=H; h++) {
			for(int n=1; n<=N; n++) {
				st = new StringTokenizer(br.readLine());
				for(int m=1; m<=M; m++) {
					map[h][n][m] = Integer.parseInt(st.nextToken());
					if(map[h][n][m]==0) {
						map[h][n][m] = Integer.MAX_VALUE;
					}
					else if(map[h][n][m]==1) {
						map[h][n][m] = 0;
					}
				}
			}
		}
		
		/*for(int h=1; h<=H; h++) {
			for(int n=1; n<=N; n++) {
				for(int m=1; m<=M; m++) {
					System.out.print(map[h][n][m]+" ");
				}
				System.out.print("\n");
			}
		}
		System.out.print("\n");*/
		
		for(int h=1; h<=H; h++) {
			for(int n=1; n<=N; n++) {
				for(int m=1; m<=M; m++) {
					if(map[h][n][m]==0) {
						dfs(h, n, m);
					}
				}
			}
		}
		
		/*for(int h=1; h<=H; h++) {
			for(int n=1; n<=N; n++) {
				for(int m=1; m<=M; m++) {
					System.out.print(map[h][n][m]+" ");
				}
				System.out.print("\n");
			}
		}*/
		
		int ret = Integer.MIN_VALUE;
		for(int h=1; h<=H; h++) {
			for(int n=1; n<=N; n++) {
				for(int m=1; m<=M; m++) {
					if(map[h][n][m]==Integer.MAX_VALUE) {
						System.out.println(-1);
						return;
					}
					else {
						ret = Math.max(ret, map[h][n][m]);
					}
				}
			}
		}
		System.out.println(ret);
	}
	
	static void dfs(int h, int n, int m) {
		if(h-1>=1 && map[h-1][n][m]!=-1 && map[h-1][n][m]>map[h][n][m]+1) {
			map[h-1][n][m] = map[h][n][m]+1;
			dfs(h-1, n, m);
		}
		if(h+1<=H && map[h+1][n][m]!=-1 && map[h+1][n][m]>map[h][n][m]+1) {
			map[h+1][n][m] = map[h][n][m]+1;
			dfs(h+1, n, m);
		}
		if(n-1>=1 && map[h][n-1][m]!=-1 && map[h][n-1][m]>map[h][n][m]+1) {
			map[h][n-1][m] = map[h][n][m]+1;
			dfs(h, n-1, m);
		}
		if(n+1<=N && map[h][n+1][m]!=-1 && map[h][n+1][m]>map[h][n][m]+1) {
			map[h][n+1][m] = map[h][n][m]+1;
			dfs(h, n+1, m);
		}
		if(m-1>=1 && map[h][n][m-1]!=-1 && map[h][n][m-1]>map[h][n][m]+1) {
			map[h][n][m-1] = map[h][n][m]+1;
			dfs(h, n, m-1);
		}
		if(m+1<=M && map[h][n][m+1]!=-1 && map[h][n][m+1]>map[h][n][m]+1) {
			map[h][n][m+1] = map[h][n][m]+1;
			dfs(h, n, m+1);
		}
	}
}
