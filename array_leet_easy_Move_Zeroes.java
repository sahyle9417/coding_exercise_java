package coding_exercise_java;

import java.io.*;

public class array_leet_easy_Move_Zeroes {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		int n = Integer.parseInt(input);
		int[] list = new int[n];
		for (int i = 0; i < n; i++) {
			input = br.readLine();
			list[i] = Integer.parseInt(input);
		}
		array_leet_easy_Move_Zeroes_Solution solution = new array_leet_easy_Move_Zeroes_Solution();
		solution.moveZeroes(list);
		for (int i = 0; i < list.length; i++) {
			System.out.println(list[i]);
		}
	}
}

class array_leet_easy_Move_Zeroes_Solution {
    public void moveZeroes(int[] nums) {
        int read_idx = 0;
        int write_idx = 0;
        for(; read_idx<nums.length; read_idx++) {
        	if(nums[read_idx] != 0) {
        		nums[write_idx++] = nums[read_idx];
        	}
        }
        for(; write_idx<nums.length; write_idx++) {
        	nums[write_idx] = 0;
        }
    }
}
