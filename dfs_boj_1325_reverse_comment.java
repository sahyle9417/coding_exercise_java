package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_boj_1325_reverse_comment {

	static int N;
	
	// map이 ArrayList 타입의 원소를 갖는 1차원 배열임을 선언
	// 연결관계를 2차원 배열에 적으면 메모리 초과 발생
	// 노드 개수 = 10,000이므로 2차원 배열의 크기는 10,000*10,000 = 1억이라서
	// 연결의 개수 = 100,000이므로 LinkedList 또는 ArrayList로 하는게 훨씬 공간 효율적
	static ArrayList<Integer>[] map;
	
	// 각 노드가 한번씩 leaf(가장 맨 마지막에 감염되는 노드)가 되면서 다른 노드들 방문하는데
	// 이미 방문한 노드 다시 방문하지 않기 위함
	static int[] visit;

	// 각 노드가 감염시킬 수 있는 다른 PC의 개수
	static int[] mutate;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());

		N = Integer.parseInt(st.nextToken());

		// map 배열의 크기는 N+1임을 선언
		map = new ArrayList[N+1];
		
		// map 배열은 ArrayList<Integer> 타입의 원소를 가지는 1차원 배열이며
		// map 배열의 길이는 N+1임을 선언해줬지만
		// 각 원소를 new로 공간 할당 및 초기화해주지 않으면 사용하려고 할때 Null Pointer Exception 발생
		// 일반적인 원시형(primitive) 타입의 원소를 담는 배열은 자동으로 초기화해주지만
		// ArrayList는 원시형 타입이 아니기 때문
		for(int n=1; n<=N; n++) {
			map[n] = new ArrayList<Integer>();
		}
		mutate = new int[N+1];
		
		int M = Integer.parseInt(st.nextToken());
		for(int m=1; m<=M; m++) {
			st = new StringTokenizer(br.readLine());
			int to = Integer.parseInt(st.nextToken());
			int from = Integer.parseInt(st.nextToken());
			// A가 B를 신뢰할때 B 해킹시 A해킹 가능하다는 점 유의
			// 즉 B(from)에서 A(to)로 이동가능한 것임
			// map의 to번째 요소에 담긴 ArrayList의 요소들은 to로 향할 수 있는 노드들(from)의 번호
			map[to].add(from);
		}
		
		// 각 노드가 한번씩 leaf(가장 맨 마지막에 감염되는 노드)가 된다.
		// 감염순서가 A->B->C가 될때, dfs(C)에서 시작하면
		// map[C]에 B 있으니 dfs(B) 호출, B는 C를 감염시킬 수 있다는 사실 알게됐으니 mutate[B]++
		// map[B]에 A 있으니 dfs(A) 호출, A는 B를 감염시킬 수 있다는 사실 알게됐으니 mutate[A]++
		for(int leaf=1; leaf<=N; leaf++) {
			visit = new int[N+1];
			/*System.out.println("=================");
			System.out.println("leaf:"+leaf);*/
			dfs(leaf);
		}
		
		/*for(int n=1; n<=N; n++) {
			System.out.print(mutate[n]+" ");
		}
		System.out.print("\n");*/
		
		int max = 0;
		for(int n=1; n<=N; n++) {
			max = Math.max(mutate[n], max);
		}
		for(int n=1; n<=N; n++) {
			if(mutate[n]==max) {
				System.out.print(n+" ");
			}
		}
	}
	
	// to를 감염시킬 수 있는 노드(from)들을 하나씩 꺼내서 dfs(from) 돌린다.
	static void dfs(int to) {
		
		visit[to] = 1;
		
		for(int from : map[to]) {
			if(visit[from]==0) {
				// to를 감염시킬 수 있으면서 아직 방문하지 않은 노드(from) 발견
				// from 입장에서는 자신이 감염시킬 수 있는 새로운 노드(to)를 발견한 것이므로 mutate[from]값 1 증가
				mutate[from]++;
				
				/*System.out.println(from+"->"+to);
				for(int n=1; n<=N; n++) {
					System.out.print(mutate[n]+" ");
				}
				System.out.print("\n");*/
				
				dfs(from);
			}
		}
	}
}
