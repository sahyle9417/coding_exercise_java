package coding_exercise_java;

import java.util.*;
import java.io.*;

public class dp_boj_1912 {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		int[] dp = new int[n+1];
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		// ret는 최종 output(연속합의 최대값)
		// dp값이 아무리 작더라도 ret값이 갱신될 수 있도록 ret를 int가 가질 수 있는 가장 작은 값으로 설정
		int ret = Integer.MIN_VALUE;

		// dp[i]에는 i번째 요소를 포함하는 경우 중에서의 연속합의 최대값
		for(int i=1; i<=n; i++) {
			int input = Integer.parseInt(st.nextToken());
			// input : i번째 입력부터 연속합 연산을 새로 시작하는 경우
			// dp[i-1]+input : i-1번째 요소 뒤에 꼬리물기하는 경우
			// 둘 중에 더 큰 값이 i번째 요소를 포함하는 경우 중에서의 연속합의 최대값
			dp[i] = Math.max(input, dp[i-1]+input);
			// dp배열의 최대값을 ret에 실시간으로 기록
			ret = Math.max(ret, dp[i]);
		}
		
		/*for(int i=1; i<=n; i++) {
			System.out.print(dp[i]+" ");
		}
		System.out.println("\n");*/
		
		System.out.println(ret);		
	}
}
