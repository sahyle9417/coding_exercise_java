package coding_exercise_java;

import java.io.*;

public class string_leet_easy_Reverse_String {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		char[] char_list = str.toCharArray();
		string_leet_easy_Reverse_String_Solution solution = new string_leet_easy_Reverse_String_Solution();
		solution.reverseString(char_list);
		for(char c : char_list) {
			System.out.print(c);
		}
	}
}

class string_leet_easy_Reverse_String_Solution {
    public void reverseString(char[] s) {
        int start = 0;
        int end = s.length - 1;

        while(start < end) {
        	char tmp = s[start];
        	s[start] = s[end];
        	s[end] = tmp;

        	start++;
        	end--;
        }
    }
}
