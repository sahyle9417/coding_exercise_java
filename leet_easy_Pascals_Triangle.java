package coding_exercise_java;

import java.io.*;
import java.util.*;

public class leet_easy_Pascals_Triangle {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		leet_easy_Pascals_Triangle_Solution solution = new leet_easy_Pascals_Triangle_Solution();
		print2DList(solution.generate(n));
	}

	static void print2DList(List<List<Integer>> all_lists) {
		for (List<Integer> one_list : all_lists) {
			for (int element : one_list) {
				System.out.print(element + " ");
			}
			System.out.println();
		}
	}
}

class leet_easy_Pascals_Triangle_Solution {
	public List<List<Integer>> generate(int numRows) {
		List<List<Integer>> all_lists = new LinkedList<>();
		if (numRows == 0) {
			return all_lists;
		}

		all_lists.add(Arrays.asList(1));
		if (numRows == 1) {
			return all_lists;
		}

		all_lists.add(Arrays.asList(1, 1));
		if (numRows == 2) {
			return all_lists;
		}

		List<Integer> prev_row = all_lists.get(1);
		for (int rowIdx = 3; rowIdx <= numRows; rowIdx++) {
			List<Integer> curr_row = new ArrayList<>();

			curr_row.add(1);
			for (int colIdx = 1; colIdx < rowIdx - 1; colIdx++) {
				curr_row.add(prev_row.get(colIdx - 1) + prev_row.get(colIdx));
			}
			curr_row.add(1);

			all_lists.add(curr_row);
			prev_row = curr_row;
		}

		return all_lists;
	}

}
