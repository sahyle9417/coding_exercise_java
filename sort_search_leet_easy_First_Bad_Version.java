package coding_exercise_java;

import java.io.*;

public class sort_search_leet_easy_First_Bad_Version {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		sort_search_leet_easy_First_Bad_Version_Solution solution = new sort_search_leet_easy_First_Bad_Version_Solution();
		int n = Integer.parseInt(br.readLine());
		solution.bad_start = Integer.parseInt(br.readLine());
		// must return true
		System.out.println(solution.bad_start == solution.firstBadVersion(n));
		// comparison occurrence
		System.out.println(solution.called);
	}
}

/*
 * The isBadVersion API is defined in the parent class VersionControl.
 * boolean isBadVersion(int version);
 */

class VersionControl {
	int bad_start = 10;
	int called = 0;

	boolean isBadVersion(int version) {
		called += 1;
		return version >= bad_start ? true : false;
	}
}

class sort_search_leet_easy_First_Bad_Version_Solution extends VersionControl {

	public int firstBadVersion(int n) {

		int start = 1;
		int end = n;

		while (start < end) {

			//int mid = (start + end) / 2;			// Overflow 발생
			int mid = start + ((end - start) / 2);	// Overflow 미발생

			if (isBadVersion((int)mid)) {
				end = mid;
			}
			else {
				start = mid + 1;
			}
		}
		return start;
	}
}
