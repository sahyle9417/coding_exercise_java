package coding_exercise_java;

import java.io.*;

public class leet_easy_Reverse_Bits {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		leet_easy_Reverse_Bits_Solution solution = new leet_easy_Reverse_Bits_Solution();
		System.out.println(solution.reverseBits(n));
	}

}

class leet_easy_Reverse_Bits_Solution {
	// you need treat n as an unsigned value
	public int reverseBits(int n) {
		int ret = 0;
		for (int i = 31; i >= 0; i--) {
			if (getBit(n, i)) {
				ret |= (1 << (31 - i));
			}
		}
		return ret;
	}

	// 우측 끝에서부터 i번째 bit가 1인지 여부 반환
	boolean getBit(int n, int i) {
		// 1 << i : 1을 좌측으로 i bit만큼 shift
		// & : Bitwise And
		return (n & (1 << i)) != 0;
	}
}
