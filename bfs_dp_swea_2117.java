package coding_exercise_java;

import java.io.*;
import java.util.*;

class ij{
	int i;
	int j;
	ij(int i, int j){
		this.i = i;
		this.j = j;
	}
}

public class bfs_dp_swea_2117 {
	
	static int N;
	static int M;
	static int[][] map;
	
	public static void main(String[] args) throws Exception{
		FileReader fr = new FileReader(new File("sample_input.txt"));
		BufferedReader br = new BufferedReader(fr);
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		int TC = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=TC; tc++) {
			st = new StringTokenizer(br.readLine());
			N = Integer.parseInt(st.nextToken());
			M = Integer.parseInt(st.nextToken());
			map = new int[N+1][N+1];
			for(int i=1; i<=N; i++) {
				st = new StringTokenizer(br.readLine());
				for(int j=1; j<=N; j++) {
					map[i][j] = Integer.parseInt(st.nextToken());
				}
			}
			
			int answer = -1;
			
			for(int center_i=1; center_i<=N; center_i++) {
				for(int center_j=1; center_j<=N; center_j++) {
					int house_num = 0;
					for(int k=1; k<=N*2; k++) {
						house_num += house_at_distance_k(center_i, center_j, k);
						int expense = k*k+(k-1)*(k-1);
						if(house_num*M >= expense) {
							answer = Math.max(house_num, answer);
						}
					}
				}
			}
			System.out.println("#"+tc+" "+answer);
			
		}
		
	}

	// 처음에 매번 bfs로 k범위 이내에 있는 집의 개수 다 구했었는데 시간초과남
	// 정확히 k만큼 떨어진 집의 개수를 누적하는 방식으로 바꿔서 해결함
	static int house_at_distance_k(int center_i, int center_j, int k) {
		
		// center에서 정확히 k만큼 떨어진 집 구하기 위해 di와 dj의 리스트 생성
		// Math.abs(di)+Math.abs(dj)==k-1일때 거리가 k됨
		Queue<ij> dij = new LinkedList<>();
		
		for(int di=0; di<k; di++) {
			// Math.abs(di)+Math.abs(dj)==k-1일때 거리가 k됨
			int dj = k-1-di;
			dij.add(new ij(di, dj));
			if(di!=0) {
				dij.add(new ij(-1*di, dj));
			}
			if(dj!=0) {
				dij.add(new ij(di, -1*dj));
			}
			if(di!=0 && dj!=0) {
				dij.add(new ij(-1*di, -1*dj));
			}
		}
		int house_num = 0;
		
		for(ij d : dij) {
			//System.out.println("k:"+k+" "+d.i+","+d.j);
			int i = center_i+d.i;
			int j = center_j+d.j;
			if(i<1 || i>N || j<1 || j>N) {
				continue;
			}
			if(map[i][j]==1) {
				house_num++;
			}
		}
		return house_num;
	}
	
	/*static int[] di = {-1, 1, 0, 0};
	static int[] dj = {0, 0, -1, 1};
	static boolean[][] visit;
	static int max_house_with_profit(int center_i, int center_j, int k) {
		visit = new boolean[N+1][N+1];
		LinkedList<ij> bfs_q = new LinkedList<>();

		bfs_q.add(new ij(center_i, center_j));
		visit[center_i][center_j] = true;
		
		int house_num = 0;
		
		int distance = 1;
		while(!bfs_q.isEmpty()) {
			int q_size = bfs_q.size();
			for(int qs=0; qs<q_size; qs++) {
				ij point = bfs_q.poll();
				if(map[point.i][point.j]==1) {
					house_num++;
				}
				if(distance==k) {
					continue;
				}
				for(int d=0; d<4; d++) {
					int next_i = point.i+di[d];
					int next_j = point.j+dj[d];
					if(next_i<1 || next_i>N || next_j<1 || next_j>N || visit[next_i][next_j]) {
						continue;
					}
					bfs_q.add(new ij(next_i, next_j));
					// 큐에서 빼낼때 visit 표시해서 큐에 중복되게 들어갔었음, 기초니까 실수X
					visit[next_i][next_j] = true;
				}
			}
			distance++;
		}
		//System.out.println("center "+center_i+","+center_j+" k:"+k+" num:"+house_num);

		int expense = k*k+(k-1)*(k-1);
		
		System.out.println("center:"+center_i+","+center_j+" k:"+k);
		System.out.println("house_num:"+house_num+" profit:"+house_num*M);
		System.out.println("expense:"+expense);
		
		if(house_num*M>=expense) {
			return house_num;
		}
		else {
			return -1;
		}
	}*/
}



