package coding_exercise_java;

import java.io.*;

public class dp_leet_easy_Maximum_Subarray {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		String[] str_list = str.equals("") ? new String[0] : str.split(",");
		int[] int_list = new int[str_list.length];
		for (int i = 0; i < str_list.length; i++) {
			int_list[i] = Integer.parseInt(str_list[i]);
		}
		dp_leet_easy_Maximum_Subarray_Solution solution = new dp_leet_easy_Maximum_Subarray_Solution();
		System.out.println(solution.maxSubArray(int_list));
	}
}

class dp_leet_easy_Maximum_Subarray_Solution {
	public int maxSubArray(int[] nums) {
		int len = nums.length;
		if (len == 0) {
			return 0;
		}
		// current_max는 현재 요소를 포함한 상태로 얻을 수 있는 최대값
		int current_max = nums[0];
		// 현재 요소 포함 여부와 무관한 최대값
		// 최소한 하나의 요소는 포함해야하므로 nums[0]이 음수여도 무조건 추가
		int total_max = nums[0];
		
		for (int i = 1; i < len; i++) {
			// current_max + nums[i] : 현재 요소를 앞의 연속된 요소들의 리스트에 추가
			// nums[i] : 현재 요소로부터 새로운 리스트 시작
			current_max = Math.max(current_max + nums[i], nums[i]);
			total_max = Math.max(total_max, current_max);
		}
		return total_max;
	}
}
