package coding_exercise_java;

import java.io.*;
import java.util.*;

public class prime_number_list_samsung_interview {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		long N = Integer.parseInt(br.readLine());
		// N보다 작거나 같은 소수 리스트 찾을 예정
		LinkedList<Long> list = new LinkedList<>();
		for(long i=2; i<=N; i++) {
			list.add(i);
		}
		// 소수 선정
		int start_index=0;
		while(start_index<list.size()) {
			long start_num = list.get(start_index);
			
			// 소수의 배수들 리스트에서 제거
			int compare_index = start_index+1;
			while(compare_index<list.size()) {
				long compare_num = list.get(compare_index);
				
				// 소수의 배수 발견, 제거
				// 제거로 인해 인덱스 하나씩 앞당겨졌으니 인덱스 증가X
				if(compare_num%start_num==0) {
					list.remove(compare_index);
				}
				// 소수의 배수 아님, 스킵
				else {
					compare_index++;
				}
			}
			
			// 그 다음 소수 선택
			start_index++;
		}
		
		// 소수 리스트 출력
		for(long n:list) {
			System.out.print(n+" ");
		}
	}
}
