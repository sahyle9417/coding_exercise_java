package coding_exercise_java;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Queue;
import java.util.LinkedList;

public class bfs_boj_2606 {
	
	private static int node_num;
	private static int[][] map;
	private static boolean[] infected;
	
	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		node_num = Integer.parseInt(br.readLine());
		map = new int[node_num+1][node_num+1];
		infected = new boolean[node_num+1];
		
		int line_num = Integer.parseInt(br.readLine());
		for(int i=0; i<line_num; i++) {
			String tmp = br.readLine();
			int n = Integer.parseInt(tmp.split(" ")[0]);
			int m = Integer.parseInt(tmp.split(" ")[1]);
			map[n][m] = map[m][n] = 1;
		}
		
		int start_node = 1;
		bfs(start_node);
		
		int infected_num = 0;
		for(int i=1; i<=node_num; i++) {
			if(infected[i]) {
				infected_num++;
			}
		}
		System.out.println(infected_num-1);		//시작 노드는 제외하고 출력하라는 문제의 조건

	}
	
	public static void bfs(int start_node) {
		Queue<Integer> q = new LinkedList<>();
		q.offer(start_node);
		
		while(!q.isEmpty()) {
			//queue에서 뽑은 정점(polled) 실제로 방문
			int polled_node = q.poll();
			infected[polled_node] = true;		//실제 방문 발생
			
			//polled된 정점과 인접한 정점들을 queue에 넣고 다른 정점이 해당 정점을 또다시 queue에 넣는 것을 방지하기 위해 infected배열에 표시
			for(int i=1; i<=node_num; i++) {
				if(map[polled_node][i]==1 && infected[i]==false) {
					q.offer(i);
					infected[i] = true;		//방문하는 건 아니지만, 다른 정점에서도 정점i에 연결되어 있어 정점i가 큐에 여러번 들어가는 것을 방지
				}
			}
		}
	}
}
