package coding_exercise_java;

import java.io.*;
import java.util.*;

public class implement_boj_1022 {
	public static void main(String[] args) throws Exception {		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int r1 = Integer.parseInt(st.nextToken());
		int c1 = Integer.parseInt(st.nextToken());
		int r2 = Integer.parseInt(st.nextToken());
		int c2 = Integer.parseInt(st.nextToken());
		
		int[] dr = {0, -1, 0, 1};	// 우, 상, 좌, 하, 우, ...
		int[] dc = {1, 0, -1, 0};	// 우, 상, 좌, 하, 우, ...
		int distance = 0;	// 1, 1, 2, 2, 3, 3, ...
		boolean distance_increase = true;	// true, false, true, false, ...
		int direction = 0;	// 0, 1, 2, 3, 0, ...
		
		int[][] ret = new int[r2-r1+1][c2-c1+1];
		
		int max_r = Math.max(Math.abs(r1), Math.abs(r2));
		int max_c = Math.max(Math.abs(c1), Math.abs(c2));
		
		int element = 1;
		int max_element = 1;
		int r = 0;
		int c = 0;
		
		while(Math.abs(r) <= max_r || Math.abs(c) <= max_c) {
			
			if(distance_increase) {
				distance++;
				distance_increase = false;
			}
			else {
				distance_increase = true;
			}
			
			for(int d=0; d<distance; d++) {
				if(r>=r1 && r<=r2 && c>=c1 && c<=c2) {
					ret[r-r1][c-c1] = element;
					max_element = Math.max(max_element, element);
				}
				//System.out.println("["+r+"]["+c+"] = "+element);
				
				element++;
				r = r+dr[direction];
				c = c+dc[direction];
			}
			direction = (direction+1) % 4;  // 0, 1, 2, 3, 0, ...
			
		}
		
		int max_length = (int)Math.log10(max_element)+1;
		String format = "%"+max_length+"d ";
		for(int i=0; i<ret.length; i++) {
			for(int j=0; j<ret[0].length; j++) {
				System.out.printf(format, ret[i][j]);
			}
			System.out.print("\n");
		}
		
	}
}
