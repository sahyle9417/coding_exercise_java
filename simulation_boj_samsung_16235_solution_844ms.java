package coding_exercise_java;

import java.io.*;
import java.util.*;

public class simulation_boj_samsung_16235_solution_844ms {

	static int[][] food;
	static int[][] food_add;
	static int N, M, K = 0;
	static int[] dx = { -1, -1, -1, 0, 0, 1, 1, 1 };
	static int[] dy = { -1, 0, 1, -1, 1, -1, 0, 1 };

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		// N:크기,M:나무수,K:시간
		String str = br.readLine();
		String[] strArr = str.split(" ");
		N = Integer.parseInt(strArr[0]);
		M = Integer.parseInt(strArr[1]);
		K = Integer.parseInt(strArr[2]);

		food = new int[N][N];
		food_add = new int[N][N];
		for (int i = 0; i < N; i++) {
			String str1 = br.readLine();
			String[] strArr1 = str1.split(" ");
			for (int j = 0; j < N; j++) {
				food_add[i][j] = Integer.parseInt(strArr1[j]);
				food[i][j] = 5; // 최초 기초 양분 5 초기화
			}
		}

		LinkedList<Tree> treeList = new LinkedList<>();
		for (int i = 0; i < M; i++) {
			String str1 = br.readLine();
			String[] strArr1 = str1.split(" ");
			int x = Integer.parseInt(strArr1[0]);
			int y = Integer.parseInt(strArr1[1]);
			int age = Integer.parseInt(strArr1[2]);
			treeList.add(new Tree(x-1, y-1, age, true));	// 좌표(x,y), 나이, 생존여부(alive)
		}

		for (int k = 0; k < K; k++) {
			
			// 봄, 나이만큼 양분을 흡수, 나이+1 (어린나무 순), 양분을 못먹으면 즉사
			for (Tree t : treeList) {
				// 양분 흡수
				if (food[t.x][t.y] >= t.age) {
					food[t.x][t.y] -= t.age;
					t.age++;
				}
				// 양분 못먹으면 사망
				else {
					t.alive = false;
				}
			}

			// 여름, 죽는나무/2 만큼의 양분이 해당 칸에 쌓임
			for (Iterator<Tree> it = treeList.iterator(); it.hasNext();) {
				Tree t = it.next();
				if (!t.alive) {
					food[t.x][t.y] += t.age/2;
					it.remove();
				}
			}

			// 가을, 나이가 5의 배수인 나무 인접 8개 영역에 나이 1인 나무를 뿌림
			LinkedList<Tree> newTreeList = new LinkedList<>();
			for (Tree t : treeList) {
				if (t.age % 5 == 0)
					for (int j = 0; j < 8; j++) {
						int next_x = t.x + dx[j];
						int next_y = t.y + dy[j];
						if (-1 < next_x && next_x < N && -1 < next_y && next_y < N) {
							newTreeList.add(new Tree(next_x, next_y, 1, true));
						}
					}
			}
			treeList.addAll(0, newTreeList);

			if (k == K - 1) {
				System.out.println(treeList.size());
				return;
			}
			// 겨울, 사용자가 지정한 양분을 보충
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					food[i][j] += food_add[i][j];
				}
			}
		}
	}
	static class Tree {
		int x;
		int y;
		int age;
		boolean alive;
		public Tree(int x, int y, int age, boolean alive) {
			super();
			this.x = x;
			this.y = y;
			this.age = age;
			this.alive = alive;
		}
	}
}