package coding_exercise_java;


//길이가 n인 배열에 1부터 n까지 숫자가 중복 없이 한 번씩 들어 있는지를 확인하려고 합니다.
//1부터 n까지 숫자가 중복 없이 한 번씩 들어 있는 경우 true를, 아닌 경우 false를 반환하도록 함수 solution을 완성해주세요.
//배열의 길이는 10만 이하입니다.
//배열의 원소는 0 이상 10만 이하인 정수입니다.

public class array_duplicate_check {
	static boolean array_duplicate_check(int[] arr) {
		boolean answer = true;
		int arr_max = arr[0];
		for(int i=0; i<arr.length; i++) {
			if(arr_max<arr[i]) {
				arr_max = arr[i];
			}
		}
		boolean bool_arr[] = new boolean[arr_max+1];
		for(int i=0;i<arr.length;i++) {
			if(bool_arr[arr[i]]==true) {	//중복값 발견
				answer=false;
			}
			bool_arr[arr[i]]=true;
		}
		for(int i=1;i<bool_arr.length;i++) {
			System.out.println("i="+i+" bool_arr[i]="+bool_arr[i]);
			if(bool_arr[i]==false) {
				answer=false;
			}
		}
		return answer;
	}
	public static void main(String[] args) throws Exception {
		int[] arr = {1,3,5,4,7};
		System.out.println(array_duplicate_check(arr));
	}


}
