package coding_exercise_java;
import java.util.Scanner;

public class bubble_sort {
	
	private static void bubble_sort(int[] input){
		for(int end=input.length-1; end>=1; end--){	//매 회전 끝날 때마다 end의 위치에 가장 큰 값이 저장, 다음 회전엔 해당 위치 고려안해도 됨
			for(int i=0; i<end; i++){
				if(input[i]>input[i+1]){
					int tmp=input[i];
					input[i]=input[i+1];
					input[i+1]=tmp;
				}
			}
		}
	}
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int input_length = scan.nextInt();
		int[] input = new int[input_length];
		for(int i=0; i<input_length; i++){
			input[i]=scan.nextInt();
		}
		
		bubble_sort(input);
		
		for(int i=0; i<input_length; i++){
			System.out.println(input[i]);
		}
		
	}
}
