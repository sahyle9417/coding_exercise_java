package coding_exercise_java;

import java.io.*;

public class quick_sort {
	
	static int data_length;
	static int[] data;

    public static void quick_sort(int left_end, int right_end){
        int left = left_end;
        int right = right_end;
        int pivot = data[(left_end+right_end)/2];

        //System.out.println("left_end:"+left_end+" right_end:"+right_end);
        
        while (left <= right) {
            while(data[left] < pivot) left++;	// pivot과 값이 같아도 stop
            while(data[right] > pivot) right--;	// pivot과 값이 같아도 stop
            
            if(left <= right){    
                int temp = data[left];
                data[left] = data[right];
                data[right] = temp;
                left++;
                right--;
            }
        }
        
        // 두개의 포인터가 양쪽 끝에서 가운데로 오면서 모든 요소를 다 훑고 교차된 상태 (left > right)
        
        if(left_end < right) quick_sort(left_end, right);	// 원소가 2개 이상 존재하면 재귀호출
        if(right_end > left) quick_sort(left, right_end);	// 원소가 2개 이상 존재하면 재귀호출
    }

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		data_length = Integer.parseInt(br.readLine());
		data = new int[data_length];
		for(int i=0; i<data_length; i++){
			data[i]=Integer.parseInt(br.readLine());
		}
		
		quick_sort(0, data.length-1);
		
		for(int i=0; i<data_length; i++){
			System.out.println(data[i]);
		}
	}
}