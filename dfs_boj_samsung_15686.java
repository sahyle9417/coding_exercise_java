package coding_exercise_java;

import java.io.*;
import java.util.*;

public class dfs_boj_samsung_15686 {
	
	static class ij{
		int i;
		int j;
		ij(int i, int j){
			this.i = i;
			this.j = j;
		}
	}
	static int N;
	static int M;
	static int[][] map;
	static int h_num;
	static int c_num;
	static LinkedList<ij> H;
	static LinkedList<ij> C;
	static boolean[] c_choice;
	static int min_distance_sum = Integer.MAX_VALUE;
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		
		H = new LinkedList<>();	// 가정집 좌표
		C = new LinkedList<>();	// 치킨집 좌표
		
		map = new int[N+1][N+1];
		for(int i=1; i<=N; i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
				if(map[i][j]==1) {
					H.add(new ij(i, j));
				}
				else if(map[i][j]==2) {
					C.add(new ij(i, j));
				}
			}
		}
		
		h_num = H.size();	// 가정집 개수
		c_num = C.size();	// 치킨집 개수
		/*System.out.println("h_num:"+h_num);
		System.out.println("c_num:"+c_num);*/
		

		c_choice = new boolean[c_num];
		choose_c(0, 0);	// 0번째 치킨집부터 영업or폐업 결정, 현재까지 영업계속하기로 결정한 치킨집 개수는 0
		System.out.println(min_distance_sum);
		
	}
	
	static void choose_c(int index, int c_num_tmp) {
		if(c_num_tmp==M) {
			/*for(boolean b : c_choice) {
				if(b) {
					System.out.print("o ");
				}
				else {
					System.out.print("x ");
				}
			}
			System.out.print("\n");*/
			update_min_distance_sum();
			return;
		}
		if(index==c_num) {
			return;
		}
		c_choice[index] = true;
		choose_c(index+1, c_num_tmp+1);

		c_choice[index] = false;
		choose_c(index+1, c_num_tmp);
	}
	
	static void update_min_distance_sum() {
		
		int min_distance_sum_tmp = 0;
		for(ij h : H) {
			// min_distance : 선택된 집의 치킨거리
			int min_distance = Integer.MAX_VALUE;
			for(int c=0; c<C.size(); c++) {
				if(!c_choice[c]) {
					continue;
				}
				else {
					// distance : 선택된 집과 선택된 치킨집 사이의 치킨거리
					int distance = Math.abs(h.i - C.get(c).i) + Math.abs(h.j - C.get(c).j);
					min_distance = Math.min(distance, min_distance);
				}
			}
			min_distance_sum_tmp += min_distance;
		}
		min_distance_sum = Math.min(min_distance_sum_tmp, min_distance_sum);
	}
}
