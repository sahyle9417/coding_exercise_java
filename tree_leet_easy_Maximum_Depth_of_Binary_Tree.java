package coding_exercise_java;

public class tree_leet_easy_Maximum_Depth_of_Binary_Tree {
	public static void main(String[] args) {
		Integer[] tree_list = { null, 10, 5, 15, null, null, 11, 20 };
		TreeNode root = makeTree(tree_list, 1);
		tree_leet_easy_Maximum_Depth_of_Binary_Tree_Solution solution = new tree_leet_easy_Maximum_Depth_of_Binary_Tree_Solution();
		System.out.println(solution.maxDepth(root));
	}
	static TreeNode makeTree(Integer[] tree_list, int idx) {
		if (idx >= tree_list.length) {
			return null;
		}
		if (tree_list[idx] == null) {
			return null;
		}
		TreeNode node = new TreeNode((int)tree_list[idx]);
		node.left = makeTree(tree_list, idx * 2);
		node.right = makeTree(tree_list, idx * 2 + 1);
		return node;
	}
}


class tree_leet_easy_Maximum_Depth_of_Binary_Tree_Solution {
	public int maxDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		if (root.left == null && root.right == null) {
			return 1;
		}
		return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
	}
}


/*
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode() {
	}

	TreeNode(int val) {
		this.val = val;
	}

	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}
*/
