package coding_exercise_java;

import java.util.*;

public class tree_leet_easy_Validate_Binary_Search_Tree {

	public static void main(String[] args) {
		Integer[] tree_list = { null, 10, 5, 15, null, null, 11, 20 };
		TreeNode root = makeTree(tree_list, 1);
		tree_leet_easy_Validate_Binary_Search_Tree_slow_Solution solution = new tree_leet_easy_Validate_Binary_Search_Tree_slow_Solution();
		System.out.println(solution.isValidBST(root));
	}

	static TreeNode makeTree(Integer[] tree_list, int idx) {
		if (idx >= tree_list.length) {
			return null;
		}
		if (tree_list[idx] == null) {
			return null;
		}
		TreeNode node = new TreeNode((int)tree_list[idx]);
		node.left = makeTree(tree_list, idx * 2);
		node.right = makeTree(tree_list, idx * 2 + 1);
		return node;
	}
}


class tree_leet_easy_Validate_Binary_Search_Tree_recursive_Solution {
	
	public boolean isValidBST(TreeNode root) {
		// 중위 순회 내역을 기록하기 위한 LinkedList
		LinkedList<TreeNode> inorderList = new LinkedList<>();
		return isValidBstRecursive(inorderList, root);
	}
	
	boolean isValidBstRecursive(LinkedList<TreeNode> inorderList, TreeNode root){
		if (root == null) {
			return true;
		}
		
		// 좌측 sub-tree에 대해 중위 순회하며 유효성 검증
		if (!isValidBstRecursive(inorderList, root.left)) {
			return false;
		}

		// BST를 중위순회하면 오름차순으로 정렬되므로
		// 가장 마지막에 들어간 노드는 현재 노드보다 작아야 함
		TreeNode prevNode = inorderList.size() > 0 ? inorderList.getLast() : null;
		if (prevNode != null && prevNode.val >= root.val) {
			return false;
		}
		
		// 현재 노드를 리스트에 추가
		inorderList.add(root);

		// 우측 sub-tree에 대해 중위 순회하며 유효성 검증
		if (!isValidBstRecursive(inorderList, root.right)) {
			return false;
		}

		return true;
	}
}

/*
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode() {}
	TreeNode(int val) {
		this.val = val;
	}
	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}
*/
